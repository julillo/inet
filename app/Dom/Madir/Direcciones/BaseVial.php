<?php

namespace App\Dom\Madir\Direcciones;

use App\Dom\Madir\Direcciones\SegmentoVial;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Sofa\Eloquence\Eloquence;

class BaseVial extends Model {

    use SoftDeletes,
        Eloquence;

    protected $searchableColumns = [
        'nombre'
    ];
    protected $table = "madir_bases_viales";
    protected $fillable = [
        'nombre', 'status'
    ];

    public function segmentos() {
        return $this->hasMany(SegmentoVial::class, 'base_id', 'id');
    }

}
