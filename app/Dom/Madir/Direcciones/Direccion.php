<?php

namespace App\Dom\Madir\Direcciones;

use App\Dom\Madir\Direcciones\SegmentoVial;
use App\Dom\Madir\Terrenos\Terreno;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Direccion extends Model {

    use SoftDeletes;

    protected $table = "madir_direcciones";
    protected $fillable = [
    ];

    public function segmento() {
        return $this->belongsTo(SegmentoVial::class, 'vial_id', 'id');
    }

    public function terreno() {
        return $this->belongsTo(Terreno::class, 'terreno_id', 'id');
    }

    public function getDireccionFAttribute() {
        return sprintf("%s %s %s", $this->segmento->tipo->nombre, $this->segmento->base->nombre, $this->numero);
    }

}
