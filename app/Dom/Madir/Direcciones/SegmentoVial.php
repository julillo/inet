<?php

namespace App\Dom\Madir\Direcciones;

use App\Dom\Madir\Direcciones\BaseVial;
use App\Dom\Madir\Direcciones\TipoVial;
use App\Dom\Madir\Direcciones\Direccion;
use App\Dom\Madir\Terrenos\Terreno;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Sofa\Eloquence\Eloquence;

class SegmentoVial extends Model {

    use SoftDeletes,
        Eloquence;

    protected $table = "madir_segmentos_viales";
    protected $searchableColumns = [
        'tipo.nombre', "base.nombre", "numero_inicio", "numero_fin"
    ];
    protected $fillable = [
        'numero_pre', 'numero_inicio' . 'numero_fin', 'status', 'base_id', 'tipo_id'
    ];

    public function base() {
        return $this->belongsTo(BaseVial::class, 'base_id', 'id');
    }

    public function tipo() {
        return $this->belongsTo(TipoVial::class, 'tipo_id', 'id');
    }
    
    public function terreno(){
        return $this->belongsTo(Terreno::class, 'terreno_id', 'id');
    }

    public function direcciones() {
        return $this->hasMany(Direccion::class, 'vial_id', 'id');
    }
    
    public function getSegmentoFAttribute(){
        return sprintf("%s %s %s-%s", $this->tipo->nombre, $this->base->nombre, $this->numero_inicio, $this->numero_fin);
    }
}
