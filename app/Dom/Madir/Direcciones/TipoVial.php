<?php

namespace App\Dom\Madir\Direcciones;

use App\Dom\Madir\Direcciones\SegmentoVial;
use Illuminate\Database\Eloquent\Model;

class TipoVial extends Model {

    use \Illuminate\Database\Eloquent\SoftDeletes;

    protected $table = "madir_tipos_viales";
    protected $fillable = [
        'nombre', 'abr', 'status', 'parent_id'
    ];

    public function childs() {
        return $this->hasMany(self::class, 'parent_id', 'id');
    }

    public function super() {
        return $this->belongsTo(self::class, 'parent_id', 'id');
    }

    public function segmentos() {
        return $this->hasMany(SegmentoVial::class, 'tipo_id', 'id');
    }

}
