<?php

namespace App\Dom\Madir\Terrenos;

use App\Dom\Madir\Direcciones\SegmentoVial;
use App\Dom\Madir\Direcciones\Direccion;
use App\Dom\Madir\Terrenos\TipoTerreno;
use App\GeoDB\Terreno as GeoPredio;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Sofa\Eloquence\Eloquence;

class Terreno extends Model {

    use SoftDeletes,
        Eloquence;

    protected $connection = 'mysql';
    protected $table = "madir_terrenos";
    protected $fillable = [
        'rol_manzana', 'rol_terreno', 'nombre', 'status', 'tipo_id', 'parent_id'
    ];
    protected $searchableColumns = [
        "tipo.nombre",
        'id',
        'rol_manzana',
        'rol_predio',
        'nombre',
    ];

    public function childs() {
        return $this->hasMany(self::class, 'parent_id', 'id');
    }

    public function recursiveDown() {
        return $this->childs()->with(['recursiveDown']);
    }

    public function super() {
        return $this->belongsTo(self::class, 'parent_id', 'id');
    }

    public function tipo() {
        return $this->belongsTo(TipoTerreno::class, 'tipo_id', 'id');
    }

    public function segmentos() {
        return $this->hasMany(SegmentoVial::class, 'terreno_id', 'id');
    }

    public function direcciones() {
        return $this->hasMany(Direccion::class, 'terreno_id', 'id');
    }

    public function recursiveUp() {
        return $this->super()->with(["direcciones", "recursiveUp"]);
    }

    public function getRolAttribute() {
        return sprintf('%s-%s', $this->rol_manzana, $this->rol_predio);
    }

    public function getRolFAttribute() {
        return sprintf('%05d-%03d', $this->rol_manzana, $this->rol_predio);
    }

    public function getTerrenoFAttribute() {
        return trim(sprintf("%s %s", $this->nombreF, $this->superNombreF));
    }

    protected function getNombreFAttribute() {
        return trim(sprintf("%s %s", isset($this->tipo) ? $this->tipo->nombre : "", !empty($this->nombre) ? $this->nombre : $this->rolF));
    }

    protected function getSuperNombreFAttribute() {
        return empty($parent = trim(sprintf("%s %s", isset($this->super) ? $this->super->tipo->nombre : "", isset($this->super) ? $this->super->nombre : ""))) ? "" : sprintf("<- %s", $parent);
    }

    public function getTerrenoFFAttribute() {
        return trim(sprintf("%s %s", $this->tipo->nombre, !empty($this->nombre) ? $this->nombre : $this->rolF));
    }

    public function geometria() {
        return $this->hasOne(GeoPredio::class, 'gid', "id");
    }

}
