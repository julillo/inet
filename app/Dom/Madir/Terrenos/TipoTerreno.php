<?php

namespace App\Dom\Madir\Terrenos;

use App\Dom\Madir\Terrenos\Terreno;
use Illuminate\Database\Eloquent\Model;

class TipoTerreno extends Model {

    use \Illuminate\Database\Eloquent\SoftDeletes;

    protected $table = "madir_tipos_terrenos";
    protected $fillable = [
        "nombre", "abr", "descripcion", "status", "parent_id"
    ];

    public function childs() {
        return $this->hasMany(self::class, 'parent_id', 'id');
    }

    public function super() {
        return $this->belongsTo(self::class, 'parent_id', 'id');
    }

    public function terrenos() {
        return $this->hasMany(Terreno::class, 'tipo_id', 'id');
    }

}
