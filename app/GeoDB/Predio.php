<?php

namespace App\GeoDB;

use App\Dom\Madir\Terrenos\Terreno as MadirTerreno;
use Illuminate\Database\Eloquent\Model;

class Predio extends Model
{
    protected $connection = 'geodb';
    protected $table = "ly_201703_base_predios_wgs84";
    protected $primaryKey = 'gid';
    
    public function madirTerreno(){
        return $this->belongsTo(MadirTerreno::class, "gid", "id");
    }
}
