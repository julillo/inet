<?php

namespace App\Http\Controllers\Acl;

use App\Http\Controllers\Controller;
use App\Permission;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Redirector;
use Yajra\Datatables\Datatables;
use Yajra\Datatables\Engines\CollectionEngine;
use function redirect;
use function route;
use function session;
use function view;

class PermissionController extends Controller {

    /**
     * 
     * @access public
     */
    public function __construct() {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index() {
        return view("acl.permissions.index");
    }

    /**
     * 
     * @param Datatables $datatables
     * @return CollectionEngine
     * @access public
     */
    public function data(Datatables $datatables) {
        // get data
        $permissions = Permission::all();
        // bind table
        return $datatables->collection($permissions)
                        ->addColumn('action', function($permission) {
                            return view("acl.permissions.tables.actions.index", compact("permission"));
                        })
                        ->make(true);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create() {
        return view("acl.permissions.create");
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  Request  $request
     * @return Response
     */
    public function store(Request $request) {
        // validate
        $this->validate($request, [
            "name" => "required"
            , "display_name" => "required"
            , "description" => "required"
        ]);
        // save
        $permission = new Permission();
        $permission->name = $request->name;
        $permission->display_name = $request->display_name;
        $permission->description = $request->description;
        $permission->save();
        // redirect
        session()->flash("message", "Permiso creado");
        return redirect(route("permissions.index"));
    }

    /**
     * Display the specified resource.
     *
     * @param  Permission  $permission
     * @return Response
     */
    public function show(Permission $permission) {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  Permission  $permission
     * @return Response
     */
    public function edit(Permission $permission) {
        return view("acl.permissions.edit", compact("permission"));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Request  $request
     * @param  Permission  $permission
     * @return Response
     */
    public function update(Request $request, Permission $permission) {
        // validate
        $this->validate($request, [
            "name" => "required"
            , "display_name" => "required"
            , "description" => "required"
        ]);
        // save
        $permission->name = $request->name;
        $permission->display_name = $request->display_name;
        $permission->description = $request->description;
        $permission->save();
        // redirect
        session()->flash("message", "Permiso Actualizado");
        return redirect(route("permissions.edit", $permission));
    }

    /**
     * Soft deleting
     * @param Permission $permission
     * @return Redirector | \Illuminate\Http\RedirectResponse
     * @access public
     */
    public function delete(Permission $permission) {
        // soft-delete;
        $permission->delete();
        //redirect
        session()->flash("message", "Permiso eliminado");
        return redirect(route("permissions.index"));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  Permission  $permission
     * @return Response
     */
    public function destroy(Permission $permission) {
        
    }

}
