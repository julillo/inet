<?php

namespace App\Http\Controllers\Acl;

use App\Http\Controllers\Controller;
use App\Permission;
use App\Role;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Redirector;
use Illuminate\Support\Facades\DB;
use Yajra\Datatables\Datatables;
use Yajra\Datatables\Engines\CollectionEngine;
use function redirect;
use function route;
use function session;
use function view;

class RoleController extends Controller {

    /**
     * 
     * @access public
     */
    public function __construct() {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index() {
        return view("acl.roles.index");
    }

    /**
     * 
     * @param Datatables $datatables
     * @return CollectionEngine
     * @access public
     */
    public function data(Datatables $datatables) {
        $roles = Role::all();
        return $datatables->collection($roles)
                        ->addColumn('action', function($role) {
                            return view("acl.roles.tables.actions.index", compact("role"));
                        })
                        ->make(true);
    }

    /**
     * 
     * @param integer $id
     * @param Datatables $datatables
     * @return CollectionEngine
     */
    public function permissionsData(Datatables $datatables, Role $role) {
//        $permissions = DB::table("permissions")
//                ->select(DB::raw("permissions.id, permissions.display_name, permissions.description, GROUP_CONCAT(permission_role.role_id) as roles, $id as role_to_compare"))
//                ->leftJoin("permission_role", "permissions.id", "permission_role.permission_id")
//                ->groupBy("permissions.id", "permissions.display_name", "permissions.description")
//                ->get();
        $permissions = Permission::all();
        return $datatables->collection($permissions)
                        ->addColumn("action", function($permission) use ($role) {
//                            return $role->permissions()->find($permission->id) ? "yes" : "no";
                            return view("acl.roles.tables.actions.permissions", compact("permission", "role"));
                        })
                        ->make(true);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create() {
        return view("acl.roles.create");
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  Request  $request
     * @return Response
     */
    public function store(Request $request) {
        // validate
        $this->validate($request, [
            "name" => "required"
            , "display_name" => "required"
            , "description" => "required"
        ]);
        // save
        $role = new Role();
        $role->name = $request->name;
        $role->display_name = $request->display_name;
        $role->description = $request->description;
        $role->save();
        // redirect
        session()->flash("message", "Rol creado");
        return redirect(route("roles.index"));
    }

    /**
     * Display the specified resource.
     *
     * @param  Role  $role
     * @return Response
     */
    public function show(Role $role) {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  Role  $role
     * @return Response
     */
    public function edit(Role $role) {
        return view("acl.roles.edit", compact("role"));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Request  $request
     * @param  Role  $role
     * @return Response
     */
    public function update(Request $request, Role $role) {
        // validate
        $this->validate($request, [
            "name" => "required"
            , "display_name" => "required"
            , "description" => "required"
        ]);
        // save
        $role->name = $request->name;
        $role->display_name = $request->display_name;
        $role->description = $request->description;
        $role->save();
        // redirect
        session()->flash("message", "Rol actualizado");
        return redirect(route("roles.edit", $role));
    }

    /**
     * 
     * @param Request $request
     * @param Role $role
     * @return Redirector | \Illuminate\Http\RedirectResponse
     * @access public
     */
    public function updatePermissions(Request $request, Role $role) {
        // update
        foreach ($request->permissions as $key => $value) {
            if ($value) {
                $role->permissions()->find($key) ?: $role->attachPermission($key);
            } else {
                !$role->permissions()->find($key) ?: $role->detachPermission($key);
            }
        }
        // redirect
        session()->flash("message", "Successfully updated role permissions!");
        return redirect(route("roles.edit", $role));
    }

    /**
     * Soft deleting
     * @param Role $role
     * @return Redirector | \Illuminate\Http\RedirectResponse
     * @access public
     */
    public function delete(Role $role) {
        // soft-delete
        $role->delete();
        // redirect
        session()->flash("message", "Rol eliminado");
        return redirect(route("roles.index"));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  Role  $role
     * @return Response
     */
    public function destroy(Role $role) {
        
    }

}
