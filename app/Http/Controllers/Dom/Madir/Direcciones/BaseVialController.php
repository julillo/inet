<?php

namespace App\Http\Controllers\Dom\Madir\Direcciones;

use App\Dom\Madir\Direcciones\BaseVial;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class BaseVialController extends Controller {

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        return view("dom.madir.direcciones.bases-viales.index");
    }

    /**
     * 
     * @param \Yajra\Datatables\Datatables $datatables
     * @return \Illuminate\Http\JsonResponse
     */
    public function indexData(\Yajra\Datatables\Datatables $datatables) {
        // get data
        $basesViales = BaseVial::all();
        // bind table
        return $datatables->collection($basesViales)
                        ->setRowClass(function ($baseVial) {
                            return $baseVial->status ? '' : 'alert-danger';
                        })
                        ->addColumn('action', function($baseVial) {
                            return view("dom.madir.direcciones.bases-viales.actions.index-table", compact("baseVial"));
                        })
                        ->make(true);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function find(Request $request) {
        setlocale(LC_ALL, "en_US.utf8");
        $term = trim($request->q);

        if (empty($term)) {
            return \Response::json([]);
        }

        $tags = BaseVial::search($term)->limit(5)->get();

        $formatted_tags = [];

        foreach ($tags as $tag) {
            $formatted_tags[] = ['id' => $tag->id, 'text' => $tag->nombre];
        }

        return \Response::json($formatted_tags);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        return view("dom.madir.direcciones.bases-viales.create");
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        // validate
        $this->validate($request, [
            "codigo_ruta" => "nullable|unique:" . with(new BaseVial)->getTable() . "|max:10",
            "nombre" => "required|unique:" . with(new BaseVial)->getTable() . "|max:191",
        ]);
        // store
        $base = new BaseVial();
        $base->codigo_ruta = $request->codigo_ruta;
        $base->nombre = $request->nombre;
        $base->save();
        // flash & redirect
        session()->flash("message", "Base creada.");
        return redirect(route("madir.bases.viales.create"));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Dom\Madir\Direcciones\BaseVial  $baseVial
     * @return \Illuminate\Http\Response
     */
    public function show(BaseVial $baseVial) {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Dom\Madir\Direcciones\BaseVial  $baseVial
     * @return \Illuminate\Http\Response
     */
    public function edit(BaseVial $baseVial) {
        return view("dom.madir.direcciones.bases-viales.edit", compact("baseVial"));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Dom\Madir\Direcciones\BaseVial  $baseVial
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, BaseVial $baseVial) {
        // validate
        $this->validate($request, [
            "codigo_ruta" => "nullable|unique:" . with(new BaseVial)->getTable() . ",codigo_ruta," . $baseVial->id . "|max:10",
            "nombre" => "required|unique:" . with(new BaseVial)->getTable() . ",nombre," . $baseVial->id . "|max:191",
        ]);
        // store
        $base = $baseVial;
        $base->codigo_ruta = $request->codigo_ruta;
        $base->nombre = $request->nombre;
        $base->status = $request->status;
        $base->save();
        // flash & redirect
        session()->flash("message", "Base editada.");
        return redirect(route("madir.bases.viales.edit", compact("baseVial")));
    }

    public function switchStatus(BaseVial $baseVial) {
        $baseVial->status = !$baseVial->status;
        $baseVial->save();
        // flash & redirect
        session()->flash("message", sprintf("Base vial %s.", $baseVial->status ? "activado" : "desactivado"));
        return redirect(route("madir.bases.viales.index"));
    }

    public function delete(BaseVial $baseVial) {
        $baseVial->delete();
        // flash & redirect
        session()->flash("message", "Tipo vial eliminado.");
        return redirect(route("madir.bases.viales.index"));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Dom\Madir\Direcciones\BaseVial  $baseVial
     * @return \Illuminate\Http\Response
     */
    public function destroy(BaseVial $baseVial) {
        //
    }

}
