<?php

namespace App\Http\Controllers\Dom\Madir\Direcciones;

use App\Dom\Madir\Direcciones\Direccion;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class DireccionController extends Controller {

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        $roots = [];
        return view("dom.madir.direcciones.maestro.index", compact("roots"));
    }

    /**
     * 
     * @param \Yajra\Datatables\Datatables $datatables
     * @return \Illuminate\Http\JsonResponse
     */
    public function indexData(\Yajra\Datatables\Datatables $datatables) {
        setlocale(LC_ALL, "en_US.utf8");
        // get data
        $direcciones = Direccion::with(['segmento', "segmento.base", "segmento.tipo"])->get();
        // bind table
        return $datatables->collection($direcciones)
                        ->setRowClass(function ($direccion) {
                            return $direccion->status ? '' : 'alert-danger';
                        })
                        ->addColumn('tipo', function($direccion) {
                            return $direccion->segmento->tipo->nombre;
                        })
                        ->addColumn('segmento', function($direccion) {
                            return $direccion->segmento->base->nombre;
                        })
                        ->addColumn('action', function($direccion) {
                            return view("dom.madir.direcciones.maestro.actions.index-table", compact("direccion"));
                        })
                        ->make(true);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        $types = [];
        $typeDef = "";
        $parents = [];
        $parentSelDef = "";
        $terrenoSelDef = "";
        $terrenos = [];
        return view("dom.madir.direcciones.maestro.create", compact("types", "typeDef", "parents", "parentSelDef", "terrenoSelDef", "terrenos"));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        //validate
        $this->validate($request, [
            "numero" => "required|unique_with:" . with(new Direccion)->getTable() . ",vial_id|max:10",
            "numero_postal" => "nullable|max:10",
            "vial_id" => "required",
            "terreno_id" => "nullable",
        ]);
        //save
        $d = new Direccion();
        $d->numero = $request->numero;
        $d->numero_postal = $request->numero_postal;
        $d->vial_id = $request->vial_id;
        $d->terreno_id = empty($request->terreno_id) ? null : $request->terreno_id;
        $d->save();
        //message & redirect
        session()->flash("message", "Dirección creado.");
        return redirect(route("madir.maestro.direcciones.create"));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Dom\Madir\Direcciones\Direccion  $direccion
     * @return \Illuminate\Http\Response
     */
    public function show(Direccion $direccion) {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Dom\Madir\Direcciones\Direccion  $direccion
     * @return \Illuminate\Http\Response
     */
    public function edit(Direccion $direccion) {
        $types = [];
        $typeDef = "";
        $parents = isset($direccion->segmento) ? [["nombre" => $direccion->segmento->segmentoF, "id" => $direccion->segmento->id]] : [];
        $parentSelDef = isset($direccion->segmento) ? $direccion->segmento->id : "";
        $terrenoSelDef = isset($direccion->terreno) ? $direccion->terreno->id : "";
        $terrenos = isset($direccion->terreno) ? [["nombre" => $direccion->terreno->terrenoF, "id" => $direccion->terreno->id]] : [];
        return view("dom.madir.direcciones.maestro.edit", compact("direccion", "types", "typeDef", "parents", "parentSelDef", "terrenos", "terrenoSelDef"));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Dom\Madir\Direcciones\Direccion  $direccion
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Direccion $direccion) {
        //validate
        $this->validate($request, [
            "numero" => "required|unique_with:" . with(new Direccion)->getTable() . ",vial_id, " . $direccion->id . "|max:10",
            "numero_postal" => "nullable|max:10",
            "vial_id" => "required",
            "terreno_id" => "nullable",
        ]);
        //save
        $d = $direccion;
        $d->numero = $request->numero;
        $d->numero_postal = $request->numero_postal;
        $d->vial_id = $request->vial_id;
        $d->status = $request->status;
        $d->terreno_id = empty($request->terreno_id) ? null : $request->terreno_id;
        $d->save();
        //message & redirect
        session()->flash("message", "Dirección actualizada.");
        return redirect(route("madir.maestro.direcciones.edit", compact("direccion")));
    }

    /**
     * 
     * @param Direccion $direccion
     * @return \Illuminate\Routing\Redirector | \Illuminate\Http\RedirectResponse
     */
    public function switchStatus(Direccion $direccion) {
        $direccion->status = !$direccion->status;
        $direccion->save();
        // flash & redirect
        session()->flash("message", sprintf("Dirección %s.", $direccion->status ? "activada" : "desactivada"));
        return redirect(route("madir.maestro.direcciones.index"));
    }

    /**
     * 
     * @param Direccion $direccion
     * @return \Illuminate\Routing\Redirector | \Illuminate\Http\RedirectResponse
     */
    public function delete(Direccion $direccion) {
        $direccion->delete();
        // flash & redirect
        session()->flash("message", "Dirección eliminada.");
        return redirect(route("madir.maestro.direcciones.index"));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Dom\Madir\Direcciones\Direccion  $direccion
     * @return \Illuminate\Http\Response
     */
    public function destroy(Direccion $direccion) {
        //
    }

}
