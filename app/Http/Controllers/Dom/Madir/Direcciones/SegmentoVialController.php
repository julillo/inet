<?php

namespace App\Http\Controllers\Dom\Madir\Direcciones;

use App\Dom\Madir\Direcciones\SegmentoVial;
use App\Dom\Madir\Direcciones\TipoVial;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class SegmentoVialController extends Controller {

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        return view("dom.madir.direcciones.segmentos-viales.index");
    }

    /**
     * 
     * @param \Yajra\Datatables\Datatables $datatables
     * @return \Illuminate\Http\JsonResponse
     */
    public function indexData(\Yajra\Datatables\Datatables $datatables) {
        // get data
        $segmentosViales = SegmentoVial::with(['base', 'tipo', 'terreno'])->get();
        // bind table
        return $datatables->collection($segmentosViales)
                        ->setRowClass(function ($segmentoVial) {
                            return $segmentoVial->status ? '' : 'alert-danger';
                        })
                        ->addColumn('base', function($segmentoVial) {
                            return $segmentoVial->base->nombre;
                        })
                        ->addColumn('tipo', function($segmentoVial) {
                            return $segmentoVial->tipo->nombre;
                        })
                        ->addColumn('terreno', function($segmentoVial) {
                            return isset($segmentoVial->terreno) ? $segmentoVial->terreno->terrenoF : "";
                        })
                        ->addColumn('action', function($segmentoVial) {
                            return view("dom.madir.direcciones.segmentos-viales.actions.index-table", compact("segmentoVial"));
                        })
                        ->make(true);
    }

    /**
     * 
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function find(Request $request) {
        setlocale(LC_ALL, "en_US.utf8");
        $term = trim($request->q);

        if (empty($term)) {
            return \Response::json([]);
        }

        $tags = SegmentoVial::search($term)->limit(5)->get();

        $formatted_tags = [];

        foreach ($tags as $tag) {
            $formatted_tags[] = [
                'id' => $tag->id,
                'text' => $tag->segmentoF];
        }

        return \Response::json($formatted_tags);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        $types = TipoVial::where("status", true)->pluck("nombre", "id")->prepend("Seleccione...", "");
        $typeDef = "";
        $baseDefId = "";
        $baseDefArray = [];
        $terrenoSelDef = "";
        $terrenos = [];
        return view("dom.madir.direcciones.segmentos-viales.create", compact("baseDefId", "baseDefArray", "types", "typeDef", "terrenoSelDef", "terrenos"));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        //validate
        $this->validate($request, [
            "numero_inicio" => "nullable|unique_with:" . with(new SegmentoVial)->getTable() . ",numero_fin,base_id,tipo_id,terreno_id",
            "numero_fin" => "nullable",
            "base_id" => "required",
            "tipo_id" => "required",
            "terreno_id" => "nullable",
        ]);
        //save
        $segmento = new SegmentoVial();
        $segmento->numero_inicio = $request->numero_inicio;
        $segmento->numero_fin = $request->numero_fin;
        $segmento->base_id = empty($request->base_id) ? null : $request->base_id;
        $segmento->tipo_id = empty($request->tipo_id) ? null : $request->tipo_id;
        $segmento->terreno_id = empty($request->terreno_id) ? null : $request->terreno_id;
        $segmento->save();
        //message & redirect
        session()->flash("message", "Segmento creado.");
        return redirect(route("madir.segmentos.viales.create"));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Dom\Madir\Direcciones\SegmentoVial  $segmentoVial
     * @return \Illuminate\Http\Response
     */
    public function show(SegmentoVial $segmentoVial) {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Dom\Madir\Direcciones\SegmentoVial  $segmentoVial
     * @return \Illuminate\Http\Response
     */
    public function edit(SegmentoVial $segmentoVial) {
        $types = TipoVial::where("status", true)->pluck("nombre", "id")->prepend("Seleccione...", "");
        $typeDef = isset($segmentoVial->tipo_id) ? $segmentoVial->tipo_id : '';
        $baseDefId = $segmentoVial->base->id;
        $baseDefArray = $segmentoVial->base->pluck("nombre", "id");
        $terrenoSelDef = isset($segmentoVial->terreno) ? $segmentoVial->terreno->id : "";
        $terrenos = isset($segmentoVial->terreno) ? [["nombre" => $segmentoVial->terreno->terrenoF, "id" => $segmentoVial->terreno->id]] : [];
        return view("dom.madir.direcciones.segmentos-viales.edit", compact("segmentoVial", "baseDefId", "baseDefArray", "types", "typeDef", "terrenoSelDef", "terrenos"));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Dom\Madir\Direcciones\SegmentoVial  $segmentoVial
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, SegmentoVial $segmentoVial) {
        //validate
        $this->validate($request, [
            "numero_inicio" => "nullable|unique_with:" . with(new SegmentoVial)->getTable() . ",numero_fin,base_id,tipo_id,terreno_id," . $segmentoVial->id,
            "numero_fin" => "nullable",
            "base_id" => "required",
            "tipo_id" => "required",
            "terreno_id" => "nullable",
        ]);
        //save
        $segmento = $segmentoVial;
        $segmento->numero_inicio = $request->numero_inicio;
        $segmento->numero_fin = $request->numero_fin;
        $segmento->base_id = empty($request->base_id) ? null : $request->base_id;
        $segmento->tipo_id = empty($request->tipo_id) ? null : $request->tipo_id;
        $segmento->terreno_id = empty($request->terreno_id) ? null : $request->terreno_id;
        $segmento->save();
        //message & redirect
        session()->flash("message", "Segmento creado.");
        return redirect(route("madir.segmentos.viales.edit", compact("segmentoVial")));
    }

    /**
     * 
     * @param SegmentoVial $segmentoVial
     * @return \Illuminate\Routing\Redirector | \Illuminate\Http\RedirectResponse
     */
    public function switchStatus(SegmentoVial $segmentoVial) {
        $segmentoVial->status = !$segmentoVial->status;
        $segmentoVial->save();
        // flash & redirect
        session()->flash("message", sprintf("Segmento vial %s.", $segmentoVial->status ? "activado" : "desactivado"));
        return redirect(route("madir.segmentos.viales.index"));
    }

    /**
     * 
     * @param SegmentoVial $segmentoVial
     * @return \Illuminate\Routing\Redirector | \Illuminate\Http\RedirectResponse
     */
    public function delete(SegmentoVial $segmentoVial) {
        $segmentoVial->delete();
        // flash & redirect
        session()->flash("message", "Segmento vial eliminado.");
        return redirect(route("madir.segmentos.viales.index"));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Dom\Madir\Direcciones\SegmentoVial  $segmentoVial
     * @return \Illuminate\Http\Response
     */
    public function destroy(SegmentoVial $segmentoVial) {
        //
    }

}
