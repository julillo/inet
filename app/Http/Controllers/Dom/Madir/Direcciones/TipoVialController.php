<?php

namespace App\Http\Controllers\Dom\Madir\Direcciones;

use App\Dom\Madir\Direcciones\TipoVial;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Yajra\Datatables\Datatables;

class TipoVialController extends Controller {

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        $roots = TipoVial::where('parent_id', '=', null)->get();
        return view("dom.madir.direcciones.tipos-viales.index", compact("roots"));
    }

    /**
     * 
     * @param \Yajra\Datatables\Datatables $datatables
     * @return \Illuminate\Http\JsonResponse
     */
    public function indexData(Datatables $datatables) {
        // get data
        $tiposViales = TipoVial::with('super')->get();
        // bind table
        return $datatables->collection($tiposViales)
                        ->setRowClass(function ($tipoVial) {
                            return $tipoVial->status ? '' : 'alert-danger';
                        })
                        ->addColumn('super', function($tipoVial) {
                            return $tipoVial->super ? $tipoVial->super->nombre : '';
                        })
                        ->addColumn('action', function($tipoVial) {
                            return view("dom.madir.direcciones.tipos-viales.actions.index-table", compact("tipoVial"));
                        })
                        ->make(true);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        $parents = TipoVial::where("status", true)->pluck("nombre", "id")->prepend("Seleccione...", "");
        $parentSelDef = "";
        return view("dom.madir.direcciones.tipos-viales.create", compact("parents", "parentSelDef"));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        // validate
        $this->validate($request, [
            "nombre" => "required|unique:" . with(new TipoVial)->getTable() . "|max:191",
            "abr" => "nullable|unique:" . with(new TipoVial)->getTable() . "|max:10",
        ]);
        // store
        $tipo = new TipoVial();
        $tipo->nombre = $request->nombre;
        $tipo->abr = $request->abr;
        $tipo->parent_id = empty($request->parent_id) ? null : $request->parent_id;
        $tipo->save();
        // flash & redirect
        session()->flash("message", "Tipo creado.");
        return redirect(route("madir.tipos.viales.create"));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Dom\Madir\Direcciones\TipoVial  $tipoVial
     * @return \Illuminate\Http\Response
     */
    public function show(TipoVial $tipoVial) {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Dom\Madir\Direcciones\TipoVial  $tipoVial
     * @return \Illuminate\Http\Response
     */
    public function edit(TipoVial $tipoVial) {
        $parents = TipoVial::where([
                            ["status", true],
                            ["id", "<>", $tipoVial->id]
                        ])
                        ->pluck("nombre", "id")->prepend("Seleccione...", "");
        $parentSelDef = isset($tipoVial->parent_id) ? $tipoVial->parent_id : '';
        return view("dom.madir.direcciones.tipos-viales.edit", compact("tipoVial", "parents", "parentSelDef"));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Dom\Madir\Direcciones\TipoVial  $tipoVial
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, TipoVial $tipoVial) {
        // validate
        $this->validate($request, [
            "nombre" => "required|unique:" . with(new TipoVial)->getTable() . ",nombre," . $tipoVial->id . "|max:191",
            "abr" => "nullable|unique:" . with(new TipoVial)->getTable() . ",abr," . $tipoVial->id . "|max:10",
        ]);
        // store
        $tipo = $tipoVial;
        $tipo->nombre = $request->nombre;
        $tipo->abr = $request->abr;
        $tipo->status = $request->status;
        $tipo->parent_id = empty($request->parent_id) ? null : $request->parent_id;
        $tipo->save();
        // flash & redirect
        session()->flash("message", "Tipo editado.");
        return redirect(route("madir.tipos.viales.edit", compact("tipoVial")));
    }

    public function switchStatus(TipoVial $tipoVial) {
        $tipoVial->status = !$tipoVial->status;
        $tipoVial->save();
        // flash & redirect
        session()->flash("message", sprintf("Tipo vial %s.", $tipoVial->status ? "activado" : "desactivado"));
        return redirect(route("madir.tipos.viales.index"));
    }

    public function delete(TipoVial $tipoVial) {
        $tipoVial->delete();
        // flash & redirect
        session()->flash("message", "Tipo vial eliminado.");
        return redirect(route("madir.tipos.viales.index"));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Dom\Madir\Direcciones\TipoVial  $tipoVial
     * @return \Illuminate\Http\Response
     */
    public function destroy(TipoVial $tipoVial) {
        //
    }

}
