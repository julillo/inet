<?php

namespace App\Http\Controllers\Dom\Madir\Terrenos;

use App\Dom\Madir\Terrenos\Terreno;
use App\Dom\Madir\Terrenos\TipoTerreno;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class TerrenoController extends Controller {

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        $roots = Terreno::where('parent_id', '=', null)->get();
        return view("dom.madir.terrenos.maestro.index", compact("roots"));
    }

    /**
     * 
     * @param \Yajra\Datatables\Datatables $datatables
     * @return \Illuminate\Http\JsonResponse
     */
    public function indexData(\Yajra\Datatables\Datatables $datatables) {
        setlocale(LC_ALL, "en_US.utf8");
        // get data
        $terrenos = Terreno::with(['super', 'tipo'])->select("madir_terrenos.*")->get();
        // bind table
        return $datatables->collection($terrenos)
                        ->setRowClass(function ($terreno) {
                            return $terreno->status ? '' : 'alert-danger';
                        })
                        ->addColumn('tipo', function($terreno) {
                            return isset($terreno->tipo) ? $terreno->tipo->nombre : '';
                        })
                        ->addColumn('rolf', function($terreno) {
                            return $terreno->rolF;
                        })
                        ->addColumn('super', function($terreno) {
                            return isset($terreno->super) ? $terreno->super->nombre : '';
                        })
                        ->addColumn('action', function($terreno) {
                            return view("dom.madir.terrenos.maestro.actions.index-table", compact("terreno"));
                        })
                        ->make(true);
    }

    /**
     * 
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function find(Request $request) {
        setlocale(LC_ALL, "en_US.utf8");
        $term = trim($request->q);

        if (empty($term)) {
            return \Response::json([]);
        }

        $tags = Terreno::search($term)->limit(5)->get();

        $formatted_tags = [];

        foreach ($tags as $tag) {
            $formatted_tags[] = [
                'id' => $tag->id,
                'text' => $tag->terrenoF];
        }

        return \Response::json($formatted_tags);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Terreno $terreno) {
        $types = TipoTerreno::where("status", true)->pluck("nombre", "id")->prepend("Seleccione...", "");
        $typeDef = "";
        $parents = isset($terreno->id) ? [["nombre" => $terreno->terrenoF, "id" => $terreno->id]] : [];
        $parentSelDef = isset($terreno->id) ? $terreno->id : "";
        return view("dom.madir.terrenos.maestro.create", compact("types", "typeDef", "parents", "parentSelDef"));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        //validate
        $this->validate($request, [
            "rol_manzana" => "nullable|unique_with:" . with(new Terreno)->getTable() . ",rol_predio|max:5",
            "rol_predio" => "nullable|max:3",
            "nombre" => "nullable|unique_with:" . with(new Terreno)->getTable() . ",tipo_id,parent_id|max:191",
            "tipo_id" => "required",
            "parent_id" => "nullable",
        ]);
        //save
        $p = new Terreno();
        $p->rol_manzana = $request->rol_manzana;
        $p->rol_predio = $request->rol_predio;
        $p->nombre = $request->nombre;
        $p->tipo_id = empty($request->tipo_id) ? null : $request->tipo_id;
        $p->parent_id = empty($request->parent_id) ? null : $request->parent_id;
        $p->save();
        //message & redirect
        session()->flash("message", "Terreno creado.");
        return redirect(route("madir.maestro.territoriales.create"));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Dom\Madir\Terrenos\Terreno  $terreno
     * @return \Illuminate\Http\Response
     */
    public function show(Terreno $terreno) {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Dom\Madir\Terrenos\Terreno  $terreno
     * @return \Illuminate\Http\Response
     */
    public function edit(Terreno $terreno) {
        $types = TipoTerreno::where("status", true)->pluck("nombre", "id")->prepend("Seleccione...", "");
        $typeDef = isset($terreno->tipo) ? $terreno->tipo->id : "";
        $parents = isset($terreno->super) ? [["nombre" => $terreno->super->terrenoF, "id" => $terreno->super->id]] : [];
        $parentSelDef = isset($terreno->super) ? $terreno->super->id : "";
        return view("dom.madir.terrenos.maestro.edit", compact("terreno", "types", "typeDef", "parents", "parentSelDef"));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Dom\Madir\Terrenos\Terreno  $terreno
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Terreno $terreno) {
        //validate
        $this->validate($request, [
            "rol_manzana" => "nullable|unique_with:" . with(new Terreno)->getTable() . ",rol_predio," . $terreno->id . "|max:5",
            "rol_predio" => "nullable|max:3",
            "nombre" => "nullable|unique_with:" . with(new Terreno)->getTable() . ",tipo_id,parent_id," . $terreno->id . "|max:191",
            "tipo_id" => "required",
            "parent_id" => "nullable",
        ]);
        //save
        $p = $terreno;
        $p->rol_manzana = $request->rol_manzana;
        $p->rol_predio = $request->rol_predio;
        $p->nombre = $request->nombre;
        $p->status = empty($request->status) ? null : $request->status;
        $p->tipo_id = empty($request->tipo_id) ? null : $request->tipo_id;
        $p->parent_id = empty($request->parent_id) ? null : $request->parent_id;
        $p->save();
        //message & redirect
        session()->flash("message", "Terreno creado.");
        return redirect(route("madir.maestro.territoriales.edit", compact("terreno")));
    }

    /**
     * 
     * @param Terreno $terreno
     * @return \Illuminate\Routing\Redirector | \Illuminate\Http\RedirectResponse
     */
    public function switchStatus(Terreno $terreno) {
        $terreno->status = !$terreno->status;
        $terreno->save();
        // flash & redirect
        session()->flash("message", sprintf("Terreno %s.", $terreno->status ? "activado" : "desactivado"));
        return redirect(route("madir.maestro.territoriales.index"));
    }

    /**
     * 
     * @param Terreno $terreno
     * @return \Illuminate\Routing\Redirector | \Illuminate\Http\RedirectResponse
     */
    public function delete(Terreno $terreno) {
        $terreno->delete();
        // flash & redirect
        session()->flash("message", "Terreno eliminado.");
        return redirect(route("madir.maestro.territoriales.index"));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Dom\Madir\Terrenos\Terreno  $terreno
     * @return \Illuminate\Http\Response
     */
    public function destroy(Terreno $terreno) {
        //
    }

// <editor-fold defaultstate="collapsed" desc="user level">

    public function indexUser() {
        return view("dom.madir.index");
    }

    public function indexUserData(\Yajra\Datatables\Datatables $datatables) {
        $terrenos = $this->procesarTerrenos(Terreno::with("direcciones", "recursiveUp")->get());
        return $datatables->collection($terrenos)->make(true);
    }

    protected function procesarTerrenos($terrenos) {
        $col = [];
        foreach ($terrenos as $terreno) {
            if (empty($terreno->direcciones->count())) {
                $direccion = $this->procesarSuper($terreno->recursiveUp);
                if (empty($direccion)) {
                    //do nothing
                } else {
                    $this->format($terreno, $direccion, $col);
                }
            } else {
                $this->format($terreno, $terreno->direcciones, $col);
            }
        }
        return new \Illuminate\Support\Collection($col);
    }

    protected function procesarSuper($terreno) {
        if (isset($terreno) && !empty($terreno->direcciones->count())) {
            return $terreno->direcciones;
        } elseif (isset($terreno)) {
            return $this->procesarSuper($terreno->recursiveUp);
        }
    }

    protected function format($terreno, $direccion, &$array) {
        foreach ($direccion as $d) {
            $array[] = ["terreno_id" => $terreno->id,
                "terreno_rol_manzana" => $terreno->rol_manzana,
                "terreno_rol_predio" => $terreno->rol_predio,
                "terreno_tipo" => $terreno->tipo->nombre,
                "terreno_tipo_abr" => $terreno->tipo->abr,
                "terreno_nombre" => $terreno->nombre,
                "terreno_nombre_f" => $terreno->terrenoF,
                "direccion_id" => $d->id,
                "direccion_tipo" => $d->segmento->tipo->nombre,
                "direccion_nombre" => $d->segmento->base->nombre,
                "direccion_numero" => $d->numero,
                "direccion_postal" => $d->numero_postal,
                "direccion_sector" => $d->segmento->terreno->terrenoF,
            ];
        }
    }

// </editor-fold>
}
