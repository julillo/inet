<?php

namespace App\Http\Controllers\Dom\Madir\Terrenos;

use App\Dom\Madir\Terrenos\TipoTerreno;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class TipoTerrenoController extends Controller {

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        $roots = TipoTerreno::where('parent_id', '=', null)->get();
        return view("dom.madir.terrenos.tipos-territoriales.index", compact("roots"));
    }

    /**
     * 
     * @param \Yajra\Datatables\Datatables $datatables
     * @return \Illuminate\Http\JsonResponse
     */
    public function indexData(\Yajra\Datatables\Datatables $datatables) {
        // get data
        $tiposTerrenos = TipoTerreno::with('super')->get();
        // bind table
        return $datatables->collection($tiposTerrenos)
                        ->setRowClass(function ($tipoTerreno) {
                            return $tipoTerreno->status ? '' : 'alert-danger';
                        })
                        ->addColumn('super', function($tipoTerreno) {
                            return $tipoTerreno->super ? $tipoTerreno->super->nombre : '';
                        })
                        ->addColumn('action', function($tipoTerreno) {
                            return view("dom.madir.terrenos.tipos-territoriales.actions.index-table", compact("tipoTerreno"));
                        })
                        ->make(true);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        $parents = TipoTerreno::where("status", true)->pluck("nombre", "id")->prepend("Seleccione...", "");
        $parentSelDef = "";
        return view("dom.madir.terrenos.tipos-territoriales.create", compact("parents", "parentSelDef"));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        $this->validate($request, [
            "nombre" => "required|unique:" . with(new TipoTerreno)->getTable() . "|max:191",
            "abr" => "nullable|unique:" . with(new TipoTerreno)->getTable() . "|max:10",
            "descripcion" => "nullable|max:255",
        ]);
        // store
        $tipo = new TipoTerreno();
        $tipo->nombre = $request->nombre;
        $tipo->abr = $request->abr;
        $tipo->descripcion = $request->descripcion;
        $tipo->parent_id = empty($request->parent_id) ? null : $request->parent_id;
        $tipo->save();
        // flash & redirect
        session()->flash("message", "Tipo creado.");
        return redirect(route("madir.tipos.territoriales.create"));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Dom\Madir\Terrenos\TipoTerreno  $tipoTerreno
     * @return \Illuminate\Http\Response
     */
    public function show(TipoTerreno $tipoTerreno) {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Dom\Madir\Terrenos\TipoTerreno  $tipoTerreno
     * @return \Illuminate\Http\Response
     */
    public function edit(TipoTerreno $tipoTerreno) {
        $parents = TipoTerreno::where([
                            ["status", true],
                            ["id", "<>", $tipoTerreno->id]
                        ])
                        ->pluck("nombre", "id")->prepend("Seleccione...", "");
        $parentSelDef = isset($tipoTerreno->parent_id) ? $tipoTerreno->parent_id : '';
        return view("dom.madir.terrenos.tipos-territoriales.edit", compact("tipoTerreno", "parents", "parentSelDef"));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Dom\Madir\Terrenos\TipoTerreno  $tipoTerreno
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, TipoTerreno $tipoTerreno) {
        $this->validate($request, [
            "nombre" => "required|unique:" . with(new TipoTerreno)->getTable() . ",nombre," . $tipoTerreno->id . "|max:191",
            "abr" => "nullable|unique:" . with(new TipoTerreno)->getTable() . ",abr," . $tipoTerreno->id . "|max:10",
            "descripcion" => "max:255",
        ]);
        // store
        $tipo = $tipoTerreno;
        $tipo->nombre = $request->nombre;
        $tipo->abr = $request->abr;
        $tipo->descripcion = $request->descripcion;
        $tipo->status = $request->status;
        $tipo->parent_id = empty($request->parent_id) ? null : $request->parent_id;
        $tipo->save();
        // flash & redirect
        session()->flash("message", "Tipo editado.");
        return redirect(route("madir.tipos.territoriales.edit", compact("tipoTerreno")));
    }

    /**
     * 
     * @param TipoTerreno $tipoTerreno
     * @return \Illuminate\Routing\Redirector | \Illuminate\Http\RedirectResponse
     */
    public function switchStatus(TipoTerreno $tipoTerreno) {
        $tipoTerreno->status = !$tipoTerreno->status;
        $tipoTerreno->save();
        // flash & redirect
        session()->flash("message", sprintf("Tipo territorial %s.", $tipoTerreno->status ? "activado" : "desactivado"));
        return redirect(route("madir.tipos.territoriales.index"));
    }

    /**
     * 
     * @param TipoTerreno $tipoTerreno
     * @return \Illuminate\Routing\Redirector | \Illuminate\Http\RedirectResponse
     */
    public function delete(TipoTerreno $tipoTerreno) {
        $tipoTerreno->delete();
        // flash & redirect
        session()->flash("message", "Tipo territorial eliminado.");
        return redirect(route("madir.tipos.territoriales.index"));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Dom\Madir\Terrenos\TipoTerreno  $tipoTerreno
     * @return \Illuminate\Http\Response
     */
    public function destroy(TipoTerreno $tipoTerreno) {
        //
    }

}
