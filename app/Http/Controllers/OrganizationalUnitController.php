<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;
use App\OrganizationalUnit;
use App\OrganizationalUnitType;

class OrganizationalUnitController extends Controller {

    public function __construct() {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        $roots = OrganizationalUnit::where('parent_id', '=', null)->get();
        return view("organizational-unit.units.index", compact("roots"));
    }

    public function indexData(Datatables $datatables) {
        // get data
        $ous = OrganizationalUnit::with('super', 'type')->select('organizational_units.*');
        // bind table
        return $datatables->eloquent($ous)
                        ->setRowClass(function ($ou) {
                            return $ou->status ? '' : 'alert-danger';
                        })
                        ->addColumn('type', function($ou) {
                            return $ou->type ? $ou->type->name : '';
                        })
                        ->addColumn('parent', function($ou) {
                            return $ou->super ? $ou->super->name : '';
                        })
                        ->addColumn('action', function($ou) {
                            return view("organizational-unit.units.actions.table-index", compact("ou"));
                        })
                        ->make(true);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        $types = OrganizationalUnitType::where("status", true)->pluck("name", "id")->prepend("Seleccione...", "");
        $parentUnits = OrganizationalUnit::where("status", true)->pluck("name", "id")->prepend("Seleccione...", "");
        $typesSelDef = "";
        $parentSelDef = "";
        return view("organizational-unit.units.create", compact("parentUnits", "types", "parentSelDef", "typesSelDef"));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        // validate
        $this->validate($request, [
            "name" => "required|unique:" . with(new OrganizationalUnit)->getTable() . "|max:191",
            "type_id" => "required"
        ]);
        // store
        $ou = new OrganizationalUnit();
        $ou->name = $request->name;
        $ou->short_name = $request->short_name;
        $ou->description = $request->description;
        $ou->type_id = $request->type_id;
        $ou->parent_id = empty($request->parent_id) ? null : $request->parent_id;
        $ou->save();
        // flash & redirect
        session()->flash("message", "UO creada.");
        return redirect(route("ous.create"));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\OrganizationalUnit  $organizationalUnit
     * @return \Illuminate\Http\Response
     */
    public function show(OrganizationalUnit $organizationalUnit) {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\OrganizationalUnit  $organizationalUnit
     * @return \Illuminate\Http\Response
     */
    public function edit(OrganizationalUnit $organizationalUnit) {
        $types = OrganizationalUnitType::where("status", true)
                        ->pluck("name", "id")->prepend("Seleccione...", "");
        $parentUnits = OrganizationalUnit::where([
                            ["status", true]
                            , ["id", "<>", $organizationalUnit->id]
                        ])
                        ->pluck("name", "id")->prepend("Seleccione...", "");
        $typesSelDef = isset($organizationalUnit->type_id) ? $organizationalUnit->type_id : '';
        $parentSelDef = isset($organizationalUnit->parent_id) ? $organizationalUnit->parent_id : '';
        return view("organizational-unit.units.edit", compact("organizationalUnit", "parentUnits", "types", "parentSelDef", "typesSelDef"));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\OrganizationalUnit  $organizationalUnit
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, OrganizationalUnit $organizationalUnit) {
        // validate
        $this->validate($request, [
            "name" => "required|unique:" . with(new OrganizationalUnit)->getTable() . ",name," . $organizationalUnit->id . "|max:191",
            "status" => "required",
            "type_id" => "required"
        ]);
        // store
        $ou = $organizationalUnit;
        $ou->name = $request->name;
        $ou->short_name = $request->short_name;
        $ou->description = $request->description;
        $ou->status = $request->status;
        $ou->type_id = $request->type_id;
        $ou->parent_id = empty($request->parent_id) ? null : $request->parent_id;
        $ou->save();
        // flash & redirect
        session()->flash("message", "UO actualizada.");
        return redirect(route("ous.edit", $ou));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\OrganizationalUnit  $organizationalUnit
     * @return \Illuminate\Http\Response
     */
    public function destroy(OrganizationalUnit $organizationalUnit) {
        //
    }

}
