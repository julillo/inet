<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;
use App\OrganizationalUnitType;

class OrganizationalUnitTypeController extends Controller {

    public function __construct() {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        $roots = OrganizationalUnitType::where('parent_id', '=', null)->get();
        return view("organizational-unit.types.index", compact("roots"));
    }

    public function indexData(Datatables $datatables) {
        // get data
        $ouTypes = OrganizationalUnitType::with('super')->select('organizational_unit_types.*');
        // bind table
        return $datatables->eloquent($ouTypes)
                        ->setRowClass(function ($ouType) {
                            return $ouType->status ? '' : 'alert-danger';
                        })
                        ->addColumn('parent', function($ouType) {
                            return $ouType->super ? $ouType->super->name : '';
                        })
                        ->addColumn('action', function($ouType) {
                            return view("organizational-unit.types.actions.table-index", compact("ouType"));
                        })
                        ->make(true);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        $parentTypes = OrganizationalUnitType::where("status", true)
                        ->pluck("name", "id")->prepend("Seleccione...", "");
        $parentSelDef = "";
        return view("organizational-unit.types.create", compact("parentTypes", "parentSelDef"));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        // validate
        $this->validate($request, [
            "name" => "required|unique:" . with(new OrganizationalUnitType)->getTable() . "|max:191"
        ]);
        // store
        $outype = new OrganizationalUnitType();
        $outype->name = $request->name;
        $outype->short_name = $request->short_name;
        $outype->description = $request->description;
        $outype->parent_id = empty($request->parent_id) ? null : $request->parent_id;
        $outype->save();
        // flash & redirect
        session()->flash("message", "Tipo UO creada.");
        return redirect(route("ou-types.create"));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\OrganizationalUnitType  $organizationalUnitType
     * @return \Illuminate\Http\Response
     */
    public function show(OrganizationalUnitType $organizationalUnitType) {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\OrganizationalUnitType  $organizationalUnitType
     * @return \Illuminate\Http\Response
     */
    public function edit(OrganizationalUnitType $organizationalUnitType) {
        $parentTypes = OrganizationalUnitType::where([
                            ["status", true]
                            , ["id", "<>", $organizationalUnitType->id]
                        ])
                        ->pluck("name", "id")->prepend("Seleccione...", "");
        $parentSelDef = isset($organizationalUnitType->parent_id) ? $organizationalUnitType->parent_id : '';
        return view("organizational-unit.types.edit", compact("organizationalUnitType", "parentTypes", "parentSelDef"));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\OrganizationalUnitType  $organizationalUnitType
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, OrganizationalUnitType $organizationalUnitType) {
        // validate
        $this->validate($request, [
            "name" => "required|unique:" . with(new OrganizationalUnitType)->getTable() . ",name," . $organizationalUnitType->id . "|max:191"
        ]);
        // store
        $outype = $organizationalUnitType;
        $outype->name = $request->name;
        $outype->short_name = $request->short_name;
        $outype->description = $request->description;
        $outype->status = $request->status;
        $outype->parent_id = $request->parent_id ? $request->parent_id : null;
        $outype->save();
        // flash & redirect
        session()->flash("message", "Tipo UO actualizada.");
        return redirect(route("ou-types.edit", $outype));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\OrganizationalUnitType  $organizationalUnitType
     * @return \Illuminate\Http\Response
     */
    public function destroy(OrganizationalUnitType $organizationalUnitType) {
        //
    }

}
