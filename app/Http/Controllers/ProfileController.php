<?php

namespace App\Http\Controllers;

use App\Profile;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Laratrust\Laratrust;

class ProfileController extends Controller {

    public function __construct() {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Profile  $profile
     * @return \Illuminate\Http\Response
     */
    public function showOwn(Profile $profile) {
        if (!Auth::user()->owns($profile)) {
            return redirect("home");
        }
        return view("users.profile.show", compact("profile"));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Profile  $profile
     * @return \Illuminate\Http\Response
     */
    public function edit(Profile $profile) {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Profile  $profile
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Profile $profile) {
        // validate
        $rules = [
            'nombres' => 'nullable|max:191',
            'apellido_p' => 'nullable|max:191',
            'apellido_m' => 'nullable|max:191',
        ];
        $this->validate($request, $rules);
        // store
        $profile->nombres = $request->get('nombres');
        $profile->apellido_p = $request->get('apellido_p');
        $profile->apellido_m = $request->get('apellido_m');
        $profile->save();
        // redirect
        session()->flash("message", "Perfil actualizado exitosamente!");

        return redirect(route("users.edit", $profile->user));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Profile  $profile
     * @return \Illuminate\Http\Response
     */
    public function destroy(Profile $profile) {
        //
    }

}
