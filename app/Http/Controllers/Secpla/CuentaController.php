<?php

namespace App\Http\Controllers\Secpla;

use App\Secpla\Cuenta;
use App\Secpla\Tipos\Cuenta as TipoCuenta;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;

class CuentaController extends Controller {

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        return view("secpla.cuentas.index");
    }

    public function indexData(Datatables $datatables) {
        // get data
        $cuentas = Cuenta::all();
        // bind table
        return $datatables->collection($cuentas)
                        ->setRowClass(function ($cuenta) {
                            return $cuenta->status ? '' : 'alert-danger';
                        })
                        ->addColumn('codigo', function($cuenta) {
                            return $cuenta->codigo;
                        })
                        ->addColumn('tipo', function($cuenta) {
                            return $cuenta->tipo ? $cuenta->tipo->nombre : '';
                        })
                        ->addColumn('action', function($cuenta) {
                            return view("secpla.cuentas.actions.index-table", compact("cuenta"));
                        })
                        ->make(true);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        $types = TipoCuenta::where("status", true)->pluck("nombre", "id")->prepend("Seleccione...", "");
        $typeDef = "";
        return view("secpla.cuentas.create", compact("types", "typeDef"));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        //validate
        $this->validate($request, [
            "titulo" => "required|unique_with:" . with(new Cuenta)->getTable() . ",subtitulo,item,asignacion,subasignacion,subsubasignacion|max:3",
            "subtitulo" => "required|max:2",
            "item" => "required|max:2",
            "asignacion" => "required|max:3",
            "subasignacion" => "required|max:3",
            "subsubasignacion" => "required|max:3",
            "glosa" => "max:191"
        ]);
        //save
        $c = new Cuenta();
        $c->titulo = $request->titulo;
        $c->subtitulo = $request->subtitulo;
        $c->item = $request->item;
        $c->asignacion = $request->asignacion;
        $c->subasignacion = $request->subasignacion;
        $c->subsubasignacion = $request->subsubasignacion;
        $c->glosa = $request->glosa;
        $c->tipo_cuenta_id = empty($request->tipo_cuenta_id) ? null : $request->tipo_cuenta_id;
        $c->save();
        //message & redirect
        session()->flash("message", "Cuenta creada");
        return redirect(route("secpla.cuentas.create"));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Secpla\Cuenta  $cuenta
     * @return \Illuminate\Http\Response
     */
    public function show(Cuenta $cuenta) {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Secpla\Cuenta  $cuenta
     * @return \Illuminate\Http\Response
     */
    public function edit(Cuenta $cuenta) {
        $types = TipoCuenta::where("status", true)->pluck("nombre", "id")->prepend("Seleccione...", "");
        $typeDef = isset($cuenta->tipo_cuenta_id) ? $cuenta->tipo_cuenta_id : '';
        return view("secpla.cuentas.edit", compact("cuenta", "types", "typeDef"));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Secpla\Cuenta  $cuenta
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Cuenta $cuenta) {
        //validate
        $this->validate($request, [
            "titulo" => "required|unique_with:" . with(new Cuenta)->getTable() . ",subtitulo,item,asignacion,subasignacion,subsubasignacion," . $cuenta->id . "|max:3",
            "subtitulo" => "required|max:2",
            "item" => "required|max:2",
            "asignacion" => "required|max:3",
            "subasignacion" => "required|max:3",
            "subsubasignacion" => "required|max:3",
            "glosa" => "max:191"
        ]);
        //save
        $c = $cuenta;
        $c->titulo = $request->titulo;
        $c->subtitulo = $request->subtitulo;
        $c->item = $request->item;
        $c->asignacion = $request->asignacion;
        $c->subasignacion = $request->subasignacion;
        $c->subsubasignacion = $request->subsubasignacion;
        $c->glosa = $request->glosa;
        $c->status = $request->status;
        $c->tipo_cuenta_id = empty($request->tipo_cuenta_id) ? null : $request->tipo_cuenta_id;
        $c->save();
        //message & redirect
        session()->flash("message", "Cuenta actualizada");
        return redirect(route("secpla.cuentas.edit", compact("cuenta")));
    }

    public function switchStatus(Cuenta $cuenta) {
        $cuenta->status = !$cuenta->status;
        $cuenta->save();
        // flash & redirect
        session()->flash("message", sprintf("Cuenta %s.", $cuenta->status ? "activada" : "desactivada"));
        return redirect(route("secpla.cuentas.index"));
    }

    public function delete(Cuenta $cuenta) {
        $cuenta->delete();
        // flash & redirect
        session()->flash("message", "Cuenta eliminada.");
        return redirect(route("secpla.cuentas.index"));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Secpla\Cuenta  $cuenta
     * @return \Illuminate\Http\Response
     */
    public function destroy(Cuenta $cuenta) {
        //
    }

}
