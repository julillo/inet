<?php

namespace App\Http\Controllers\Secpla;

use App\Secpla\Proyecto;
use App\Secpla\ProyectoDibujo;
use App\Secpla\Tipos\EjeEstrategico;
use App\Secpla\Tipos\Proyecto as TipoProyecto;
use App\Secpla\Tipos\Financiamiento;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Cornford\Googlmapper\Facades\MapperFacade as Mapper;

//use Cornford\Googlmapper\Mapper as loquesea;

class ProyectoController extends Controller {

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {

        return view("secpla.proyectos.index");
    }

    public function indexData(\Yajra\Datatables\Datatables $datatables) {
        // get data
        $proyectos = Proyecto::with(['tipo', 'financiamiento', 'eje', 'requisitoPara', 'requiereDe']);
        // bind table
        return $datatables->eloquent($proyectos)
//                        ->editColumn('nombre', function($proyecto){
//                            return str_limit($proyecto->nombre, 20, '...');
//                        })
                        ->addColumn('eje', function($proyecto) {
                            return $proyecto->eje ? $proyecto->eje->nombre : '';
                        })
                        ->addColumn('tipo', function($proyecto) {
                            return $proyecto->tipo ? $proyecto->tipo->nombre : '';
                        })
                        ->addColumn('financiamiento', function($proyecto) {
                            return $proyecto->financiamiento ? $proyecto->financiamiento->nombre : '';
                        })
                        ->addColumn('requiere', function($proyecto) {
                            return $proyecto->requiereDe->map(function($proyecto) {
                                        return str_limit($proyecto->id, 30, '...');
                                    })->implode(', ');
                        })
                        ->addColumn('requerido', function($proyecto) {
                            return $proyecto->requisitoPara->map(function($proyecto) {
                                        return str_limit($proyecto->id, 30, '...');
                                    })->implode(', ');
                        })
                        ->addColumn('action', function($proyecto) {
                            return view("secpla.proyectos.actions.index-table", compact("proyecto"));
                        })
                        ->make(true);
    }

    public function requisitosData(\Yajra\Datatables\Datatables $datatables, Proyecto $proyecto) {
// <editor-fold defaultstate="collapsed" desc="old code">
//        $proyectos = Proyecto::where('id', '<>', $proyecto->id)->get();
//        return $datatables->collection($proyectos)
//                        ->addColumn("action", function($p) use ($proyecto) {
//                            return view("secpla.proyectos.actions.edit-table-requisitos", compact("p", "proyecto"));
//                        })
//                        ->make(true);
// </editor-fold>
        $proyectos = Proyecto::whereDoesntHave('requiereDe', function($q) use ($proyecto) {
                    $q->where('to_id', $proyecto->id);
                })->where('id', '<>', $proyecto->id)->get();
        return $datatables->collection($proyectos)
                        ->addColumn("action", function($p) use ($proyecto) {
                            return view("secpla.proyectos.actions.edit-table-requisitos", compact("p", "proyecto"));
                        })
                        ->make(true);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        $ejes = EjeEstrategico::where("status", true)->pluck("nombre", "id")->prepend("Seleccione...", "");
        $tipos = TipoProyecto::where("status", true)->pluck("nombre", "id")->prepend("Seleccione...", "");
        $tiposFin = Financiamiento::where("status", true)->pluck("nombre", "id")->prepend("Seleccione...", "");
        $ejeDef = "";
        $tipoDef = "";
        $tipoFinDef = "";
        return view("secpla.proyectos.create", compact("ejes", "tipos", "tiposFin", "ejeDef", "tipoDef", "tipoFinDef"));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        // validate
        $this->validate($request, [
            "nombre" => "required|unique:" . with(new Proyecto)->getTable() . "|max:191",
            "codigo" => "nullable|unique:" . with(new Proyecto)->getTable() . "|max:191",
        ]);
        // store
        $pro = new Proyecto();
        $pro->nombre = $request->nombre;
        $pro->codigo = $request->codigo;
        $pro->tipo_id = empty($request->tipo_id) ? null : $request->tipo_id;
        $pro->financiamiento_id = empty($request->financiamiento_id) ? null : $request->financiamiento_id;
        $pro->eje_estrategico_id = empty($request->eje_estrategico_id) ? null : $request->eje_estrategico_id;
        $pro->save();
        // flash & redirect
        session()->flash("message", "Proyecto creado.");
        return redirect(route("secpla.proyectos.edit", compact("pro")));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Secpla\Proyecto  $proyecto
     * @return \Illuminate\Http\Response
     */
    public function show(Proyecto $proyecto) {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Secpla\Proyecto  $proyecto
     * @return \Illuminate\Http\Response
     */
    public function edit(Proyecto $proyecto) {
        $ejes = EjeEstrategico::where("status", true)->pluck("nombre", "id")->prepend("Seleccione...", "");
        $tipos = TipoProyecto::where("status", true)->pluck("nombre", "id")->prepend("Seleccione...", "");
        $tiposFin = Financiamiento::where("status", true)->pluck("nombre", "id")->prepend("Seleccione...", "");
        $ejeDef = isset($proyecto->eje) ? $proyecto->eje->id : '';
        $tipoDef = isset($proyecto->tipo) ? $proyecto->tipo->id : '';
        $tipoFinDef = isset($proyecto->financiamiento) ? $proyecto->financiamiento->id : '';
        setlocale(LC_ALL, "en_US.utf8");
        Mapper::location('san alberto 389, puente alto, chile')->map([
            'ui' => true,
            'zoom' => 15,
            'center' => true,
            'marker' => true,
            'eventBeforeLoad' => '',
            'eventAfterLoad' => 'window.mapita = map;drawingManagerSetup(map);'
        ]);
        $dibujos = $proyecto->dibujos()->get()->toJson();
        return view("secpla.proyectos.edit", compact("proyecto", "dibujos", "ejes", "tipos", "tiposFin", "ejeDef", "tipoDef", "tipoFinDef"));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Secpla\Proyecto  $proyecto
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Proyecto $proyecto) {
        // validate
        $this->validate($request, [
            "nombre" => "required|unique:" . with(new Proyecto)->getTable() . ",nombre," . $proyecto->id . "|max:191",
            "codigo" => "nullable|unique:" . with(new Proyecto)->getTable() . ",codigo," . $proyecto->id . "|max:191",
        ]);
        // store
        $pro = $proyecto;
        $pro->nombre = $request->nombre;
        $pro->codigo = $request->codigo;
        $pro->tipo_id = empty($request->tipo_id) ? null : $request->tipo_id;
        $pro->financiamiento_id = empty($request->financiamiento_id) ? null : $request->financiamiento_id;
        $pro->eje_estrategico_id = empty($request->eje_estrategico_id) ? null : $request->eje_estrategico_id;
        $pro->save();
        // flash & redirect
        session()->flash("message", "Proyecto editado.");
        return redirect(route("secpla.proyectos.edit", compact("proyecto")));
    }

    public function updateRequisitos(Request $request, Proyecto $proyecto) {
        // update
        foreach ($request->requisitos as $key => $value) {
            if ($value) {
                $proyecto->requiereDe()->find($key) ?: $proyecto->requiereDe()->attach($key);
            } else {
                !$proyecto->requiereDe()->find($key) ?: $proyecto->requiereDe()->detach($key);
            }
        }
        // redirect
        session()->flash("message", "Requisitos actualizados con éxito!");
        return redirect(route("secpla.proyectos.edit", compact("proyecto")));
    }

    public function delete(Proyecto $proyecto) {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Secpla\Proyecto  $proyecto
     * @return \Illuminate\Http\Response
     */
    public function destroy(Proyecto $proyecto) {
        //
    }

    public function saveMap(Request $request, Proyecto $proyecto) {
        // clear
        $proyecto->dibujos()->delete();
        // store
        $shapes = json_decode($request->invisible);
        foreach ($shapes as $shape) {
            $pd = new ProyectoDibujo();
            $pd->type = $shape->type;
            $pd->geometry = $shape->geometry;
            $pd->radius = isset($shape->radius) ? $shape->radius : null;
            $pd->style = isset($shape->style) ? $shape->style : null;
            $pd->proyecto_id = $proyecto->id;
            $pd->save();
        }
        // flash & redirect
        session()->flash("message", "Proyecto editado.");
        return redirect(route("secpla.proyectos.edit", compact("proyecto")));
    }

}
