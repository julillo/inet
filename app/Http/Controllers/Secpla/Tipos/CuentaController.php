<?php

namespace App\Http\Controllers\Secpla\Tipos;

use App\Secpla\Tipos\Cuenta;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Yajra\Datatables\Datatables;

class CuentaController extends Controller {

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        $roots = [];
        return view("secpla.tipos.cuentas.index", compact("roots"));
    }

    /**
     * 
     * @param ajra\Datatables\Datatables $datatables
     * @return \Yajra\Datatables\Engines\CollectionEngine
     */
    public function indexData(Datatables $datatables) {
        // get data
        $cuentas = Cuenta::all();
        // bind table
        return $datatables->collection($cuentas)
                        ->setRowClass(function ($cuenta) {
                            return $cuenta->status ? '' : 'alert-danger';
                        })
                        ->addColumn('action', function($cuenta) {
                            return view("secpla.tipos.cuentas.actions.index-table", compact("cuenta"));
                        })
                        ->make(true);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        return view("secpla.tipos.cuentas.create");
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        //validate
        $this->validate($request, [
            "nombre" => "required|unique:" . with(new Cuenta)->getTable() . "|max:191",
        ]);
        //save
        $c = new Cuenta();
        $c->nombre = $request->nombre;
        $c->save();
        //message & redirect
        session()->flash("message", "Tipo creado");
        return redirect(route("secpla.tipos.cuentas.create"));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Secpla\Tipos\Cuenta  $cuenta
     * @return \Illuminate\Http\Response
     */
    public function show(Cuenta $cuenta) {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Secpla\Tipos\Cuenta  $cuenta
     * @return \Illuminate\Http\Response
     */
    public function edit(Cuenta $cuenta) {
        return view("secpla.tipos.cuentas.edit", compact("cuenta"));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Secpla\Tipos\Cuenta  $cuenta
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Cuenta $cuenta) {
        //validate
        $this->validate($request, [
            "nombre" => "required|unique:" . with(new Cuenta)->getTable() . ",nombre," . $cuenta->id . "|max:191",
            "status" => "required",
        ]);
        //save
        $c = $cuenta;
        $c->nombre = $request->nombre;
        $c->status = $request->status;
        $c->save();
        //message & redirect
        session()->flash("message", "Tipo editado");
        return redirect(route("secpla.tipos.cuentas.edit", compact("cuenta")));
    }

    /**
     * 
     * @param \App\Secpla\Tipos\Cuenta $cuenta
     * @return \Illuminate\Http\Response
     */
    public function switchStatus(Cuenta $cuenta) {
        $cuenta->status = !$cuenta->status;
        $cuenta->save();
        // flash & redirect
        session()->flash("message", sprintf("Tipo %s.", $cuenta->status ? "activado" : "desactivado"));
        return redirect(route("secpla.tipos.cuentas.index"));
    }

    /**
     * 
     * @param \App\Secpla\Tipos\Cuenta $cuenta
     * @return \Illuminate\Http\Response
     */
    public function delete(Cuenta $cuenta) {
        $cuenta->delete();
        // flash & redirect
        session()->flash("message", "Tipo eliminado.");
        return redirect(route("secpla.tipos.cuentas.index"));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Secpla\Tipos\Cuenta  $cuenta
     * @return \Illuminate\Http\Response
     */
    public function destroy(Cuenta $cuenta) {
        //
    }

}
