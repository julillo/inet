<?php

namespace App\Http\Controllers\Secpla\Tipos;

use App\Secpla\Tipos\EjeEstrategico;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Yajra\Datatables\Datatables;

class EjeEstrategicoController extends Controller {

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        $roots = EjeEstrategico::where('parent_id', '=', null)->get();
        return view("secpla.tipos.ejes.index", compact("roots"));
    }

    public function indexData(Datatables $datatables) {
        // get data
        $ejes = EjeEstrategico::with('super');
        // bind table
        return $datatables->eloquent($ejes)
                        ->setRowClass(function ($eje) {
                            return $eje->status ? '' : 'alert-danger';
                        })
                        ->addColumn('parent', function($eje) {
                            return $eje->super ? $eje->super->nombre : '';
                        })
                        ->addColumn('action', function($eje) {
                            return view("secpla.tipos.ejes.actions.index-table", compact("eje"));
                        })
                        ->make(true);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        $parents = EjeEstrategico::where("status", true)->pluck("nombre", "id")->prepend("Seleccione...", "");
        $parentSelDef = "";
        return view("secpla.tipos.ejes.create", compact("parents", "parentSelDef"));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        // validate
        $this->validate($request, [
            "nombre" => "required|unique:" . with(new EjeEstrategico)->getTable() . "|max:191",
        ]);
        // store
        $eje = new EjeEstrategico();
        $eje->nombre = $request->nombre;
        $eje->parent_id = empty($request->parent_id) ? null : $request->parent_id;
        $eje->save();
        // flash & redirect
        session()->flash("message", "Eje estratégico creado.");
        return redirect(route("secpla.tipos.ejes.create"));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Secpla\Tipos\EjeEstrategico  $ejeEstrategico
     * @return \Illuminate\Http\Response
     */
    public function show(EjeEstrategico $ejeEstrategico) {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Secpla\Tipos\EjeEstrategico  $ejeEstrategico
     * @return \Illuminate\Http\Response
     */
    public function edit(EjeEstrategico $ejeEstrategico) {
        $parents = EjeEstrategico::where([
                            ["status", true]
                            , ["id", "<>", $ejeEstrategico->id]
                        ])
                        ->pluck("nombre", "id")->prepend("Seleccione...", "");
        $parentSelDef = isset($ejeEstrategico->parent_id) ? $ejeEstrategico->parent_id : '';
        return view("secpla.tipos.ejes.edit", compact("ejeEstrategico", "parents", "parentSelDef"));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Secpla\Tipos\EjeEstrategico  $ejeEstrategico
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, EjeEstrategico $ejeEstrategico) {
        // validate
        $this->validate($request, [
            "nombre" => "required|unique:" . with(new EjeEstrategico)->getTable() . ",nombre," . $ejeEstrategico->id . "|max:191",
            "status" => "required",
        ]);
        // store
        $eje = $ejeEstrategico;
        $eje->nombre = $request->nombre;
        $eje->status = $request->status;
        $eje->parent_id = empty($request->parent_id) ? null : $request->parent_id;
        $eje->save();
        // flash & redirect
        session()->flash("message", "Eje estratégico actualizado.");
        return redirect(route("secpla.tipos.ejes.edit", compact("eje")));
    }

    public function switchStatus(EjeEstrategico $ejeEstrategico) {
        $eje = $ejeEstrategico;
        $eje->status = !$eje->status;
        $eje->save();
        // flash & redirect
        session()->flash("message", sprintf("Eje estratégico %s.", $eje->status ? "activado" : "desactivado"));
        return redirect(route("secpla.tipos.ejes.index"));
    }

    public function delete(EjeEstrategico $ejeEstrategico) {
        $ejeEstrategico->delete();
        // flash & redirect
        session()->flash("message", "Eje estratégico eliminado.");
        return redirect(route("secpla.tipos.ejes.index"));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Secpla\Tipos\EjeEstrategico  $ejeEstrategico
     * @return \Illuminate\Http\Response
     */
    public function destroy(EjeEstrategico $ejeEstrategico) {
        //
    }

}
