<?php

namespace App\Http\Controllers\Secpla\Tipos;

use App\Secpla\Tipos\Financiamiento;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class FinanciamientoController extends Controller {

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        $roots = Financiamiento::where('parent_id', '=', null)->get();
        return view("secpla.tipos.financiamiento.index", compact("roots"));
    }

    public function indexData(\Yajra\Datatables\Datatables $datatables) {
        // get data
        $tipos = Financiamiento::with('super');
        // bind table
        return $datatables->eloquent($tipos)
                        ->setRowClass(function ($tipo) {
                            return $tipo->status ? '' : 'alert-danger';
                        })
                        ->addColumn('parent', function($tipo) {
                            return $tipo->super ? $tipo->super->nombre : '';
                        })
                        ->addColumn('action', function($tipo) {
                            return view("secpla.tipos.financiamiento.actions.index-table", compact("tipo"));
                        })
                        ->make(true);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        $parents = Financiamiento::where("status", true)->pluck("nombre", "id")->prepend("Seleccione...", "");
        $parentSelDef = "";
        return view("secpla.tipos.financiamiento.create", compact("parents", "parentSelDef"));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        // validate
        $this->validate($request, [
            "nombre" => "required|unique:" . with(new Financiamiento)->getTable() . "|max:191",
        ]);
        // store
        $fin = new Financiamiento();
        $fin->nombre = $request->nombre;
        $fin->parent_id = empty($request->parent_id) ? null : $request->parent_id;
        $fin->save();
        // flash & redirect
        session()->flash("message", "Tipo creado.");
        return redirect(route("secpla.tipos.financiamiento.create"));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Secpla\Tipos\Financiamiento  $financiamiento
     * @return \Illuminate\Http\Response
     */
    public function show(Financiamiento $financiamiento) {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Secpla\Tipos\Financiamiento  $financiamiento
     * @return \Illuminate\Http\Response
     */
    public function edit(Financiamiento $financiamiento) {
        $parents = Financiamiento::where([
                            ["status", true]
                            , ["id", "<>", $financiamiento->id]
                        ])
                        ->pluck("nombre", "id")->prepend("Seleccione...", "");
        $parentSelDef = isset($financiamiento->parent_id) ? $financiamiento->parent_id : '';
        return view("secpla.tipos.financiamiento.edit", compact("financiamiento", "parents", "parentSelDef"));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Secpla\Tipos\Financiamiento  $financiamiento
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Financiamiento $financiamiento) {
        // validate
        $this->validate($request, [
            "nombre" => "required|unique:" . with(new Financiamiento)->getTable() . ",nombre," . $financiamiento->id . "|max:191",
            "status" => "required",
        ]);
        // store
        $fin = $financiamiento;
        $fin->nombre = $request->nombre;
        $fin->status = $request->status;
        $fin->parent_id = empty($request->parent_id) ? null : $request->parent_id;
        $fin->save();
        // flash & redirect
        session()->flash("message", "Tipo actualizado.");
        return redirect(route("secpla.tipos.financiamiento.edit", compact("fin")));
    }

    public function switchStatus(Financiamiento $financiamiento) {
        $financiamiento->status = !$financiamiento->status;
        $financiamiento->save();
        // flash & redirect
        session()->flash("message", sprintf("Tipo de financiamiento %s.", $financiamiento->status ? "activado" : "desactivado"));
        return redirect(route("secpla.tipos.financiamiento.index"));
    }

    public function delete(Financiamiento $financiamiento) {
        $financiamiento->delete();
        // flash & redirect
        session()->flash("message", "Tipo de financiamiento eliminado.");
        return redirect(route("secpla.tipos.financiamiento.index"));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Secpla\Tipos\Financiamiento  $financiamiento
     * @return \Illuminate\Http\Response
     */
    public function destroy(Financiamiento $financiamiento) {
        //
    }

}
