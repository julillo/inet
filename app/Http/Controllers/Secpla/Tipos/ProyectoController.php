<?php

namespace App\Http\Controllers\Secpla\Tipos;

use App\Secpla\Tipos\Proyecto as TipoProyecto;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ProyectoController extends Controller {

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        $roots = TipoProyecto::where('parent_id', '=', null)->get();
        return view("secpla.tipos.proyecto.index", compact("roots"));
    }

    public function indexData(\Yajra\Datatables\Datatables $datatables) {
        // get data
        $tipos = TipoProyecto::with('super');
        // bind table
        return $datatables->eloquent($tipos)
                        ->setRowClass(function ($tipo) {
                            return $tipo->status ? '' : 'alert-danger';
                        })
                        ->addColumn('parent', function($tipo) {
                            return $tipo->super ? $tipo->super->nombre : '';
                        })
                        ->addColumn('action', function($tipo) {
                            return view("secpla.tipos.proyecto.actions.index-table", compact("tipo"));
                        })
                        ->make(true);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        $parents = TipoProyecto::where("status", true)->pluck("nombre", "id")->prepend("Seleccione...", "");
        $parentSelDef = "";
        return view("secpla.tipos.proyecto.create", compact("parents", "parentSelDef"));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        // validate
        $this->validate($request, [
            "nombre" => "required|unique:" . with(new TipoProyecto)->getTable() . "|max:191",
        ]);
        // store
        $pro = new TipoProyecto();
        $pro->nombre = $request->nombre;
        $pro->parent_id = empty($request->parent_id) ? null : $request->parent_id;
        $pro->save();
        // flash & redirect
        session()->flash("message", "Tipo creado.");
        return redirect(route("secpla.tipos.proyecto.create"));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Secpla\Tipos\Proyecto  $proyecto
     * @return \Illuminate\Http\Response
     */
    public function show(TipoProyecto $proyecto) {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Secpla\Tipos\Proyecto  $proyecto
     * @return \Illuminate\Http\Response
     */
    public function edit(TipoProyecto $proyecto) {
        $parents = TipoProyecto::where([
                            ["status", true]
                            , ["id", "<>", $proyecto->id]
                        ])
                        ->pluck("nombre", "id")->prepend("Seleccione...", "");
        $parentSelDef = isset($proyecto->parent_id) ? $proyecto->parent_id : '';
        return view("secpla.tipos.proyecto.edit", compact("proyecto", "parents", "parentSelDef"));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Secpla\Tipos\Proyecto  $proyecto
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, TipoProyecto $proyecto) {
        // validate
        $this->validate($request, [
            "nombre" => "required|unique:" . with(new TipoProyecto)->getTable() . ",nombre," . $proyecto->id . "|max:191",
            "status" => "required",
        ]);
        // store
        $pro = $proyecto;
        $pro->nombre = $request->nombre;
        $pro->status = $request->status;
        $pro->parent_id = empty($request->parent_id) ? null : $request->parent_id;
        $pro->save();
        // flash & redirect
        session()->flash("message", "Tipo actualizado.");
        return redirect(route("secpla.tipos.proyecto.edit", compact("pro")));
    }

    public function switchStatus(TipoProyecto $proyecto) {
        $proyecto->status = !$proyecto->status;
        $proyecto->save();
        // flash & redirect
        session()->flash("message", sprintf("Tipo de proyecto %s.", $proyecto->status ? "activado" : "desactivado"));
        return redirect(route("secpla.tipos.proyecto.index"));
    }

    public function delete(TipoProyecto $proyecto) {
        $proyecto->delete();
        // flash & redirect
        session()->flash("message", "Tipo de proyecto eliminado.");
        return redirect(route("secpla.tipos.proyecto.index"));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Secpla\Tipos\Proyecto  $proyecto
     * @return \Illuminate\Http\Response
     */
    public function destroy(TipoProyecto $proyecto) {
        //
    }

}
