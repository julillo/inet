<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use App\Permission;
use App\Role;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Redirector;
use Illuminate\Support\Facades\DB;
use Yajra\Datatables\Datatables;
use Yajra\Datatables\Engines\CollectionEngine;
use function bcrypt;
use function redirect;
use function route;
use function session;
use function view;

class UserController extends Controller {

    /**
     * 
     * @access public
     */
    public function __construct() {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index() {
        return view("users.index");
    }

    /**
     * 
     * @param Datatables $datatables
     * @return CollectionEngine
     * @access public
     */
    public function data(Datatables $datatables) {
        $users = User::all();
        return $datatables->collection($users)
                        ->setRowClass(function ($user) {
                            return $user->status ? '' : 'alert-danger';
                        })
                        ->addColumn('action', function($user) {
                            return view('users.tables.actions.user-actions', compact("user"));
                        })
                        ->make(true);
    }

    /**
     * 
     * @param Datatables $datatables
     * @param User $user
     * @return CollectionEngine
     * @access public
     */
    public function roleData(Datatables $datatables, User $user) {
        $roles = Role::all();
        return $datatables->collection($roles)
                        ->addColumn("action", function($role) use ($user) {
                            return view('users.tables.actions.role-actions', compact("role", "user"));
                        })
                        ->make(true);
    }

    /**
     * 
     * @param Datatables $datatables
     * @param User $user
     * @return CollectionEngine
     * @access public
     */
    public function permissionData(Datatables $datatables, User $user) {
        $permissions = Permission::all();
        return $datatables->collection($permissions)
                        ->addColumn("action", function($permission) use ($user) {
                            return view("users.tables.actions.permission-actions", compact("permission", "user"));
                        })
                        ->make(true);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create() {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  Request  $request
     * @return Response
     */
    public function store(Request $request) {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  User  $user
     * @return Response
     */
    public function show(User $user) {
        return view("users.show")->with("user", $user);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  User  $user
     * @return Response
     */
    public function edit(User $user) {
        return view("users.edit")->with("user", $user);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Request  $request
     * @param  User  $user
     * @return Response
     */
    public function update(Request $request, User $user) {
        // validate
        $rules = [
            'name' => 'required',
            'email' => 'required|string|email|max:255|exists:users',
            'status' => 'required',
        ];
        if ($request->has("password")) {
            $rules['password'] = 'required|string|min:6|confirmed';
        }
        $this->validate($request, $rules);
        // store
        $user->name = $request->get('name');
        $user->email = $request->get('email');
        $user->status = $request->get('status');
        if ($request->has("password")) {
            $user->password = bcrypt($request->get('password'));
        }
        $user->save();
        // redirect
        session()->flash("message", "Usuario actualizado exitosamente!");

        return redirect(route("users.edit", $user));
    }

    public function updateRoles(Request $request, User $user) {
        // update
        foreach ($request->roles as $key => $value) {
            if ($value) {
                $user->roles()->find($key) ?: $user->attachRole($key);
            } else {
                !$user->roles()->find($key) ?: $user->detachRole($key);
            }
        }
        // redirect
        session()->flash("message", "Roles del usuario actualizados con éxito!");
        return redirect(route("users.edit", $user));
    }

    public function updatePermissions(Request $request, User $user) {
        // update
        foreach ($request->permissions as $key => $value) {
            if ($value) {
                $user->permissions()->find($key) ?: $user->attachPermission($key);
            } else {
                !$user->permissions()->find($key) ?: $user->detachPermission($key);
            }
        }
        // redirect
        session()->flash("message", "Permisos del usuario actualizados con éxito!");
        return redirect(route("users.edit", $user));
    }

    /**
     * Soft Deleting user
     * @param User $user
     * @return Redirector | \Illuminate\Http\RedirectResponse
     * @access public
     */
    public function delete(User $user) {
        $user->delete();
        session()->flash("message", "Usuario eliminado.");
        return redirect(route("users.index"));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  User  $user
     * @return Response
     */
    public function destroy(User $user) {
        //
    }

}
