<?php

namespace App\Listeners\User;

use Illuminate\Auth\Events\Registered;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Role;
use App\User;

class AssignUserRole
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  Registered  $event
     * @return void
     */
    public function handle(Registered $event)
    {
        $role = Role::find(3);
        $user = $event->user;
        $user->attachRole($role);
//        logger("Rol '{$role->name}' asignado al usuario '{$user->name}'");
    }
}
