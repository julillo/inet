<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class OrganizationalUnit extends Model {

    use SoftDeletes;

    protected $fillable = [
        'name', 'description', 'short_name', "status", "type_id", "parent_id"
    ];

    public function childs() {
        return $this->hasMany(self::class, 'parent_id', 'id');
    }

    public function super() {
        return $this->belongsTo(self::class, 'parent_id', 'id');
    }

    public function type() {
        return $this->hasOne(OrganizationalUnitType::class, "id", "type_id");
    }

}
