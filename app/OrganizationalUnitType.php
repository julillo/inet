<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class OrganizationalUnitType extends Model {

    use SoftDeletes;
    
    protected $fillable = [
        'name', 'description', 'short_name', "status", "parent_id"
    ];

    public function childs() {
        return $this->hasMany(self::class, 'parent_id', 'id');
    }

    public function super() {
        return $this->belongsTo(self::class, 'parent_id', 'id');
    }

    public function units() {
        return $this->belongsTo(OrganizationalUnit::class, "id", "type_id");
    }

}
