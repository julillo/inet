<?php

namespace App;

use Illuminate\Database\Eloquent\SoftDeletes;
use Laratrust\LaratrustPermission;

class Permission extends LaratrustPermission
{
    use SoftDeletes;
}
