<?php

namespace App;

use App\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Profile extends Model {

    use SoftDeletes;

    protected $primaryKey = 'user_id';
    public $incrementing = false;
    protected $fillable = [
        'nombres', 'apellido_p', 'apellido_m'
    ];

    public function user() {
        return $this->belongsTo(User::class, "user_id", "id");
    }

    public function getRouteKeyName() {
        return "user_id";
    }

}
