<?php

namespace App;

use Illuminate\Database\Eloquent\SoftDeletes;
use Laratrust\LaratrustRole;

class Role extends LaratrustRole
{
    use SoftDeletes;
}
