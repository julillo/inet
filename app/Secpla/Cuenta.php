<?php

namespace App\Secpla;

use App\Secpla\Tipos\Cuenta as TipoCuenta;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Cuenta extends Model {

    use SoftDeletes;

    protected $table = "secpla_cuentas";
    protected $fillable = [
        'titulo', 'subtitulo', 'item', 'asignacion', 'subasignacion',
        'subsubasignacion', 'glosa', 'status', 'tipo_cuenta_id'
    ];

    public function getNumeroAttribute() {
        return intval(sprintf("%s%s%s%s%s%s", $this->titulo, $this->subtitulo, $this->item, $this->asignacion, $this->subasignacion, $this->subsubasignacion));
    }

    public function getCodigoAttribute() {
        return sprintf("%s.%s.%s.%s.%s.%s", $this->tituloF, $this->subtituloF, $this->itemF, $this->asignacionF, $this->subasignacionF, $this->subsubasignacionF);
    }

    public function getTituloFAttribute() {
        return sprintf('%03d', $this->titulo);
    }

    public function getSubtituloFAttribute() {
        return sprintf('%02d', $this->subtitulo);
    }

    public function getItemFAttribute() {
        return sprintf('%02d', $this->item);
    }

    public function getAsignacionFAttribute() {
        return sprintf('%03d', $this->asignacion);
    }

    public function getSubasignacionFAttribute() {
        return sprintf('%03d', $this->subasignacion);
    }

    public function getSubsubasignacionFAttribute() {
        return sprintf('%03d', $this->subsubasignacion);
    }

    public function tipo() {
        return $this->belongsTo(TipoCuenta::class, "tipo_cuenta_id", "id");
    }

}
