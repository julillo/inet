<?php

namespace App\Secpla;

use App\Secpla\Tipos\EjeEstrategico;
use App\Secpla\Tipos\Financiamiento;
use App\Secpla\Tipos\Proyecto as TipoProyecto;
use App\Secpla\ProyectoDibujo;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Proyecto extends Model {

    use SoftDeletes;

    protected $table = "secpla_proyectos";
    protected $fillable = [
        'nombre', "codigo", "tipo_id", "eje_estrategico_id", "financiamiento_id"
    ];

    public function tipo() {
        return $this->belongsTo(TipoProyecto::class, "tipo_id", "id");
    }

    public function financiamiento() {
        return $this->belongsTo(Financiamiento::class, "financiamiento_id", "id");
    }

    public function eje() {
        return $this->belongsTo(EjeEstrategico::class, "eje_estrategico_id", "id");
    }
    
    public function dibujos(){
        return $this->hasMany(ProyectoDibujo::class, "proyecto_id", 'id');
    }

    public function requisitoPara() {
//        return $this->belongsToMany(Proyecto::class, "secpla_proyecto_requisitos", "to_id", "from_id");
//        return $this->belongsToMany(Proyecto::class, "secpla_proyecto_requisitos")->withPivot("to_id", "from_id");
        return $this->belongsToMany(Proyecto::class, "secpla_proyecto_requisitos", "to_id", "from_id")->withPivot("to_id", "from_id");
    }

    public function requiereDe() {
//        return $this->belongsToMany(Proyecto::class, "secpla_proyecto_requisitos", "from_id", "to_id");
//        return $this->belongsToMany(Proyecto::class, "secpla_proyecto_requisitos")->withPivot("from_id", "to_id");
        return $this->belongsToMany(Proyecto::class, "secpla_proyecto_requisitos", "from_id", "to_id")->withPivot("from_id", "to_id");
    }

//    public function setCodigoAttribute($value) {
//        if (empty($value)) { // will check for empty string, null values, see php.net about it
//            $this->attributes['codigo'] = NULL;
//        } else {
//            $this->attributes['codigo'] = $value;
//        }
//    }
    
//    public function setFirstNameAttribute($value)
//    {
//        $this->attributes['first_name'] = strtolower($value);
//    }

}
