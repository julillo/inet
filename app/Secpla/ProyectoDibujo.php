<?php

namespace App\Secpla;

use Illuminate\Database\Eloquent\Model;
use App\Secpla\Proyecto;

class ProyectoDibujo extends Model {

    protected $table = "secpla_proyecto_dibujos";
    protected $fillable = [
        'tipo', "geometria", "radio", "estilo", "proyecto_id"
    ];

    public function proyecto() {
        return $this->belongsTo(Proyecto::class, "proyecto_id", "id");
    }

    public function getGeometryAttribute($value) {
        return json_decode($value);
    }

    public function setGeometryAttribute($value) {
        $this->attributes['geometry'] = json_encode($value);
    }

    public function getStyleAttribute($value) {
        return json_decode($value);
    }

    public function setStyleAttribute($value) {
        $this->attributes['style'] = isset($value) && !empty($value) ? json_encode($value) : null;
    }

}
