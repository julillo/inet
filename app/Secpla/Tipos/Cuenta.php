<?php

namespace App\Secpla\Tipos;

use App\Secpla\Cuenta as CuentaSecpla;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Cuenta extends Model {

    use SoftDeletes;

    protected $table = "secpla_tipos_cuentas";
    
    protected $fillable = [
        'nombre', 'status'
    ];

    public function cuentas() {
        return $this->hasMany(CuentaSecpla::class, "tipo_cuenta_id", "id");
    }

}
