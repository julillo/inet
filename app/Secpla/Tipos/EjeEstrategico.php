<?php

namespace App\Secpla\Tipos;

use App\Secpla\Proyecto;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\SoftDeletes;

class EjeEstrategico extends Model
{
    use SoftDeletes;
    
    /**
     * La tabla en la bd.
     * @var string
     * @access protected
     */
    protected $table = "secpla_ejes_estrategicos";
    
    /**
     * Campos editables.
     * @var array
     * @access protected
     */
    protected $fillable = [
        'nombre', "status", "parent_id"
    ];

    /**
     * Retorna objetos hijos
     * @return HasMany
     * @access public
     */
    public function childs() {
        return $this->hasMany(self::class, 'parent_id', 'id');
    }

    /**
     * Retorna objeto padre
     * @return BelongsTo
     * @access public
     */
    public function super() {
        return $this->belongsTo(self::class, 'parent_id', 'id');
    }

    /**
     * Retorna los proyectos con el eje
     * @return HasMany
     * @access public
     */
    public function proyectos() {
        return $this->hasMany(Proyecto::class, "tipo_id", "id");
    }
}
