<?php

namespace App\Secpla\Tipos;

use App\Secpla\Proyecto as ProyectoSecpla;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Proyecto extends Model {

    use SoftDeletes;

    protected $table = "secpla_tipos_proyecto";
    
    /**
     * Campos editables.
     * @var array
     * @access protected
     */
    protected $fillable = [
        'nombre', "status", "parent_id"
    ];

    public function childs() {
        return $this->hasMany(self::class, 'parent_id', 'id');
    }

    public function super() {
        return $this->belongsTo(self::class, 'parent_id', 'id');
    }

    public function proyectos() {
        return $this->hasMany(ProyectoSecpla::class, "tipo_id", "id");
    }

}
