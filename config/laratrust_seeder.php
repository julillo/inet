<?php

return [
    'role_structure' => [
        'superadministrator' => [
            'users' => 'c,r,u,d',
            'acl' => 'c,r,u,d',
            'profile' => 'c,r,u,d',
            'own-profile' => 'c,r,u,d',
        ],
        'administrator' => [
            'users' => 'c,r,u',
            'profile' => 'r,u',
            'own-profile' => 'r,u',
        ],
        'user' => [
            'own-profile' => 'r,u',
        ],
        'ou-manager' => [
            'organizational-units' => 'c,r,u,d',
            'organizational-unit-types' => 'c,r,u,d',
        ],
        'secpla-admin' => [
            'secpla-admin' => 'r',
            'secpla-ejes-estrategicos' => 'c,r,u,d',
            'secpla-tipos-proyectos' => 'c,r,u,d',
            'secpla-tipos-financiamiento' => 'c,r,u,d',
            'secpla-tipos-cuentas' => 'c,r,u,d',
            'secpla-cuentas' => 'c,r,u,d',
            'secpla-proyectos' => 'r',
        ],
        'secpla-project-manager' => [
            'secpla-project-manager' => 'r',
            'secpla-proyectos' => 'c,r,u,d',
        ],
        'madir-admin' => [
            'madir-admin' => 'r',
            'madir-tipos-viales' => 'c,r,u,d',
            'madir-bases-viales' => 'c,r,u,d',
            'madir-segmentos-viales' => 'c,r,u,d',
            'madir-direcciones' => 'c,r,u,d',
            'madir-tipos-terrenos' => 'c,r,u,d',
            'madir-terrenos' => 'c,r,u,d',
        ],
        'madir-user' => [
            'madir-user' => 'r',
            'madir-direcciones' => 'r',
        ]
    ],
    'permission_structure' => [
//        'cru_user' => [
//            'profile' => 'c,r,u'
//        ],
    ],
    'permissions_map' => [
        'c' => 'create',
        'r' => 'read',
        'u' => 'update',
        'd' => 'delete'
    ]
];
