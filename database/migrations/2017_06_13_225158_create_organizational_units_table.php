<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrganizationalUnitsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('organizational_unit_types', function (Blueprint $table) {
            $table->increments('id');
            $table->string("name")->unique();
            $table->text("description")->nullable();
            $table->string("short_name")->nullable();
            $table->boolean("status")->default(1);
            $table->integer("parent_id")->unsigned()->nullable();
            $table->timestamps();
            $table->softDeletes();
            $table->foreign('parent_id')->references('id')->on('organizational_unit_types')
                    ->onUpdate('cascade')->onDelete('cascade');
        });
        
        Schema::create('organizational_units', function (Blueprint $table) {
            $table->increments('id');
            $table->string("name")->unique();
            $table->text("description")->nullable();
            $table->string("short_name")->nullable();
            $table->boolean("status")->default(1);
            $table->integer("type_id")->unsigned();
            $table->integer("parent_id")->unsigned()->nullable();
            $table->timestamps();
            $table->softDeletes();
            $table->foreign('type_id')->references('id')->on('organizational_unit_types')
                    ->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('parent_id')->references('id')->on('organizational_units')
                    ->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('organizational_units');
        Schema::dropIfExists('organizational_unit_types');
    }
}
