<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMaestroDireccionesTables extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('madir_tipos_terrenos', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nombre')->unique();
            $table->string('abr', 10)->unique()->nullable();
            $table->text('descripcion')->nullable();
            $table->boolean('status')->default(1);
            $table->timestamps();
            $table->softDeletes();
            $table->integer("parent_id")->unsigned()->nullable();
            $table->foreign('parent_id')->references('id')->on('madir_tipos_terrenos')
                    ->onUpdate('cascade')->onDelete('cascade');
        });

        Schema::create('madir_terrenos', function (Blueprint $table) {
            $table->increments('id');
            $table->string('rol_manzana', 5)->nullable();
            $table->string('rol_predio', 3)->nullable();
            $table->string('nombre')->nullable();
            $table->boolean('status')->default(1);
            $table->timestamps();
            $table->softDeletes();
            $table->integer("tipo_id")->unsigned();
            $table->integer("parent_id")->unsigned()->nullable();
            $table->foreign('tipo_id')->references('id')->on('madir_tipos_terrenos')
                    ->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('parent_id')->references('id')->on('madir_terrenos')
                    ->onUpdate('cascade')->onDelete('cascade');
            $table->unique(["rol_manzana", "rol_predio"], "UC_madir_terrenos_1");
            $table->unique(["nombre", "tipo_id", "parent_id"], "UC_madir_terrenos_2");
        });

        Schema::create('madir_bases_viales', function(Blueprint $table) {
            $table->increments('id');
            $table->string("codigo_ruta",10)->unique()->nullable();
            $table->string("nombre")->unique();
            $table->boolean('status')->default(1);
            $table->timestamps();
            $table->softDeletes();
        });

        Schema::create('madir_tipos_viales', function(Blueprint $table) {
            $table->increments('id');
            $table->string("nombre")->unique();
            $table->string("abr", 10)->unique()->nullable();
            $table->boolean('status')->default(1);
            $table->timestamps();
            $table->softDeletes();
            $table->integer("parent_id")->unsigned()->nullable();
            $table->foreign('parent_id')->references('id')->on('madir_tipos_viales')
                    ->onUpdate('cascade')->onDelete('cascade');
        });

        Schema::create('madir_segmentos_viales', function(Blueprint $table) {
            $table->increments('id');
            $table->string("numero_pre", 10)->nullable();
            $table->string("numero_inicio", 10)->nullable();
            $table->string("numero_fin", 10)->nullable();
            $table->boolean('status')->default(1);
            $table->timestamps();
            $table->softDeletes();
            $table->integer("base_id")->unsigned();
            $table->integer("tipo_id")->unsigned();
            $table->integer("terreno_id")->unsigned()->nullable();
            $table->foreign('base_id')->references('id')->on('madir_bases_viales')
                    ->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('tipo_id')->references('id')->on('madir_tipos_viales')
                    ->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('terreno_id')->references('id')->on('madir_terrenos')
                    ->onUpdate('cascade')->onDelete('cascade');
            $table->unique(["numero_inicio", "numero_fin", "base_id", "tipo_id", "terreno_id"], "UC_madir_segmentos_viales");
        });

        Schema::create('madir_direcciones', function(Blueprint $table) {
            $table->increments('id');
            $table->string("numero", 10);
            $table->string("numero_postal", 10)->nullable();
            $table->boolean('status')->default(1);
            $table->timestamps();
            $table->softDeletes();
            $table->integer("vial_id")->unsigned();
            $table->integer("terreno_id")->unsigned()->nullable();
            $table->foreign('vial_id')->references('id')->on('madir_segmentos_viales')
                    ->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('terreno_id')->references('id')->on('madir_terrenos')
                    ->onUpdate('cascade')->onDelete('cascade');
            $table->unique(["numero", "vial_id"], "UC_madir_direcciones");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::dropIfExists('madir_direcciones');
        Schema::dropIfExists('madir_segmentos_viales');
        Schema::dropIfExists('madir_bases_viales');
        Schema::dropIfExists('madir_tipos_viales');
        Schema::dropIfExists('madir_terrenos');
        Schema::dropIfExists('madir_tipos_terrenos');
    }

}
