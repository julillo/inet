<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSecplaProyectosTables extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {

        Schema::create('secpla_tipos_proyecto', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nombre')->unique();
            $table->boolean('status')->default(1);
            $table->timestamps();
            $table->softdeletes();
            $table->integer("parent_id")->unsigned()->nullable();
            $table->foreign("parent_id")->references("id")->on("secpla_tipos_proyecto")
                    ->onUpdate("cascade")->onDelete("cascade");
        });

        Schema::create('secpla_tipos_financiamiento', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nombre')->unique();
            $table->boolean('status')->default(1);
            $table->timestamps();
            $table->softdeletes();
            $table->integer("parent_id")->unsigned()->nullable();
            $table->foreign("parent_id")->references("id")->on("secpla_tipos_financiamiento")
                    ->onUpdate("cascade")->onDelete("cascade");
        });

        Schema::create('secpla_ejes_estrategicos', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nombre')->unique();
            $table->boolean('status')->default(1);
            $table->timestamps();
            $table->softdeletes();
            $table->integer("parent_id")->unsigned()->nullable();
            $table->foreign('parent_id')->references('id')->on('secpla_ejes_estrategicos')
                    ->onUpdate('cascade')->onDelete('cascade');
        });

        Schema::create('secpla_tipos_cuentas', function(Blueprint $table) {
            $table->increments('id');
            $table->string('nombre')->unique();
            $table->boolean('status')->default(1);
            $table->timestamps();
            $table->softdeletes();
        });

        Schema::create('secpla_cuentas', function(Blueprint $table) {
            $table->increments('id');
            $table->string('titulo', 3)->nullable();
            $table->string('subtitulo', 2)->nullable();
            $table->string('item', 2)->nullable();
            $table->string('asignacion', 3)->nullable();
            $table->string('subasignacion', 3)->nullable();
            $table->string('subsubasignacion', 3)->nullable();
            $table->string('glosa')->unique()->nullable();
            $table->boolean('status')->default(1);
            $table->timestamps();
            $table->softdeletes();
            $table->integer('tipo_cuenta_id')->unsigned()->nullable();
            $table->foreign('tipo_cuenta_id')->references('id')->on('secpla_tipos_cuentas')
                    ->onUpdate('cascade')->onDelete('cascade');
            $table->unique(["titulo", "subtitulo", "item", "asignacion", "subasignacion", "subsubasignacion"], "UC_secpla_cuentas_codigo");
        });

        Schema::create('secpla_proyectos', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nombre')->unique();
            $table->string('codigo')->unique()->nullable();
            $table->timestamps();
            $table->softdeletes();
            $table->integer("tipo_id")->unsigned()->nullable();
            $table->integer("eje_estrategico_id")->unsigned()->nullable();
            $table->integer("financiamiento_id")->unsigned()->nullable();
            $table->foreign('tipo_id')->references('id')->on('secpla_tipos_proyecto')
                    ->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('eje_estrategico_id')->references('id')->on('secpla_ejes_estrategicos')
                    ->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('financiamiento_id')->references('id')->on('secpla_tipos_financiamiento')
                    ->onUpdate('cascade')->onDelete('cascade');
        });

        Schema::create('secpla_proyecto_requisitos', function(Blueprint $table) {
            $table->integer("from_id")->unsigned();
            $table->integer("to_id")->unsigned();
            $table->foreign("from_id")->references("id")->on("secpla_proyectos")
                    ->onUpdate('cascade')->onDelete('cascade');
            $table->foreign("to_id")->references("id")->on("secpla_proyectos")
                    ->onUpdate('cascade')->onDelete('cascade');
            $table->primary(["from_id", "to_id"], "Pk_secpla_proyecto_requisitos");
        });

        Schema::create('secpla_proyecto_dibujos', function(Blueprint $table) {
            $table->string("type", 20);
            $table->longText("geometry");
            $table->float("radius")->unsigned()->nullable();
            $table->longText("style")->nullable();
            $table->timestamps();
            $table->integer("proyecto_id")->unsigned();
            $table->foreign("proyecto_id")->references("id")->on("secpla_proyectos")
                    ->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::dropIfExists("secpla_proyecto_dibujos");
        Schema::dropIfExists("secpla_proyecto_requisitos");
        Schema::dropIfExists('secpla_proyectos');
        Schema::dropIfExists('secpla_cuentas');
        Schema::dropIfExists('secpla_tipos_cuentas');
        Schema::dropIfExists('secpla_tipos_proyecto');
        Schema::dropIfExists('secpla_tipos_financiamiento');
        Schema::dropIfExists('secpla_ejes_estrategicos');
    }

}
