window.$ = window.jQuery = require('jquery');
window.$ = window.Tether = require("./tether.min");
window.$ = window.Bootstrap = require("bootstrap");
window.$ = window.Ie10BugWorkaround = require("./ie10-viewport-bug-workaround");
//window.$ = window.Datatables = require("datatables");
//window.$ = window.Datatables = require("datatables.net-bs");
window.$ = window.Datatables = require("datatables.net");
window.$ = window.Datatables_buttons_base = require("datatables.net-buttons");
window.$ = window.Datatables_buttons_colvis = require( 'datatables.net-buttons/js/buttons.colVis.js' ); // Column visibility
window.$ = window.Datatables_buttons_html5 = require( 'datatables.net-buttons/js/buttons.html5.js' );  // HTML 5 file export
window.$ = window.Datatables_buttons_flash = require( 'datatables.net-buttons/js/buttons.flash.js' );  // Flash file export
window.$ = window.Datatables_buttons_print = require( 'datatables.net-buttons/js/buttons.print.js' );  // Print view button
//window.$ = window.Datatables_lang = require("https://cdn.datatables.net/plug-ins/1.10.15/i18n/Spanish.json");
//window.$ = window.Datatables_custom = require("./datatables");
//window.$ = window.Octicons = require("octicons");
//window.$ = window.Glyphicons = require("glyphicons");


