var flash_message = function ($) {
    $('#flash-message').delay(500).fadeIn('normal', function () {
        $(this).delay(2500).fadeOut();
    });
}(jQuery);