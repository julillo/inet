@extends ("layouts.master")

@section ("title")
| Registrar nuevo permiso
@endsection

@section ("content")
<h1>Registrar nuevo permiso</h1>
<div class="card card-default">
    <div class="card-block">
        @include ("acl.permissions.forms.create")
    </div>
</div>
<hr>
@endsection