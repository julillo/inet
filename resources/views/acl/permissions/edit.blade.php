@extends ("layouts.master")

@section ("title")
 | Editar permiso '{{$permission->display_name}}'
@endsection

@section ("content")
<h1>Editar permiso '{{$permission->display_name}}'</h1>
<div class="card card-default">
    <div class="card-block">
        @include ("acl.permissions.forms.edit")
    </div>
</div>
<hr>
@endsection