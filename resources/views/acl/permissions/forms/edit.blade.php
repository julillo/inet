<!-- if there are creation errors, they will show here -->
{{ Html::ul($errors->all()) }}

{{ Form::model($permission, array('route' => array('permissions.update', $permission), 'method' => 'PUT')) }}

@include ("acl.permissions.forms.campos")

{{ Form::submit('Guardar', array('class' => 'btn btn-primary')) }}

{{ Form::close() }}
