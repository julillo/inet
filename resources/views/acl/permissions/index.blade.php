@extends ("layouts.master")

@section ("title")
| Lista de permisos
@endsection

@section ("content")
<h1>Lista de permisos</h1>
@include ("acl.permissions.tables.index")
<hr>
@endsection

@push ("js")
@include ("acl.permissions.tables.scripts.index")
@endpush