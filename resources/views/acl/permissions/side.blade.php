@permission('read-acl')
<h6>@icon("key", "octicon-key", ["height" => 28, "width" => 32]) Permisos</h6>
<ul class="nav nav-pills flex-column">
    @permission('read-acl')
    <li class="nav-item">
        <a class="nav-link" href="{{route("permissions.index")}}">@icon("eye", "octicon-eye") Ver permisos</a>
    </li>
    @endpermission
    @permission('create-acl')
    <li class="nav-item">
        <a class="nav-link" href="{{route("permissions.create")}}">@icon("diff-added", "octicon-diff-added") Crear permiso</a>
    </li>
    @endpermission
</ul>
<hr>
@endpermission