@permission('update-acl')
<a class="btn btn-xs btn-primary" href="{{ route("permissions.edit", $permission) }}" title="Editar">@icon('pencil', 'octicon-pencil')</a>
@endpermission
@permission("delete-acl")
<a class="btn btn-xs btn-danger" href="#" onclick="event.preventDefault();if(confirm('Eliminar permiso {{ $permission->name }}?')){document.getElementById('delete-form-{{$permission->id}}').submit();}" title="Eliminar">@icon('x', 'octicon-x')</a>
{{ Form::model($permission, array('route' => array('permissions.delete', $permission), 'method' => 'DELETE', "id" => "delete-form-".$permission->id)) }}
{{ Form::close() }}
@endpermission