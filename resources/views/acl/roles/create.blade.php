@extends ("layouts.master")

@section ("title")
 | Registrar nuevo rol
@endsection

@section ("content")
<h1>Registrar nuevo rol</h1>
<div class="card card-default">
    <div class="card-block">
        @include ("acl.roles.forms.create")
    </div>
</div>
<hr>
@endsection