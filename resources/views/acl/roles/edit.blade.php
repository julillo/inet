@extends ("layouts.master")

@section ("title")
 | Editar rol '{{$role->display_name}}'
@endsection

@section ("content")
<h1>Editar rol '{{$role->display_name}}'</h1>
<div class="card card-default">
    <div class="card-header">
        <ul class="nav nav-tabs card-header-tabs">
            <li class="nav-item">
                <a id="link1"class="nav-link active" href="#">Información Básica</a>
            </li>
            <li class="nav-item">
                <a id="link2" class="nav-link" href="#">Permisos</a>
            </li>
        </ul>
    </div>
    <div id="card1" class="card-block" >
        @include ("acl.roles.forms.edit")
    </div>
    <div id="card2" class="card-block" style="display: none;">
        @include ("acl.roles.tables.edit-permissions")
    </div>
</div>
<hr>
@endsection

@push ("js")
@include ("acl.roles.tables.scripts.edit-permissions")
@include ("acl.roles.scripts.edit")
@endpush

