<div class="form-group">
    {{ Form::label('name', 'Nombre', array("class" => "col-md-4 control-label")) }}
    <div class="col-md-6">
        {{ Form::text('name', null, array('class' => 'form-control', "required" => true)) }}
    </div>
</div>
<div class="form-group">
    {{ Form::label('display_name', 'Nombre para mostrar', array("class" => "col-md-4 control-label")) }}
    <div class="col-md-6">
        {{ Form::text('display_name', null, array('class' => 'form-control', "required" => true)) }}
    </div>
</div>
<div class="form-group">
    {{ Form::label('description', 'Descripción', array("class" => "col-md-4 control-label")) }}
    <div class="col-md-6">
        {{ Form::textarea('description', null, array('class' => 'form-control', "required" => true)) }}
    </div>
</div>