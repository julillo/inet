<!-- if there are creation errors, they will show here -->
{{ Html::ul($errors->all()) }}

{{ Form::model(array('route' => 'roles.create', 'method' => 'POST')) }}

@include ("acl.roles.forms.campos")

{{ Form::submit('Guardar', array('class' => 'btn btn-primary')) }}

{{ Form::close() }}
