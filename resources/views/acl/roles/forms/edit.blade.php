<!-- if there are creation errors, they will show here -->
{{ Html::ul($errors->all()) }}

{{ Form::model($role, array('route' => array('roles.update', $role), 'method' => 'PUT')) }}

@include ("acl.roles.forms.campos")

{{ Form::submit('Guardar', array('class' => 'btn btn-primary')) }}

{{ Form::close() }}
