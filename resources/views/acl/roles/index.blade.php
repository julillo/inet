@extends ("layouts.master")

@section ("title")
 | Lista de roles
@endsection

@section ("content")
<h1>Lista de roles</h1>
@include ("acl.roles.tables.index")
<hr>
@endsection

@push ("js")
@include ("acl.roles.tables.scripts.index")
@endpush