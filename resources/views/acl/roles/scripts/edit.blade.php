<script type="text/javascript">
    var nav = function ($) {
        $("#link1").click(function () {
            $("#card1").show();
            $("#card2").hide();
            $("#card3").hide();
            $("#link2").removeClass("active");
            $("#link3").removeClass("active");
            $(this).addClass("active")
        });
        $("#link2").click(function () {
            $("#card1").hide();
            $("#card2").show();
            $("#card3").hide();
            $("#link1").removeClass("active");
            $("#link3").removeClass("active");
            $(this).addClass("active")
        });
    }(jQuery);
</script>