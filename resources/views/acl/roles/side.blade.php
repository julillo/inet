@permission('read-acl')
<h3>@icon("checklist", "octicon-checklist", ["height" => 32, "width" => 32]) Roles</h3>
<ul class="nav nav-pills flex-column">
    @permission('read-acl')
    <li class="nav-item">
        <a class="nav-link" href="{{route("roles.index")}}">@icon("eye", "octicon-eye") Ver roles</a>
    </li>
    @endpermission
    @permission('create-acl')
    <li class="nav-item">
        <a class="nav-link" href="{{route("roles.create")}}">@icon("diff-added", "octicon-diff-added") Crear rol</a>
    </li>
    @endpermission
</ul>
<hr>
@endpermission