@permission('update-acl')
<a class="btn btn-xs btn-primary" href="{{ route("roles.edit", $role) }}" title="Editar">@icon('pencil', 'octicon-pencil')</a>
@endpermission
@permission("delete-acl")
<a class="btn btn-xs btn-danger" href="#" onclick="event.preventDefault();if(confirm('Eliminar rol {{ $role->name }}?')){document.getElementById('delete-form-{{$role->id}}').submit();}" title="Eliminar">@icon('x', 'octicon-x')</a>
{{ Form::model($role, array('route' => array('roles.delete', $role), 'method' => 'DELETE', "id" => "delete-form-".$role->id)) }}
{{ Form::close() }}
@endpermission