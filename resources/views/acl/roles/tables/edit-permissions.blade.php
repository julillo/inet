<div class="table-responsive">
    {{ Form::model($role, array('route' => array('roles.update-permissions', $role), 'method' => 'PUT')) }}
    <table id="role-permissions-table" class="table table-bordered" style="width: 100%">
        <thead>
            <tr>
                <th>#</th>
                <th>Name</th>
                <th>Description</th>
                <th>Action</th>
            </tr>
        </thead>
    </table>
    {{ Form::submit('Guardar', array('class' => 'btn btn-primary')) }}

    {{ Form::close() }}
</div>