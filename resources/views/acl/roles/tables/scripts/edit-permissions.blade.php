<script type="text/javascript">
    var edit_role_tabla_permisos = function ($) {
        $('#role-permissions-table').DataTable({
            serverSide: true,
            processing: true,
            ajax: '{{ route("roles.permissions-data", $role->id) }}',
            columns: [
                {data: 'id'},
                {data: 'display_name'},
                {data: 'description'},
                {data: 'action', orderable: false, searchable: false}
            ]
        });
    }(jQuery);
</script>