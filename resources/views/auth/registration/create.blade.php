@extends('layouts.master')

@section ("title")
| Registrar nuevo usuario
@endsection

@section('content')
<h1>Registrar nuevo usuario</h1>
<div class="card card-default">
    <div class="card-block">
        @include ("auth.registration.form")
    </div>
</div>
<hr>
@endsection
