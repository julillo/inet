@permission('update-madir-bases-viales')
<a class="btn btn-xs btn-primary" 
    href="{{ route("madir.bases.viales.edit", $baseVial) }}" 
    title="Editar">
    @icon('pencil', 'octicon-pencil')
</a>
@endpermission
@permission('update-madir-bases-viales')
@if($baseVial->status)
<button class="btn btn-xs btn-warning" 
    onclick="event.preventDefault();if(confirm('Desactivar base vial \'{{ $baseVial->nombre }}\'?')){document.getElementById('status-form-{{$baseVial->id}}').submit(); }" 
    title="Desactivar">
    @icon('alert', 'octicon-alert')
</button>
@else
<button class="btn btn-xs btn-success" 
    onclick="event.preventDefault();if(confirm('Activar base vial \'{{ $baseVial->nombre }}\'?')){document.getElementById('status-form-{{$baseVial->id}}').submit(); }" 
    title="Activar">
    @icon('alert', 'octicon-alert')
</button>
@endif
@endpermission
@permission('delete-madir-bases-viales')
<button class="btn btn-xs btn-danger" 
    onclick="event.preventDefault();if(confirm('Eliminar base vial \'{{ $baseVial->nombre }}\'?')){document.getElementById('delete-form-{{$baseVial->id}}').submit(); }" 
    title="Eliminar">
    @icon('x', 'octicon-x')
</button>
@endpermission
@permission('update-madir-bases-viales')
{{ Form::model($baseVial, array('route' => array('madir.bases.viales.switch-status', $baseVial), 'method' => 'PUT', "id" => "status-form-".$baseVial->id)) }}
{{ Form::close() }}
@endpermission
@permission('delete-madir-bases-viales')
{{ Form::model($baseVial, array('route' => array('madir.bases.viales.delete', $baseVial), 'method' => 'DELETE', "id" => "delete-form-".$baseVial->id)) }}
{{ Form::close() }}
@endpermission