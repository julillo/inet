@extends ("layouts.master")

@section ("title")
| Admin Maestro Direcciones - Registrar nueva base vial
@endsection

@section ("content")
<h1>Registrar nueva base vial</h1>
<div class="card card-default">
    <div class="card-block">
        @include ("dom.madir.direcciones.bases-viales.forms.create")
    </div>
</div>
<hr>
@endsection