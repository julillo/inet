@extends ("layouts.master")

@section ("title")
| Admin Maestro Direcciones - Editar base vial '{{ $baseVial->nombre }}'
@endsection

@section ("content")
<h1>Editar base vial '{{ $baseVial->nombre }}'</h1>
<div class="card card-default">
    <div class="card-block">
        @include ("dom.madir.direcciones.bases-viales.forms.edit")
    </div>
</div>
<hr>
@endsection