<!-- if there are creation errors, they will show here -->
{{ Html::ul($errors->all()) }}

{{ Form::model(array('route' => 'madir.bases.viales.store', 'method' => 'POST')) }}

@include ("dom.madir.direcciones.bases-viales.forms.params")

{{ Form::submit('Guardar', array('class' => 'btn btn-primary')) }}

{{ Form::close() }}