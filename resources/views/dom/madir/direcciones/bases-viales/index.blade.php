@extends ("layouts.master")

@section ("title")
| Admin Maestro Direcciones - Bases Viales
@endsection

@section ("content")
<h1>Bases Viales</h1>
@include ("dom.madir.direcciones.bases-viales.tables.index")
<hr>
@endsection

@push ("css")
@endpush

@push ("js")
@include ("dom.madir.direcciones.bases-viales.js.index-table")
@endpush