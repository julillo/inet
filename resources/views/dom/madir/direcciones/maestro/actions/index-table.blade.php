@permission('update-madir-tipos-viales')
<a class="btn btn-xs btn-primary" 
    href="{{ route("madir.maestro.direcciones.edit", $direccion) }}" 
    title="Editar">
    @icon('pencil', 'octicon-pencil')
</a>
@endpermission
@permission('update-madir-tipos-viales')
@if($direccion->status)
<button class="btn btn-xs btn-warning" 
    onclick="event.preventDefault();if(confirm('Desactivar dirección \'{{ $direccion->direccionF }}\'?')){document.getElementById('status-form-{{$direccion->id}}').submit(); }" 
    title="Desactivar">
    @icon('alert', 'octicon-alert')
</button>
@else
<button class="btn btn-xs btn-success" 
    onclick="event.preventDefault();if(confirm('Activar dirección \'{{ $direccion->direccionF }}\'?')){document.getElementById('status-form-{{$direccion->id}}').submit(); }" 
    title="Activar">
    @icon('alert', 'octicon-alert')
</button>
@endif
@endpermission
@permission('delete-madir-tipos-viales')
<button class="btn btn-xs btn-danger" 
    onclick="event.preventDefault();if(confirm('Eliminar dirección \'{{ $direccion->direccionF }}\'?')){document.getElementById('delete-form-{{$direccion->id}}').submit(); }" 
    title="Eliminar">
    @icon('x', 'octicon-x')
</button>
@endpermission
@permission('update-madir-tipos-viales')
{{ Form::model($direccion, array('route' => array('madir.maestro.direcciones.switch-status', $direccion), 'method' => 'PUT', "id" => "status-form-".$direccion->id)) }}
{{ Form::close() }}
@endpermission
@permission('delete-madir-tipos-viales')
{{ Form::model($direccion, array('route' => array('madir.maestro.direcciones.delete', $direccion), 'method' => 'DELETE', "id" => "delete-form-".$direccion->id)) }}
{{ Form::close() }}
@endpermission