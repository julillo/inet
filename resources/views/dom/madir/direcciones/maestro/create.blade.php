@extends ("layouts.master")

@section ("title")
| Admin Maestro Direcciones - Registrar nueva dirección
@endsection

@section ("content")
<h1>Registrar nueva dirección</h1>
<div class="card card-default">
    <div class="card-block">
        @include ("dom.madir.direcciones.maestro.forms.create")
    </div>
</div>
<hr>
@endsection

@push ("css")
@endpush

@push ("js")
@endpush