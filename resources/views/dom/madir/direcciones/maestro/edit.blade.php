@extends ("layouts.master")

@section ("title")
| Admin Maestro Direcciones - Editar dirección '{{ $direccion->direccionF }}'
@endsection

@section ("content")
<h1>Editar dirección '{{ $direccion->direccionF }}'</h1>
<div class="card card-default">
    <div class="card-block">
        @include ("dom.madir.direcciones.maestro.forms.edit")
    </div>
</div>
<hr>
@endsection