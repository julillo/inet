<!-- if there are creation errors, they will show here -->
{{ Html::ul($errors->all()) }}

{{ Form::model(array('route' => 'madir.maestro.direcciones.store', 'method' => 'POST')) }}

@include ("dom.madir.direcciones.maestro.forms.params")

{{ Form::submit('Guardar', array('class' => 'btn btn-primary')) }}

{{ Form::close() }}