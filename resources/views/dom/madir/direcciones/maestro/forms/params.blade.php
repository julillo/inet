<div class="form-group">
    {{ Form::label('vial_id', 'Base', array("class" => "col-md-4 control-label")) }}
    <div class="col-md-6">
        {{ Form::select('vial_id', $parents, $parentSelDef, array('id' => 'vial_id','class' => 'form-control')) }}
    </div>
</div>
<div class="form-group">
    {{ Form::label('numero', 'Numero', array("class" => "col-md-4 control-label")) }}
    <div id="rol" class="col-md-6">
        {{ Form::text('numero', null, array('class' => 'form-control', 'required' => 'true')) }}
    </div>
</div>
<div class="form-group">
    {{ Form::label('numero_postal', 'Código Postal', array("class" => "col-md-4 control-label")) }}
    <div class="col-md-6">
        {{ Form::text('numero_postal', null, array('class' => 'form-control')) }}
    </div>
</div>
@stack ("extras")
<div class="form-group">
    {{ Form::label('terreno_id', 'Terreno', array("class" => "col-md-4 control-label")) }}
    <div class="col-md-6">
        {{ Form::select('terreno_id', $terrenos, $terrenoSelDef, array('id' => 'terreno_id','class' => 'form-control')) }}
    </div>
</div>
@push ("js")
<script type="text/javascript">
    jQuery('#vial_id').select2({
        language: "es",
        placeholder: "Seleccione...",
        minimumInputLength: 2,
        allowClear: true,
        ajax: {
            url: '{{ route("madir.segmentos.viales.find") }}',
            dataType: 'json',
            data: function (params) {
                return {
                    q: jQuery.trim(params.term)
                };
            },
            processResults: function (data) {
                return {
                    results: data
                };
            },
            cache: true
        }
    });
    jQuery('#terreno_id').select2({
        language: "es",
        placeholder: "Seleccione...",
        minimumInputLength: 2,
        allowClear: true,
        ajax: {
            url: '{{ route("madir.maestro.territoriales.find") }}',
            dataType: 'json',
            data: function (params) {
                return {
                    q: jQuery.trim(params.term)
                };
            },
            processResults: function (data) {
                return {
                    results: data
                };
            },
            cache: true
        }
    });
</script>
@endpush