@extends ("layouts.master")

@section ("title")
| Admin Maestro Direcciones - Maestro Direcciones
@endsection

@section ("content")
<h1>Maestro Direcciones</h1>
@include ("dom.madir.direcciones.maestro.tables.index")
<hr>
@endsection

@push ("css")
@endpush

@push ("js")
@include ("dom.madir.direcciones.maestro.js.index-table")
@endpush