<ul>
    @foreach($childs as $child)
    <li>
        @if(empty($child->nombre))
        {{ $child->rolF }}
        @else
        {{ $child->nombre }}
        @endif
        @if(count($child->childs))
        @include('dom.madir.terrenos.maestro.tree.manageChild',['childs' => $child->childs])
        @endif
    </li>
    @endforeach
</ul>