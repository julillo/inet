@permission('update-madir-segmentos-viales')
<a class="btn btn-xs btn-primary" 
    href="{{ route("madir.segmentos.viales.edit", $segmentoVial) }}" 
    title="Editar">
    @icon('pencil', 'octicon-pencil')
</a>
@endpermission
@permission('update-madir-segmentos-viales')
@if($segmentoVial->status)
<button class="btn btn-xs btn-warning" 
    onclick="event.preventDefault();if(confirm('Desactivar segmento \'{{ $segmentoVial->base->nombre }} {{ $segmentoVial->numero_inicio }}-{{ $segmentoVial->numero_fin }}\'?')){document.getElementById('status-form-{{$segmentoVial->id}}').submit(); }" 
    title="Desactivar">
    @icon('alert', 'octicon-alert')
</button>
@else
<button class="btn btn-xs btn-success" 
    onclick="event.preventDefault();if(confirm('Activar segmento \'{{ $segmentoVial->base->nombre }} {{ $segmentoVial->numero_inicio }}-{{ $segmentoVial->numero_fin }}\'?')){document.getElementById('status-form-{{$segmentoVial->id}}').submit(); }" 
    title="Activar">
    @icon('alert', 'octicon-alert')
</button>
@endif
@endpermission
@permission('delete-madir-segmentos-viales')
<button class="btn btn-xs btn-danger" 
    onclick="event.preventDefault();if(confirm('Eliminar segmento \'{{ $segmentoVial->base->nombre }} {{ $segmentoVial->numero_inicio }}-{{ $segmentoVial->numero_fin }}\'?')){document.getElementById('delete-form-{{$segmentoVial->id}}').submit(); }" 
    title="Eliminar">
    @icon('x', 'octicon-x')
</button>
@endpermission
@permission('update-madir-segmentos-viales')
{{ Form::model($segmentoVial, array('route' => array('madir.segmentos.viales.switch-status', $segmentoVial), 'method' => 'PUT', "id" => "status-form-".$segmentoVial->id)) }}
{{ Form::close() }}
@endpermission
@permission('delete-madir-segmentos-viales')
{{ Form::model($segmentoVial, array('route' => array('madir.segmentos.viales.delete', $segmentoVial), 'method' => 'DELETE', "id" => "delete-form-".$segmentoVial->id)) }}
{{ Form::close() }}
@endpermission