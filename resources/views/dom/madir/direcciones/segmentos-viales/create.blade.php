@extends ("layouts.master")

@section ("title")
| Admin Maestro Direcciones - Registrar nuevo segmento vial
@endsection

@section ("content")
<h1>Registrar nuevo segmento vial</h1>
<div class="card card-default">
    <div class="card-block">
        @include ("dom.madir.direcciones.segmentos-viales.forms.create")
    </div>
</div>
<hr>
@endsection

@push ("css")
@endpush

@push ("js")
@endpush
