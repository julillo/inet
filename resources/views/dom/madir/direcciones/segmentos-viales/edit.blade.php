@extends ("layouts.master")

@section ("title")
| Admin Maestro Direcciones - Editar segmento vial '{{ $segmentoVial->base->nombre }} {{ $segmentoVial->numero_inicio }}-{{ $segmentoVial->numero_fin }}'
@endsection

@section ("content")
<h1>Editar segmento vial '{{ $segmentoVial->base->nombre }} {{ $segmentoVial->numero_inicio }}-{{ $segmentoVial->numero_fin }}'</h1>
<div class="card card-default">
    <div class="card-block">
        @include ("dom.madir.direcciones.segmentos-viales.forms.edit")
    </div>
</div>
<hr>
@endsection

@push ("css")
@endpush

@push ("js")
@endpush