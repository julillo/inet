<div class="form-group">
    {{ Form::label('base_id', 'Base', array("class" => "col-md-4 control-label")) }}
    <div class="col-md-6">
        {{ Form::select('base_id', $baseDefArray, $baseDefId, array('id' => 'base_list','class' => 'form-control')) }}
    </div>
</div>
<div class="form-group">
    {{ Form::label('numero_inicio', 'Numero Inicio', array("class" => "col-md-4 control-label")) }}
    <div class="col-md-6">
        {{ Form::text('numero_inicio', null, array('class' => 'form-control')) }}
    </div>
</div>
<div class="form-group">
    {{ Form::label('numero_fin', 'Numero Fin', array("class" => "col-md-4 control-label")) }}
    <div class="col-md-6">
        {{ Form::text('numero_fin', null, array('class' => 'form-control')) }}
    </div>
</div>
@stack ("extras")
<div class="form-group">
    {{ Form::label('tipo_id', 'Tipo', array("class" => "col-md-4 control-label")) }}
    <div class="col-md-6">
        {{ Form::select('tipo_id', $types, $typeDef, array('class' => 'form-control')) }}
    </div>
</div>
<div class="form-group">
    {{ Form::label('terreno_id', 'Sector', array("class" => "col-md-4 control-label")) }}
    <div class="col-md-6">
        {{ Form::select('terreno_id', $terrenos, $terrenoSelDef, array('id' => 'terreno_id','class' => 'form-control')) }}
    </div>
</div>

@push ("js")
<script type="text/javascript">
    jQuery('#base_list').select2({
        language: "es",
        placeholder: "Seleccione...",
        minimumInputLength: 2,
        ajax: {
            url: '{{ route("madir.bases.viales.find") }}',
            dataType: 'json',
            data: function (params) {
                return {
                    q: jQuery.trim(params.term)
                };
            },
            processResults: function (data) {
                return {
                    results: data
                };
            },
            cache: true
        }
    });
    jQuery('#terreno_id').select2({
        language: "es",
        placeholder: "Seleccione...",
        minimumInputLength: 2,
        allowClear: true,
        ajax: {
            url: '{{ route("madir.maestro.territoriales.find") }}',
            dataType: 'json',
            data: function (params) {
                return {
                    q: jQuery.trim(params.term)
                };
            },
            processResults: function (data) {
                return {
                    results: data
                };
            },
            cache: true
        }
    });
</script>
@endpush