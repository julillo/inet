@extends ("layouts.master")

@section ("title")
| Admin Maestro Direcciones - Segmentos Viales
@endsection

@section ("content")
<h1>Segmentos Viales</h1>
        @include ("dom.madir.direcciones.segmentos-viales.tables.index")
<hr>
@endsection

@push ("css")
@endpush

@push ("js")
@include ("dom.madir.direcciones.segmentos-viales.js.index-table")
@endpush