@permission('update-madir-tipos-viales')
<a class="btn btn-xs btn-primary" 
    href="{{ route("madir.tipos.viales.edit", $tipoVial) }}" 
    title="Editar">
    @icon('pencil', 'octicon-pencil')
</a>
@endpermission
@permission('update-madir-tipos-viales')
@if($tipoVial->status)
<button class="btn btn-xs btn-warning" 
    onclick="event.preventDefault();if(confirm('Desactivar tipo \'{{ $tipoVial->nombre }}\'?')){document.getElementById('status-form-{{$tipoVial->id}}').submit(); }" 
    title="Desactivar">
    @icon('alert', 'octicon-alert')
</button>
@else
<button class="btn btn-xs btn-success" 
    onclick="event.preventDefault();if(confirm('Activar tipo \'{{ $tipoVial->nombre }}\'?')){document.getElementById('status-form-{{$tipoVial->id}}').submit(); }" 
    title="Activar">
    @icon('alert', 'octicon-alert')
</button>
@endif
@endpermission
@permission('delete-madir-tipos-viales')
<button class="btn btn-xs btn-danger" 
    onclick="event.preventDefault();if(confirm('Eliminar tipo \'{{ $tipoVial->nombre }}\'?')){document.getElementById('delete-form-{{$tipoVial->id}}').submit(); }" 
    title="Eliminar">
    @icon('x', 'octicon-x')
</button>
@endpermission
@permission('update-madir-tipos-viales')
{{ Form::model($tipoVial, array('route' => array('madir.tipos.viales.switch-status', $tipoVial), 'method' => 'PUT', "id" => "status-form-".$tipoVial->id)) }}
{{ Form::close() }}
@endpermission
@permission('delete-madir-tipos-viales')
{{ Form::model($tipoVial, array('route' => array('madir.tipos.viales.delete', $tipoVial), 'method' => 'DELETE', "id" => "delete-form-".$tipoVial->id)) }}
{{ Form::close() }}
@endpermission