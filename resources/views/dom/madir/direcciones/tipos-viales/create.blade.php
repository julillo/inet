@extends ("layouts.master")

@section ("title")
| Admin Maestro Direcciones - Registrar nuevo tipo vial
@endsection

@section ("content")
<h1>Registrar nuevo tipo vial</h1>
<div class="card card-default">
    <div class="card-block">
        @include ("dom.madir.direcciones.tipos-viales.forms.create")
    </div>
</div>
<hr>
@endsection