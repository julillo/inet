@extends ("layouts.master")

@section ("title")
| Admin Maestro Direcciones - Editar tipo vial '{{ $tipoVial->nombre }}'
@endsection

@section ("content")
<h1>Editar tipo vial '{{ $tipoVial->nombre }}'</h1>
<div class="card card-default">
    <div class="card-block">
        @include ("dom.madir.direcciones.tipos-viales.forms.edit")
    </div>
</div>
<hr>
@endsection