<!-- if there are creation errors, they will show here -->
{{ Html::ul($errors->all()) }}

{{ Form::model(array('route' => 'madir.tipos.viales.store', 'method' => 'POST')) }}

@include ("dom.madir.direcciones.tipos-viales.forms.params")

{{ Form::submit('Guardar', array('class' => 'btn btn-primary')) }}

{{ Form::close() }}