<div class="form-group">
    {{ Form::label('nombre', 'Nombre', array("class" => "col-md-4 control-label")) }}
    <div class="col-md-6">
        {{ Form::text('nombre', null, array('class' => 'form-control', "required" => true)) }}
    </div>
</div>
<div class="form-group">
    {{ Form::label('abr', 'Abr', array("class" => "col-md-4 control-label")) }}
    <div class="col-md-6">
        {{ Form::text('abr', null, array('class' => 'form-control')) }}
    </div>
</div>
@stack ("extras")
<div class="form-group">
    {{ Form::label('parent_id', 'Predecesor', array("class" => "col-md-4 control-label")) }}
    <div class="col-md-6">
        {{ Form::select('parent_id', $parents, $parentSelDef, array('class' => 'form-control')) }}
    </div>
</div>