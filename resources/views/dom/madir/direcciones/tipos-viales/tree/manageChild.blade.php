<ul>
    @foreach($childs as $child)
    <li>
        {{ $child->nombre }}
        @if(count($child->childs))
        @include('dom.madir.direcciones.tipos-viales.tree.manageChild',['childs' => $child->childs])
        @endif
    </li>
    @endforeach
</ul>