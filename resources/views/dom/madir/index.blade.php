@extends ("layouts.master")

@section ("title")
| Maestro Direcciones
@endsection

@section ("content")
<h1>Maestro Direcciones</h1>
<div class="table-responsive">
    <table id="user-table-madir" class="table table-bordered" style="width: 100%">
        <thead>
            <tr>
                <th>Tipo</th>
                <th>Nombre</th>
                <th>Numero</th>
                <th>Otros</th>
                <th>C. Postal</th>
                <th>Sector</th>
                <th>Rol Manzana</th>
                <th>Rol Predio</th>
            </tr>
        </thead>
    </table>
</div>
<hr>
@endsection

@push ("css")
@endpush

@push ("js")
<script type="text/javascript">
    jQuery("#user-table-madir").DataTable({
        serverSide: true,
        processing: true,
        ajax: '{{ route("madir.user.index-data") }}',
        pagingType: "full_numbers",
        dom: 'B<"clear">lfrtip',
        buttons: [
            {
                extend: 'copyHtml5',
                exportOptions: {
                    columns: ':visible'
                }
            },
            {
                extend: 'csvHtml5',
                exportOptions: {
                    columns: ':visible'
                }
            },
            {
                extend: 'pdfHtml5',
                download: 'open',
                orientation: 'landscape',
                exportOptions: {
                    columns: ':visible'
                }
            },
            {
                extend: 'print',
                exportOptions: {
                    columns: ':visible'
                }
            },
            'colvis'
        ],
        columns: [
            {data: "direccion_tipo"},
            {data: "direccion_nombre"},
            {data: "direccion_numero"},
            {data: "terreno_nombre_f"},
            {data: "direccion_postal"},
            {data: "direccion_sector"},
            {data: "terreno_rol_manzana", visible: false},
            {data: "terreno_rol_predio", visible: false},
        ]
    });
</script>
@endpush