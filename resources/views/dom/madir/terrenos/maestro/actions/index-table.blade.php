@permission('update-madir-tipos-viales')
<a class="btn btn-xs btn-primary" 
    href="{{ route("madir.maestro.territoriales.edit", $terreno) }}" 
    title="Editar">
    @icon('pencil', 'octicon-pencil')
</a>
@endpermission
@permission('update-madir-tipos-viales')
@if($terreno->status)
<button class="btn btn-xs btn-warning" 
    onclick="event.preventDefault();if(confirm('Desactivar \'{{ $terreno->nombre }}\'?')){document.getElementById('status-form-{{$terreno->id}}').submit(); }" 
    title="Desactivar">
    @icon('alert', 'octicon-alert')
</button>
@else
<button class="btn btn-xs btn-success" 
    onclick="event.preventDefault();if(confirm('Activar \'{{ $terreno->nombre }}\'?')){document.getElementById('status-form-{{$terreno->id}}').submit(); }" 
    title="Activar">
    @icon('alert', 'octicon-alert')
</button>
@endif
@endpermission
@permission('delete-madir-tipos-viales')
<button class="btn btn-xs btn-danger" 
    onclick="event.preventDefault();if(confirm('Eliminar \'{{ $terreno->nombre }}\'?')){document.getElementById('delete-form-{{$terreno->id}}').submit(); }" 
    title="Eliminar">
    @icon('x', 'octicon-x')
</button>
@endpermission
@permission('update-madir-tipos-viales')
{{ Form::model($terreno, array('route' => array('madir.maestro.territoriales.switch-status', $terreno), 'method' => 'PUT', "id" => "status-form-".$terreno->id)) }}
{{ Form::close() }}
@endpermission
@permission('delete-madir-tipos-viales')
{{ Form::model($terreno, array('route' => array('madir.maestro.territoriales.delete', $terreno), 'method' => 'DELETE', "id" => "delete-form-".$terreno->id)) }}
{{ Form::close() }}
@endpermission