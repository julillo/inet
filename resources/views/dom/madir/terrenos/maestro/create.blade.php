@extends ("layouts.master")

@section ("title")
| Admin Maestro Direcciones - Registrar nuevo terreno
@endsection

@section ("content")
<h1>Registrar nuevo terreno</h1>
<div class="card card-default">
    <div class="card-block">
        @include ("dom.madir.terrenos.maestro.forms.create")
    </div>
</div>
<hr>
@endsection

@push ("css")
@endpush

@push ("js")
@endpush