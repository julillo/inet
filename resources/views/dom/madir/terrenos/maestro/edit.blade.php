@extends ("layouts.master")

@section ("title")
| Admin Maestro Direcciones - Editar terreno '{{ $terreno->nombre }}'
@endsection

@section ("content")
<h1>Editar terreno '{{ $terreno->nombre }}'</h1>
<div class="card card-default">
    <div class="card-block">
        @include ("dom.madir.terrenos.maestro.forms.edit")
    </div>
</div>
<hr>
@endsection