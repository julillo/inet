<!-- if there are creation errors, they will show here -->
{{ Html::ul($errors->all()) }}

{{ Form::model(array('route' => 'madir.maestro.territoriales.store', 'method' => 'POST')) }}

@include ("dom.madir.terrenos.maestro.forms.params")

{{ Form::submit('Guardar', array('class' => 'btn btn-primary')) }}

{{ Form::close() }}