<div class="form-group">
    {{ Form::label('rol', 'Rol', array("class" => "col-md-4 control-label")) }}
    <div id="rol" class="col-md-6 form-inline">
        {{ Form::text('rol_manzana', null, array('class' => 'col-md-2 form-control', "maxlength" => 5)) }}&nbsp;-&nbsp;
        {{ Form::text('rol_predio', null, array('class' => 'col-md-2 form-control', "maxlength" => 3)) }}
    </div>
</div>
<div class="form-group">
    {{ Form::label('nombre', 'Nombre', array("class" => "col-md-4 control-label")) }}
    <div class="col-md-6">
        {{ Form::text('nombre', null, array('class' => 'form-control')) }}
    </div>
</div>
@stack ("extras")
<div class="form-group">
    {{ Form::label('tipo_id', 'Tipo', array("class" => "col-md-4 control-label")) }}
    <div class="col-md-6">
        {{ Form::select('tipo_id', $types, $typeDef, array('class' => 'form-control')) }}
    </div>
</div>
<div class="form-group">
    {{ Form::label('parent_id', 'Predecesor', array("class" => "col-md-4 control-label")) }}
    <div class="col-md-6">
        {{ Form::select('parent_id', $parents, $parentSelDef, array('id' => 'parent_id','class' => 'form-control')) }}
    </div>
</div>

@push ("js")
<script type="text/javascript">
    jQuery('#parent_id').select2({
        language: "es",
        placeholder: "Seleccione...",
        minimumInputLength: 2,
        allowClear: true,
        ajax: {
            url: '{{ route("madir.maestro.territoriales.find") }}',
            dataType: 'json',
            data: function (params) {
                return {
                    q: jQuery.trim(params.term)
                };
            },
            processResults: function (data) {
                return {
                    results: data
                };
            },
            cache: true
        }
    });
</script>
@endpush