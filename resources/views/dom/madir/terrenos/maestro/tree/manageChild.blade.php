<ul>
    @foreach($childs as $child)
    <li>
        {{ $child->terrenoFF }}
        @if(count($child->childs))
        @include('dom.madir.terrenos.maestro.tree.manageChild',['childs' => $child->childs])
        @endif
    </li>
    @endforeach
</ul>