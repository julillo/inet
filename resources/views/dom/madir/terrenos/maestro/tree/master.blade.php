<ul id="tree1">
    @foreach($roots as $root)
    <li>
        {{ $root->terrenoFF }}
        @if(count($root->childs))
        @include('dom.madir.terrenos.maestro.tree.manageChild',['childs' => $root->childs])
        @endif
    </li>
    @endforeach
</ul>