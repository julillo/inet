@permission('update-madir-tipos-viales')
<a class="btn btn-xs btn-primary" 
    href="{{ route("madir.tipos.territoriales.edit", $tipoTerreno) }}" 
    title="Editar">
    @icon('pencil', 'octicon-pencil')
</a>
@endpermission
@permission('update-madir-tipos-viales')
@if($tipoTerreno->status)
<button class="btn btn-xs btn-warning" 
    onclick="event.preventDefault();if(confirm('Desactivar tipo \'{{ $tipoTerreno->nombre }}\'?')){document.getElementById('status-form-{{$tipoTerreno->id}}').submit(); }" 
    title="Desactivar">
    @icon('alert', 'octicon-alert')
</button>
@else
<button class="btn btn-xs btn-success" 
    onclick="event.preventDefault();if(confirm('Activar tipo \'{{ $tipoTerreno->nombre }}\'?')){document.getElementById('status-form-{{$tipoTerreno->id}}').submit(); }" 
    title="Activar">
    @icon('alert', 'octicon-alert')
</button>
@endif
@endpermission
@permission('delete-madir-tipos-viales')
<button class="btn btn-xs btn-danger" 
    onclick="event.preventDefault();if(confirm('Eliminar tipo \'{{ $tipoTerreno->nombre }}\'?')){document.getElementById('delete-form-{{$tipoTerreno->id}}').submit(); }" 
    title="Eliminar">
    @icon('x', 'octicon-x')
</button>
@endpermission
@permission('update-madir-tipos-viales')
{{ Form::model($tipoTerreno, array('route' => array('madir.tipos.territoriales.switch-status', $tipoTerreno), 'method' => 'PUT', "id" => "status-form-".$tipoTerreno->id)) }}
{{ Form::close() }}
@endpermission
@permission('delete-madir-tipos-viales')
{{ Form::model($tipoTerreno, array('route' => array('madir.tipos.territoriales.delete', $tipoTerreno), 'method' => 'DELETE', "id" => "delete-form-".$tipoTerreno->id)) }}
{{ Form::close() }}
@endpermission