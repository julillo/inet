@extends ("layouts.master")

@section ("title")
| Admin Maestro Direcciones - Registrar nuevo tipo territorial
@endsection

@section ("content")
<h1>Registrar nuevo tipo territorial</h1>
<div class="card card-default">
    <div class="card-block">
        @include ("dom.madir.terrenos.tipos-territoriales.forms.create")
    </div>
</div>
<hr>
@endsection