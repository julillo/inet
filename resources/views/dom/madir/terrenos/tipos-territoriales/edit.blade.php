@extends ("layouts.master")

@section ("title")
| Admin Maestro Direcciones - Editar tipo territorial '{{ $tipoTerreno->nombre }}'
@endsection

@section ("content")
<h1>Editar tipo territorial '{{ $tipoTerreno->nombre }}'</h1>
<div class="card card-default">
    <div class="card-block">
        @include ("dom.madir.terrenos.tipos-territoriales.forms.edit")
    </div>
</div>
<hr>
@endsection