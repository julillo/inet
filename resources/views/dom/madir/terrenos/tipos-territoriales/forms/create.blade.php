<!-- if there are creation errors, they will show here -->
{{ Html::ul($errors->all()) }}

{{ Form::model(array('route' => 'madir.tipos.territoriales.store', 'method' => 'POST')) }}

@include ("dom.madir.terrenos.tipos-territoriales.forms.params")

{{ Form::submit('Guardar', array('class' => 'btn btn-primary')) }}

{{ Form::close() }}