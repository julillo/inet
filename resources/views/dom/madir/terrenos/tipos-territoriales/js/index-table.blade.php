<script type="text/javascript">
    jQuery('#dom_madir_direcciones_tipos_territoriales_table').DataTable({
        serverSide: true,
        processing: true,
        ajax: '{{ route("madir.tipos.territoriales.index-data") }}',
        pagingType: "full_numbers",
        dom: 'B<"clear">lfrtip',
        buttons: [
            {
                extend: 'copyHtml5',
                exportOptions: {
                    columns: ':visible'
                }
            },
            {
                extend: 'csvHtml5',
                exportOptions: {
                    columns: ':visible'
                }
            },
            {
                extend: 'pdfHtml5',
                download: 'open',
                orientation: 'landscape',
                exportOptions: {
                    columns: ':visible'
                }
            },
            {
                extend: 'print',
                exportOptions: {
                    columns: ':visible'
                }
            },
            'colvis'
        ],
        columns: [
            {data: 'id'},
            {data: 'nombre'},
            {data: 'abr'},
            {data: 'descripcion'},
            {data: 'super'},
            {data: 'created_at', visible: false},
            {data: 'updated_at', visible: false},
            {data: 'action', orderable: false, searchable: false}
        ]
    });
</script>