@permission('read-madir-admin')
<h3>Admin. Maestro Direcciones</h3>
<ul class="nav nav-pills flex-column">
    @permission('read-madir-tipos-terrenos')
    <li class="nav-item">
        <a class="nav-link" href="{{route("madir.tipos.territoriales.index")}}">@icon("eye", "octicon-eye") Ver tipos territoriales</a>
    </li>
    @endpermission
    @permission('create-madir-tipos-terrenos')
    <li class="nav-item">
        <a class="nav-link" href="{{route("madir.tipos.territoriales.create")}}">@icon("diff-added", "octicon-diff-added") Crear tipo territorial</a>
    </li>
    @endpermission
    @permission('read-madir-terrenos')
    <li class="nav-item">
        <a class="nav-link" href="{{route("madir.maestro.territoriales.index")}}">@icon("eye", "octicon-eye") Ver terrenos</a>
    </li>
    @endpermission
    @permission('create-madir-terrenos')
    <li class="nav-item">
        <a class="nav-link" href="{{route("madir.maestro.territoriales.create")}}">@icon("diff-added", "octicon-diff-added") Crear terreno</a>
    </li>
    @endpermission
</ul>
<hr>
<ul class="nav nav-pills flex-column">
    @permission('read-madir-tipos-viales')
    <li class="nav-item">
        <a class="nav-link" href="{{route("madir.tipos.viales.index")}}">@icon("eye", "octicon-eye") Ver tipos viales</a>
    </li>
    @endpermission
    @permission('create-madir-tipos-viales')
    <li class="nav-item">
        <a class="nav-link" href="{{route("madir.tipos.viales.create")}}">@icon("diff-added", "octicon-diff-added") Crear tipo vial</a>
    </li>
    @endpermission
    @permission('read-madir-bases-viales')
    <li class="nav-item">
        <a class="nav-link" href="{{route("madir.bases.viales.index")}}">@icon("eye", "octicon-eye") Ver bases viales</a>
    </li>
    @endpermission
    @permission('create-madir-bases-viales')
    <li class="nav-item">
        <a class="nav-link" href="{{route("madir.bases.viales.create")}}">@icon("diff-added", "octicon-diff-added") Crear base vial</a>
    </li>
    @endpermission
    @permission('read-madir-segmentos-viales')
    <li class="nav-item">
        <a class="nav-link" href="{{route("madir.segmentos.viales.index")}}">@icon("eye", "octicon-eye") Ver segmentos viales</a>
    </li>
    @endpermission
    @permission('create-madir-segmentos-viales')
    <li class="nav-item">
        <a class="nav-link" href="{{route("madir.segmentos.viales.create")}}">@icon("diff-added", "octicon-diff-added") Crear segmento vial</a>
    </li>
    @endpermission
    @permission('read-madir-direcciones')
    <li class="nav-item">
        <a class="nav-link" href="{{route("madir.maestro.direcciones.index")}}">@icon("eye", "octicon-eye") Ver Direcciones</a>
    </li>
    @endpermission
    @permission('create-madir-direcciones')
    <li class="nav-item">
        <a class="nav-link" href="{{route("madir.maestro.direcciones.create")}}">@icon("diff-added", "octicon-diff-added") Crear Dirección</a>
    </li>
    @endpermission
</ul>
<hr >
@endpermission
@permission('read-madir-user')
<h3>Maestro Direcciones</h3>
<ul class="nav nav-pills flex-column">
    <li class="nav-item">
        <a class="nav-link" href="{{route("madir.user.index")}}">@icon("eye", "octicon-eye") Ver Direcciones</a>
    </li>
</ul>
<hr>
@endpermission