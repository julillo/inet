@extends ("layouts.master")

@section ("title")
| Bienvenido, {{ Auth::user()->name }}!
@endsection

@section ("content")
<h1>Bienvenido, {{ Auth::user()->name }}!</h1>
@endsection