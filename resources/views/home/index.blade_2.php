@extends ("layouts.master")

@section ("content")

<div class="container-fluid">
    <div class="row">
        <nav class="col-sm-3 col-md-2 hidden-xs-down bg-faded sidebar">
            <ul class="nav nav-pills flex-column">
                <li class="nav-item text-center">
                    {{Auth::user()->name}}
                </li>      
            </ul>
        </nav>
        <main class="col-sm-9 offset-sm-3 col-md-10 offset-md-2 pt-3">
            <h1>Bienvenido, {{ Auth::user()->name }}!</h1>
        </main>   
    </div>
</div>

@endsection