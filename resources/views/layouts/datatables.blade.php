<div class="table-responsive">
    <table @yield('table-id') class="table table-bordered" style="width: 100%">
        <thead>
            <tr>
                @yield("table-head")
            </tr>
        </thead>
    </table>
</div>