<footer class="footer">
    <p>Blog template built for <a href="https://getbootstrap.com">Bootstrap</a> by <a href="https://twitter.com/mdo">@mdo</a>.</p>
    <p>(c) {{ date('Y') }}, <a href="mailto:ju.lillo.rojas@gmail.com">Juan Lillo</a>.</p>
    <p>
        <a href="#">Back to top</a>
    </p>
</footer>
