<!DOCTYPE html>
<!--  
  Copyright (c) {{ date('Y') }}, Juan Lillo Rojas <ju.lillo.rojas@gmail.com>
  All rights reserved.
 
  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
 
   Redistributions of source code must retain the above copyright notice, this
    list of conditions and the following disclaimer.
   Redistributions in binary form must reproduce the above copyright notice,
    this list of conditions and the following disclaimer in the documentation
    and/or other materials provided with the distribution.
 
  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
  SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
  INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
  CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
  POSSIBILITY OF SUCH DAMAGE.
 /-->
<html lang="{{ config('app.locale') }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="description" content="">
        <meta name="author" content="">
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <title>{{ config('app.name') }} @yield ("title")</title>
        <link href="{{ asset('css/app.css') }}" rel="stylesheet">
        @stack ("css")
        <link rel="icon" href="../../favicon.ico">
    </head>
    <body>
        @include ("layouts.nav")
        @include ("layouts.flash")
        @if (!Auth::guest())
        <div class="container-fluid">
            <div class="row">
                @yield("sidebar-master")
                @yield("sidebar-mods")
                <main class="col-sm-9 offset-sm-3 col-md-10 offset-md-2 pt-3">
                    @yield ("content")
                </main>
            </div>
        </div>
        @else 
        @include ("layouts.login")
        @include ("layouts.reset")
        @endif
         Scripts 
        <script src="{{ asset('js/app.js') }}"></script>
        @stack ("js")
    </body>
</html>