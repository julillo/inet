@extends ("layouts.master-b")

@if (!Auth::guest())

@section ("sidebar-master")
@include ("layouts.side-master")
@endsection

@endif

@section ("sidebar-mods")
@include ("layouts.side-mods")
@endsection