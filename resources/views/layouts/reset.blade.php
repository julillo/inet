<div class="container">
    <div class="row">
        <div class="col-md-12 col-md-offset-2">
            @yield ("content-reset")
        </div>
    </div>
</div>