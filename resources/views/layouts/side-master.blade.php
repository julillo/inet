<nav class="col-sm-3 col-md-2 hidden-xs-down bg-faded sidebar">
    <ul class="nav nav-pills flex-column">
        <li class="nav-item text-center">
            {{Auth::user()->name}}
        </li>      
    </ul>
    <hr>
</nav>