<nav class="col-sm-3 col-md-2 hidden-xs-down bg-faded sidebar" style="top:120px !important">
@include ("acl.roles.side")
@include ("acl.permissions.side")
@include ("users.side")
@include ("organizational-unit.side")
@include ("secpla.side")
@include ("dom.side")
</nav>