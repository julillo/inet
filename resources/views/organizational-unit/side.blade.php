@permission('read-organizational-units')
<h3>@icon("file-submodule", "octicon-file-submodule", ["width" => 24, "height" => 32]) Un. Org.</h3>
<ul class="nav nav-pills flex-column">
    @permission('read-organizational-units')
    <li class="nav-item">
        <a class="nav-link" href="{{route("ous.index")}}">@icon("eye", "octicon-eye") Ver UO</a>
    </li>
    @endpermission
    @permission('create-organizational-units')
    <li class="nav-item">
        <a class="nav-link" href="{{route("ous.create")}}">@icon("diff-added", "oction-diff-added") Crear UO</a>
    </li>
    @endpermission
    @permission('read-organizational-unit-types')
    <li class="nav-item">
        <a class="nav-link" href="{{route("ou-types.index")}}">@icon("eye", "octicon-eye") Ver tipos UO</a>
    </li>
    @endpermission
    @permission('create-organizational-unit-types')
    <li class="nav-item">
        <a class="nav-link" href="{{route("ou-types.create")}}">@icon("diff-added", "oction-diff-added") Crear tipo UO</a>
    </li>
    @endpermission
</ul>
<hr>
@endpermission