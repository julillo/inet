@permission('update-organizational-unit-types')
<a class="btn btn-xs btn-primary" href="{{ route("ou-types.edit", $ouType) }}" title="Editar">@icon('pencil', 'octicon-pencil')</a>
@endpermission
