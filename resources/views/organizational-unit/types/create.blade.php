@extends ("layouts.master")

@section ("title")
| Registrar nuevo tipo UO
@endsection

@section ("content")
<h1>Registrar nuevo tipo UO</h1>
<div class="card card-default">
    <div class="card-block">
        @include ("organizational-unit.types.forms.create")
    </div>
</div>
<hr>
@endsection