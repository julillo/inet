@extends ("layouts.master")

@section ("title")
| Editar tipo UO '{{ $organizationalUnitType->name }}'
@endsection

@section ("content")
<h1>Editar tipo UO '{{ $organizationalUnitType->name }}'</h1>
<div class="card card-default">
    <div class="card-block">
        @include ("organizational-unit.types.forms.edit")
    </div>
</div>
<hr>
@endsection