<!-- if there are creation errors, they will show here -->
{{ Html::ul($errors->all()) }}

{{ Form::model(array('route' => 'ou-types.store', 'method' => 'POST')) }}

@include ("organizational-unit.types.forms.params")

{{ Form::submit('Guardar', array('class' => 'btn btn-primary')) }}

{{ Form::close() }}