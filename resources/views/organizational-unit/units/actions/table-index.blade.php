@permission('update-organizational-units')
<a class="btn btn-xs btn-primary" href="{{ route("ous.edit", $ou) }}" title="Editar">@icon('pencil', 'octicon-pencil')</a>
@endpermission