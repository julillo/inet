@extends ("layouts.master")

@section ("title")
| Registrar nueva UO
@endsection

@section ("content")
<h1>Registrar nueva UO</h1>
<div class="card card-default">
    <div class="card-block">
        @include ("organizational-unit.units.forms.create")
    </div>
</div>
<hr>
@endsection