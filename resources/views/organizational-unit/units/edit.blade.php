@extends ("layouts.master")

@section ("title")
| Editar UO '{{ $organizationalUnit->name }}'
@endsection

@section ("content")
<h1>Editar UO '{{ $organizationalUnit->name }}'</h1>
<div class="card card-default">
    <div class="card-block">
        @include ("organizational-unit.units.forms.edit")
    </div>
</div>
<hr>
@endsection