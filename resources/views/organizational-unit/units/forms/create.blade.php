<!-- if there are creation errors, they will show here -->
{{ Html::ul($errors->all()) }}

{{ Form::model(array('route' => 'ous.store', 'method' => 'POST')) }}

@include ("organizational-unit.units.forms.params")

{{ Form::submit('Guardar', array('class' => 'btn btn-primary')) }}

{{ Form::close() }}