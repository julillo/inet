<div class="form-group">
    {{ Form::label('name', 'Nombre', array("class" => "col-md-4 control-label")) }}
    <div class="col-md-6">
        {{ Form::text('name', null, array('class' => 'form-control', "required" => true)) }}
    </div>
</div>
<div class="form-group">
    {{ Form::label('short_name', 'Abreviatura', array("class" => "col-md-4 control-label")) }}
    <div class="col-md-6">
        {{ Form::text('short_name', null, array('class' => 'form-control')) }}
    </div>
</div>
<div class="form-group">
    {{ Form::label('description', 'Descripción', array("class" => "col-md-4 control-label")) }}
    <div class="col-md-6">
        {{ Form::textarea('description', null, array('class' => 'form-control')) }}
    </div>
</div>
@stack ("extras")
<div class="form-group">
    {{ Form::label('type_id', 'Tipo', array("class" => "col-md-4 control-label")) }}
    <div class="col-md-6">
        {{ Form::select('type_id', $types, $typesSelDef, array('class' => 'form-control', "required" => true)) }}
    </div>
</div>
<div class="form-group">
    {{ Form::label('parent_id', 'Predecesor', array("class" => "col-md-4 control-label")) }}
    <div class="col-md-6">
        {{ Form::select('parent_id', $parentUnits, $parentSelDef, array('class' => 'form-control')) }}
    </div>
</div>