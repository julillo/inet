@extends ("layouts.master")

@section ("title")
| Unidades Organizacionales
@endsection

@section ("content")
<h1>Unidades Organizacionales</h1>
<div class="card card-default">
    <div class="card-header">
        <ul class="nav nav-tabs card-header-tabs">
            <li class="nav-item">
                <a id="link1"class="nav-link active" href="#">Vista tabla</a>
            </li>
            <li class="nav-item">
                <a id="link2"class="nav-link" href="#">Vista arbol</a>
            </li>
        </ul>
    </div>
    <div id="card1" class="card-block" >
        @include ("organizational-unit.units.tables.index")
    </div>
    <div id="card2" class="card-block" style="display: none;">
        @include ("organizational-unit.units.tree.master")
    </div>
</div>
<hr>
@endsection

@push ("css")
@include ("organizational-unit.units.css.tree")
@endpush

@push ("js")
@include ("organizational-unit.units.js.index-tabs")
@include ("organizational-unit.units.js.tree")
@include ("organizational-unit.units.js.table-index")
@endpush