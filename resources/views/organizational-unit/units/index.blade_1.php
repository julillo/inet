@extends ("layouts.master")

@section ("title")
 | Unidades Organizacionales
@endsection

@section ("content")
<h1>Unidades Organizacionales</h1>
<div class="card card-default">
    <div class="card-header">
        <ul class="nav nav-tabs card-header-tabs">
            <li class="nav-item">
                <a id="link1"class="nav-link active" href="#">Vista tabla</a>
            </li>
            <li class="nav-item">
                <a id="link2"class="nav-link" href="#">Vista arbol</a>
            </li>
<!--            <li class="nav-item">
                <a id="link3"class="nav-link" href="#">Roles</a>
            </li>-->
<!--            <li class="nav-item">
                <a id="link4" class="nav-link" href="#">Permisos</a>
            </li>-->
        </ul>
    </div>
    <div id="card1" class="card-block" >
        a
    </div>
    <div id="card2" class="card-block" style="display: none;">

        
<!--            <ul id="treeview">
                <li data-icon-cls="fa fa-inbox" data-expanded="true">Inbox
                    <ul>
                        <li><b>Today (2)</b></li>
                        <li>Monday</li>
                        <li>Last Week</li>
                    </ul>
                </li>
                <li data-icon-cls="fa fa-trash">Trash
                </li>
                <li data-icon-cls="fa fa-calendar">Calendar
                    <ul>
                        <li>Day</li>
                        <li>Week</li>
                        <li>Month</li>
                    </ul>
                </li>
                <li data-icon-cls="fa fa-user">Contacts
                    <ul>
                        <li>Alexander Stein</li>
                        <li>John Doe</li>
                        <li>Paul Smith</li>
                        <li>Steward Lynn</li>
                    </ul>
                </li>
                <li data-icon-cls="fa fa-folder">Folders
                    <ul>
                        <li>Backup</li>
                        <li>Deleted</li>
                        <li>Projects</li>
                    </ul>
                </li>
            </ul>-->



<!--<div class="container">
    <div class="row">
        <div class="col-md-4 offset-md-4">-->
            <ul id="treeview">
                <li data-icon-cls="fa fa-inbox" data-expanded="true">Inbox
                    <ul>
                        <li><b>Today (2)</b></li>
                        <li>Monday</li>
                        <li>Last Week</li>
                    </ul>
                </li>
                <li data-icon-cls="fa fa-trash">Trash
                </li>
                <li data-icon-cls="fa fa-calendar">Calendar
                    <ul>
                        <li>Day</li>
                        <li>Week</li>
                        <li>Month</li>
                    </ul>
                </li>
                <li data-icon-cls="fa fa-user">Contacts
                    <ul>
                        <li>Alexander Stein</li>
                        <li>John Doe</li>
                        <li>Paul Smith</li>
                        <li>Steward Lynn</li>
                    </ul>
                </li>
                <li data-icon-cls="fa fa-folder">Folders
                    <ul>
                        <li>Backup</li>
                        <li>Deleted</li>
                        <li>Projects</li>
                    </ul>
                </li>
            </ul>
<!--        </div>
    </div>
</div>-->








    </div>
<!--    <div id="card3" class="card-block" style="display: none;">
    </div>-->
<!--    <div id="card4" class="card-block" style="display: none;">
    </div>-->
</div>
<hr>
@endsection

@section ("js")

@include ("organizational-unit.scripts.index-tabs")
<!--<link rel="stylesheet" type="text/css" href="https://www.shieldui.com/shared/components/latest/css/light-bootstrap/all.min.css" />
<script type="text/javascript" src="https://www.shieldui.com/shared/components/latest/js/shieldui-all.min.js"></script>

<script type="text/javascript">
    jQuery(function ($) {
        $("#treeview").shieldTreeView();
    });
</script>-->




<!-- you need to include the ShieldUI CSS and JS assets in order for the TreeView widget to work -->
<link rel="stylesheet" type="text/css" href="https://www.shieldui.com/shared/components/latest/css/light-bootstrap/all.min.css" />
<script type="text/javascript" src="https://www.shieldui.com/shared/components/latest/js/shieldui-all.min.js"></script>

<script type="text/javascript">
    jQuery(function ($) {
        $("#treeview").shieldTreeView();
    });
</script>

@endsection