<ul>
    @foreach($childs as $child)
    <li>
        {{ (isset($child->type)?$child->type->name:'') . ' ' .$child->name }}
        @if(count($child->childs))
        @include('organizational-unit.units.tree.manageChild',['childs' => $child->childs])
        @endif
    </li>
    @endforeach
</ul>