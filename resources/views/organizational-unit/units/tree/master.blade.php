<ul id="tree1">
    @foreach($roots as $root)
    <li>
        {{ (isset($root->type)?$root->type->name:'') . ' ' . $root->name }}
        @if(count($root->childs))
        @include('organizational-unit.units.tree.manageChild',['childs' => $root->childs])
        @endif
    </li>
    @endforeach
</ul>