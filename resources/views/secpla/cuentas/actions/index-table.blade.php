@permission('update-secpla-cuentas')
<a class="btn btn-xs btn-primary" 
    href="{{ route("secpla.cuentas.edit", $cuenta) }}" 
    title="Editar">
    @icon('pencil', 'octicon-pencil')
</a>
@endpermission
@permission('update-secpla-cuentas')
@if($cuenta->status)
<button class="btn btn-xs btn-warning" 
    onclick="event.preventDefault();if(confirm('Desactivar cuenta {{ $cuenta->nombre }}?')){document.getElementById('status-form-{{$cuenta->id}}').submit(); }" 
    title="Desactivar">
    @icon('alert', 'octicon-alert')
</button>
@else
<button class="btn btn-xs btn-success" 
    onclick="event.preventDefault();if(confirm('Activar cuenta {{ $cuenta->nombre }}?')){document.getElementById('status-form-{{$cuenta->id}}').submit(); }" 
    title="Activar">
    @icon('alert', 'octicon-alert')
</button>
@endif
@endpermission
@permission('delete-secpla-cuentas')
<button class="btn btn-xs btn-danger" 
    onclick="event.preventDefault();if(confirm('Eliminar cuenta {{ $cuenta->nombre }}?')){document.getElementById('delete-form-{{$cuenta->id}}').submit(); }" 
    title="Eliminar">
    @icon('x', 'octicon-x')
</button>
@endpermission
@permission('update-secpla-cuentas')
{{ Form::model($cuenta, array('route' => array('secpla.cuentas.switch-status', $cuenta), 'method' => 'PUT', "id" => "status-form-".$cuenta->id)) }}
{{ Form::close() }}
@endpermission
@permission('delete-secpla-cuentas')
{{ Form::model($cuenta, array('route' => array('secpla.cuentas.delete', $cuenta), 'method' => 'DELETE', "id" => "delete-form-".$cuenta->id)) }}
{{ Form::close() }}
@endpermission