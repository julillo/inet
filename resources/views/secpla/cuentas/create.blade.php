@extends ("layouts.master")

@section ("title")
| Admin Secpla - Registrar nueva cuenta
@endsection

@section ("content")
<h1>Registrar nueva cuenta</h1>
<div class="card card-default">
    <div class="card-block">
        @include ("secpla.cuentas.forms.create")
    </div>
</div>
<hr>
@endsection