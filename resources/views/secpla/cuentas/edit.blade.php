@extends ("layouts.master")

@section ("title")
| Admin Secpla - Editar cuenta {{ $cuenta->codigo }}
@endsection

@section ("content")
<h1>Editar cuenta '{{ trim($cuenta->codigo.' '.$cuenta->glosa) }}'</h1>
<div class="card card-default">
    <div class="card-block">
        @include ("secpla.cuentas.forms.edit")
    </div>
</div>
<hr>
@endsection