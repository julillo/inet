<!-- if there are creation errors, they will show here -->
{{ Html::ul($errors->all()) }}

{{ Form::model($cuenta, array('route' => array('secpla.cuentas.update', $cuenta), 'method' => 'PUT')) }}

@push("extras")
<div class="form-group">
    {{ Form::label('status', 'Estado', array("class" => "col-md-4 control-label")) }}
    <div class="col-md-6">
        {{ Form::radio('status', '1') }} Activo | {{ Form::radio('status', '0') }} Inactivo
    </div>
</div>
@endpush

@include ("secpla.cuentas.forms.params")

{{ Form::submit('Guardar', array('class' => 'btn btn-primary')) }}

{{ Form::close() }}