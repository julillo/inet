<div class="form-group">
    {{ Form::label('codigo', 'Código', array("class" => "col-md-4 control-label")) }}
    <div id="codigo" class="col-md-6 form-inline">
        {{ Form::text('titulo', null, array('class' => 'col-md-2 form-control', "required" => true, "maxlength" => 3)) }}&nbsp;@icon("primitive-dot", "octicon-primitive-dot")&nbsp;
        {{ Form::text('subtitulo', null, array('class' => 'col-md-1 form-control', "required" => true, "maxlength" => 2)) }}&nbsp;@icon("primitive-dot", "octicon-primitive-dot")&nbsp;
        {{ Form::text('item', null, array('class' => 'col-md-1 form-control', "required" => true, "maxlength" => 2)) }}&nbsp;@icon("primitive-dot", "octicon-primitive-dot")&nbsp;
        {{ Form::text('asignacion', null, array('class' => 'col-md-2 form-control', "required" => true, "maxlength" => 3)) }}&nbsp;@icon("primitive-dot", "octicon-primitive-dot")&nbsp;
        {{ Form::text('subasignacion', null, array('class' => 'col-md-2 form-control', "required" => true, "maxlength" => 3)) }}&nbsp;@icon("primitive-dot", "octicon-primitive-dot")&nbsp;
        {{ Form::text('subsubasignacion', null, array('class' => 'col-md-2 form-control', "required" => true, "maxlength" => 3)) }}
    </div>
</div>
<div class="form-group">
    {{ Form::label('glosa', 'Glosa', array("class" => "col-md-4 control-label")) }}
    <div class="col-md-6">
        {{ Form::text('glosa', null, array('class' => 'form-control')) }}
    </div>
</div>
@stack ("extras")
<div class="form-group">
    {{ Form::label('tipo_cuenta_id', 'Tipo', array("class" => "col-md-4 control-label")) }}
    <div class="col-md-6">
        {{ Form::select('tipo_cuenta_id', $types, $typeDef, array('class' => 'form-control')) }}
    </div>
</div>