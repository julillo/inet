@extends ("layouts.master")

@section ("title")
| Admin Secpla - Cuentas presupuestarias
@endsection

@section ("content")
<h1>Cuentas presupuestarias</h1>
        @include ("secpla.cuentas.tables.index")
<hr>
@endsection

@push ("css")
@endpush

@push ("js")
@include ("secpla.cuentas.js.index-table")
@endpush