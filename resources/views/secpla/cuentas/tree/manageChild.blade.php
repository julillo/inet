<ul>
    @foreach($childs as $child)
    <li>
        {{ $child->nombre }}
        @if(count($child->childs))
        @include('secpla.cuentas.tree.manageChild',['childs' => $child->childs])
        @endif
    </li>
    @endforeach
</ul>