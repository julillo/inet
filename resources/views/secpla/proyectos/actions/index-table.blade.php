@permission('update-secpla-proyectos')
<a class="btn btn-xs btn-primary" 
   href="{{ route("secpla.proyectos.edit", $proyecto) }}" 
   title="Editar">
    @icon('pencil', 'octicon-pencil')
</a>
@endpermission
@permission('delete-secpla-proyectos')
<button class="btn btn-xs btn-danger" 
    onclick="event.preventDefault();if(confirm('Eliminar tipo {{ $proyecto->nombre }}?')){document.getElementById('delete-form-{{$proyecto->id}}').submit(); }" 
    title="Eliminar">
    @icon('x', 'octicon-x')
</button>
@endpermission
@permission('delete-secpla-proyectos')
{{ Form::model($proyecto, array('route' => array('secpla.proyectos.delete', $proyecto), 'method' => 'DELETE', "id" => "delete-form-".$proyecto->id)) }}
{{ Form::close() }}
@endpermission