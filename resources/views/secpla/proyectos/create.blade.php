@extends ("layouts.master")

@section ("title")
| Secpla - Registrar nuevo proyecto
@endsection

@section ("content")
<h1>Registrar nuevo tipo de proyecto</h1>
<div class="card card-default">
    <div class="card-block">
        @include ("secpla.proyectos.forms.create")
    </div>
</div>
<hr>
@endsection