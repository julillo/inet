@extends ("layouts.master")

@push ("css")
@endpush

@push ("js")
@include ("secpla.proyectos.js.edit-tabs")
@include ("secpla.proyectos.js.edit-table-requisitos")
@endpush

@section ("title")
| Secpla - Editar proyecto '{{ $proyecto->nombre }}'
@endsection

@section ("content")
<h1>Editar proyecto '{{ $proyecto->nombre }}'</h1>
<div class="card card-default">
    <div class="card-header">
        <ul class="nav nav-tabs card-header-tabs">
            <li class="nav-item">
                <a id="link1"class="nav-link active" href="#">Info. básica</a>
            </li>
            <li class="nav-item">
                <a id="link2"class="nav-link" href="#">Pre-requisito</a>
            </li>
            <li class="nav-item">
                <a id="link3"class="nav-link" href="#">Dibujo</a>
            </li>
        </ul>
    </div>
    <div id="card1" class="card-block" >
        @include ("secpla.proyectos.forms.edit")
    </div>
    <div id="card2" class="card-block" style="display: none;">
        @include ("secpla.proyectos.tables.edit-table-requisitos")
    </div>
    <div id="card3" class="card-block" style="display: none;">
        @include ("secpla.proyectos.map3")
    </div>
</div>
<hr>
@endsection

