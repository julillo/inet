<!-- if there are creation errors, they will show here -->
{{ Html::ul($errors->all()) }}

{{ Form::model($proyecto, array('route' => array('secpla.proyectos.update', $proyecto), 'method' => 'PUT')) }}

@include ("secpla.proyectos.forms.params")

{{ Form::submit('Guardar', array('class' => 'btn btn-primary')) }}

{{ Form::close() }}