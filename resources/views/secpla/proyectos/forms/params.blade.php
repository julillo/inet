<div class="form-group">
    {{ Form::label('nombre', 'Nombre', array("class" => "col-md-4 control-label")) }}
    <div class="col-md-6">
        {{ Form::text('nombre', null, array('class' => 'form-control', "required" => true)) }}
    </div>
</div>
<div class="form-group">
    {{ Form::label('codigo', 'Código', array("class" => "col-md-4 control-label")) }}
    <div class="col-md-6">
        {{ Form::text('codigo', null, array('class' => 'form-control')) }}
    </div>
</div>
@stack ("extras")
<div class="form-group">
    {{ Form::label('eje_estrategico_id', 'Eje Estrategico', array("class" => "col-md-4 control-label")) }}
    <div class="col-md-6">
        {{ Form::select('eje_estrategico_id', $ejes, $ejeDef, array('class' => 'form-control')) }}
    </div>
</div>
<div class="form-group">
    {{ Form::label('tipo_id', 'Tipo', array("class" => "col-md-4 control-label")) }}
    <div class="col-md-6">
        {{ Form::select('tipo_id', $tipos, $tipoDef, array('class' => 'form-control')) }}
    </div>
</div>
<div class="form-group">
    {{ Form::label('financiamiento_id', 'Financiamiento', array("class" => "col-md-4 control-label")) }}
    <div class="col-md-6">
        {{ Form::select('financiamiento_id', $tiposFin, $tipoFinDef, array('class' => 'form-control')) }}
    </div>
</div>