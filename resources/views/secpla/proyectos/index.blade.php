@extends ("layouts.master")

@section ("title")
| Secpla - Proyecto
@endsection

@section ("content")
<h1>Proyecto</h1>
@include ("secpla.proyectos.tables.index")
<hr>
@endsection

@push ("css")
@endpush

@push ("js")
@include ("secpla.proyectos.js.index-table")
@endpush