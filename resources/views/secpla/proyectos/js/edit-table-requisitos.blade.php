<script type="text/javascript">
    var secpla_proyectos_requisitos_table = function ($) {
        $('#secpla-proyectos-requisitos-table').DataTable({
            responsive: true,
            serverSide: true,
            processing: true,
            ajax: '{{ route("secpla.proyectos.edit-requisitos", $proyecto) }}',
            pagingType: "full_numbers",
            columns: [
                {data: 'id'},
                {data: 'nombre'},
                {data: 'action', orderable: false, searchable: false}
            ]
        });
    }(jQuery);
</script>