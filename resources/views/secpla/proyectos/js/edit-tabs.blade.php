<script type="text/javascript">
    var nav = function ($) {
        $("#link1").click(function () {
            $("#card1").show();
            $("#card2").hide();
            $("#card3").hide();
            $("#card4").hide();
            $("#link2").removeClass("active");
            $("#link3").removeClass("active");
            $("#link4").removeClass("active");
            $(this).addClass("active");
        });
        $("#link2").click(function () {
            $("#card1").hide();
            $("#card2").show();
            $("#card3").hide();
            $("#card4").hide();
            $("#link1").removeClass("active");
            $("#link3").removeClass("active");
            $("#link4").removeClass("active");
            $(this).addClass("active");
        });
        $("#link3").click(function () {
            $("#card1").hide();
            $("#card2").hide();
            $("#card3").show();
            $("#card4").hide();
            $("#link1").removeClass("active");
            $("#link2").removeClass("active");
            $("#link4").removeClass("active");
            $(this).addClass("active");
            var map = maps[0].map;
            var currentCenter = map.getCenter();
            google.maps.event.trigger(map, "resize");
            map.setCenter(currentCenter);
        });
        $("#link4").click(function () {
            $("#card1").hide();
            $("#card2").hide();
            $("#card3").hide();
            $("#card4").show();
            $("#link1").removeClass("active");
            $("#link2").removeClass("active");
            $("#link3").removeClass("active");
            $(this).addClass("active");
        });
    }(jQuery);
</script>