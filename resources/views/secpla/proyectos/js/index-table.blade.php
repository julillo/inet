<script type="text/javascript">
    var secpla_proyectos_table = function ($) {
        $('#secpla-proyectos-table').DataTable({
            responsive: true,
            serverSide: true,
            processing: true,
            ajax: '{{ route("secpla.proyectos.index-data") }}',
            pagingType: "full_numbers",
            dom: 'B<"clear">lfrtip',
            buttons: [
                {
                    extend: 'copyHtml5',
                    exportOptions: {
                        columns: ':visible'
                    }
                },
                {
                    extend: 'csvHtml5',
                    exportOptions: {
                        columns: ':visible'
                    }
                },
                {
                    extend: 'pdfHtml5',
                    download: 'open',
                    orientation: 'landscape',
                    exportOptions: {
                        columns: ':visible'
                    }
                },
                {
                    extend: 'print',
                    exportOptions: {
                        columns: ':visible'
                    }
                },
                'colvis'
            ],
            columns: [
                {data: 'id'},
                {data: 'nombre'},
                {data: 'codigo'},
                {data: 'tipo'},
                {data: 'eje'},
                {data: 'financiamiento'},
                {data: 'requiere'},
                {data: 'requerido'},
                {data: 'created_at'},
                {data: 'updated_at'},
                {data: 'action', orderable: false, searchable: false}
            ]
        });
    }(jQuery);
</script>