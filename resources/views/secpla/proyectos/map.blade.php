@extends ("layouts.master")

@section ("title")
| Secpla - Editar proyecto '{{ $proyecto->nombre }}'
@endsection

@section ("content")
<h1>Editar proyecto '{{ $proyecto->nombre }}'</h1>
<div class="card card-default">
    <div id="panel">
        <div id="color-palette"></div>
        <div>
            <button id="delete-button">Eliminar</button>
        </div>
    </div>
    <div style="width: 100%; height: 500px;">
        {!! Mapper::render() !!}
    </div>
    <input id="save_encoded" value="save (encoded)"   onclick="console.log(shapes, true);" type="button" />
    <input id="save_encoded" value="save (raw)"   onclick="console.log(shapes, false);" type="button" />
    <input id="save_encoded" value="save (json encoded)"   onclick="console.log(IO.IN(shapes, true));" type="button" />
    <input id="save_encoded" value="save (json raw)"   onclick="console.log(IO.IN(shapes, false));" type="button" />
    <!--<a class="btn btn-xs btn-secondary"  href="{{ route("secpla.proyectos.get-map", $proyecto) }}">get</a>-->
    <a class="btn btn-xs btn-secondary"  href="#" onclick="shapes = IO.OUT(JSON.parse(byId('invisible').value), mapita);">get</a>
    <!-- if there are creation errors, they will show here -->
    <button class="btn btn-primary" 
            onclick="event.preventDefault();byId('invisible').value = (JSON.stringify(IO.IN(shapes, false)));document.getElementById('saveMap').submit();" 
            title="Guardar">
        Guardar
        <!--@icon('x', 'octicon-x')-->
    </button>
    {{ Form::model($proyecto, array('route' => array('secpla.proyectos.save-map', $proyecto), 'method' => 'PUT', 'id' => 'saveMap')) }}

    {{ Form::hidden('invisible', $dibujos, array("id" => 'invisible')) }}

    {{ Form::close() }}
</div>
<hr>
@endsection

@push ("css")
<style type="text/css">
    /*    #map, html, body {
                    padding: 0;
                    margin: 0;
                    width: 960px;
                    height: 700px;
                }*/

    #panel {
        width: 200px;
        font-family: Arial, sans-serif;
        font-size: 13px;
        float: right;
        margin: 10px;
    }

    #color-palette {
        clear: both;
    }

    .color-button {
        width: 14px;
        height: 14px;
        font-size: 0;
        margin: 2px;
        float: left;
        cursor: pointer;
    }

    #delete-button {
        margin-top: 5px;
    }
</style>
@endpush

@push ("js")
<!--<script type="text/javascript">
    function drawingManagerSetup(parentMap) {
        var drawingManager = new google.maps.drawing.DrawingManager({
//          drawingMode: google.maps.drawing.OverlayType.MARKER,
            drawingControl: true,
            drawingControlOptions: {
                position: google.maps.ControlPosition.TOP_CENTER,
                drawingModes: ['marker', 'circle', 'polygon', 'polyline', 'rectangle']
            },
            markerOptions: {icon: 'https://developers.google.com/maps/documentation/javascript/examples/full/images/beachflag.png'},
            circleOptions: {
                fillColor: '#ffff00',
                fillOpacity: 1,
                strokeWeight: 5,
                clickable: false,
                editable: true,
                zIndex: 1
            }
        });
        drawingManager.setMap(parentMap);

        var poly = new google.maps.Polygon({
            editable: true
        });
        poly.setMap(parentMap);
        google.maps.event.addListener(poly, 'rightclick', function (event) {
            if (event.vertex == undefined) {
                return;
            } else {
                removeVertex(event.vertex);
            }
        });

    }

    function removeVertex(vertex) {
        var path = poly.getPath();
        path.removeAt(vertex);
    }
</script>-->
<script type="text/javascript">
    var drawingManager;
    var selectedShape;
    var colors = [
        '#1E90FF',
        '#FF1493',
        '#32CD32',
        '#FF8C00',
        '#4B0082',
        '#e7214c',
        '#0066CC',
        '#9D5D5D',
        '#A2CD5A',
        '#000000'];
    var selectedColor;
    var colorButtons = {};
    var shapes = [];

    function clearSelection() {
        if (selectedShape) {
            if (selectedShape.type !== 'marker') {
                selectedShape.setEditable(false);
            }
            selectedShape = null;
        }
    }

    var setSelection = function (shape) {
        if (shape.type !== 'marker') {
            clearSelection();
            shape.setEditable(true);
            selectColor(shape.get('fillColor') || shape.get('strokeColor'));
        }

        selectedShape = shape;
    }

    function deleteSelectedShape() {
        if (selectedShape) {
            for (i = 0, l = shapes.length; i < l; i++) {
                if (shapes[i] === selectedShape) {
                    shapes[i] = null;
                }
            }
            shapes = shapes.filter(n => n);
            selectedShape.setMap(null);
        }
    }

    function selectColor(color) {
        selectedColor = color;
        for (var i = 0; i < colors.length; ++i) {
            var currColor = colors[i];
            colorButtons[currColor].style.border = currColor == color ? '2px solid #789' : '2px solid #fff';
        }

        // Retrieves the current options from the drawing manager and replaces the
        // stroke or fill color as appropriate.
        var polylineOptions = drawingManager.get('polylineOptions');
        polylineOptions.strokeColor = color;
        drawingManager.set('polylineOptions', polylineOptions);

        var rectangleOptions = drawingManager.get('rectangleOptions');
        rectangleOptions.fillColor = color;
        drawingManager.set('rectangleOptions', rectangleOptions);

        var circleOptions = drawingManager.get('circleOptions');
        circleOptions.fillColor = color;
        drawingManager.set('circleOptions', circleOptions);

        var polygonOptions = drawingManager.get('polygonOptions');
        polygonOptions.fillColor = color;
        drawingManager.set('polygonOptions', polygonOptions);
    }

    function setSelectedShapeColor(color) {
        if (selectedShape) {
            if (selectedShape.type == google.maps.drawing.OverlayType.POLYLINE) {
                selectedShape.set('strokeColor', color);
            } else {
                selectedShape.set('fillColor', color);
            }
        }
    }

    function makeColorButton(color) {
        var button = document.createElement('span');
        button.className = 'color-button';
        button.style.backgroundColor = color;
        google.maps.event.addDomListener(button, 'click', function () {
            selectColor(color);
            setSelectedShapeColor(color);
        });

        return button;
    }

    function buildColorPalette() {
        var colorPalette = document.getElementById('color-palette');
        for (var i = 0; i < colors.length; ++i) {
            var currColor = colors[i];
            var colorButton = makeColorButton(currColor);
            colorPalette.appendChild(colorButton);
            colorButtons[currColor] = colorButton;
        }
        selectColor(colors[0]);
    }

    function drawingManagerSetup(map) {
        window.mapita = map;
        var polyOptions = {
            strokeWeight: 0,
            fillOpacity: 0.45,
            editable: true,
            draggable: true
        };
        // Creates a drawing manager attached to the map that allows the user to draw
        // markers, lines, and shapes.
        drawingManager = new google.maps.drawing.DrawingManager({
//                drawingMode: google.maps.drawing.OverlayType.POLYGON,
            markerOptions: {
                draggable: true
            },
            polylineOptions: {
                editable: true,
                draggable: true
            },
            rectangleOptions: polyOptions,
            circleOptions: polyOptions,
            polygonOptions: polyOptions,
            map: map
        });

        google.maps.event.addListener(drawingManager, 'overlaycomplete', function (e) {
            var newShape = e.overlay;

            newShape.type = e.type;

            if (e.type !== google.maps.drawing.OverlayType.MARKER) {
                // Switch back to non-drawing mode after drawing a shape.
                drawingManager.setDrawingMode(null);

                // Add an event listener that selects the newly-drawn shape when the user
                // mouses down on it.
                google.maps.event.addListener(newShape, 'click', function (e) {
                    if (e.vertex !== undefined) {
                        if (newShape.type === google.maps.drawing.OverlayType.POLYGON) {
                            var path = newShape.getPaths().getAt(e.path);
                            path.removeAt(e.vertex);
                            if (path.length < 3) {
                                newShape.setMap(null);
                            }
                        }
                        if (newShape.type === google.maps.drawing.OverlayType.POLYLINE) {
                            var path = newShape.getPath();
                            path.removeAt(e.vertex);
                            if (path.length < 2) {
                                newShape.setMap(null);
                            }
                        }
                    }
                    setSelection(newShape);
                });
                setSelection(newShape);
            } else {
                google.maps.event.addListener(newShape, 'click', function (e) {
                    setSelection(newShape);
                });
                setSelection(newShape);
            }
            shapes.push(newShape);
        });

        // Clear the current selection when the drawing mode is changed, or when the
        // map is clicked.
        google.maps.event.addListener(drawingManager, 'drawingmode_changed', clearSelection);
        google.maps.event.addListener(map, 'click', clearSelection);
        google.maps.event.addDomListener(document.getElementById('delete-button'), 'click', deleteSelectedShape);

        buildColorPalette();
    }
</script>
<script type="text/javascript">
    var IO = {
        //returns array with storable google.maps.Overlay-definitions
        IN: function (arr, //array with google.maps.Overlays
                encoded//boolean indicating if pathes should be stored encoded
                ) {
            var shapes = [],
                    goo = google.maps,
                    shape, tmp;

            for (var i = 0; i < arr.length; i++)
            {
                shape = arr[i];
                tmp = {type: this.t_(shape.type), id: shape.id || null};


                switch (tmp.type) {
                    case 'CIRCLE':
                        tmp.radius = shape.getRadius();
                        tmp.geometry = this.p_(shape.getCenter());
                        tmp.style = {fillOpacity: shape.fillOpacity, fillColor: shape.fillColor, strokeColor: shape.strokeColor, strokeOpacity: shape.strokeOpacity, strokeWeight: shape.strokeWeight};
                        break;
                    case 'MARKER':
                        tmp.geometry = this.p_(shape.getPosition());
                        break;
                    case 'RECTANGLE':
                        tmp.geometry = this.b_(shape.getBounds());
                        tmp.style = {fillOpacity: shape.fillOpacity, fillColor: shape.fillColor, strokeColor: shape.strokeColor, strokeOpacity: shape.strokeOpacity, strokeWeight: shape.strokeWeight};
                        break;
                    case 'POLYLINE':
                        tmp.geometry = this.l_(shape.getPath(), encoded);
                        tmp.style = {strokeColor: shape.strokeColor, strokeOpacity: shape.strokeOpacity, strokeWeight: shape.strokeWeight};
                        break;
                    case 'POLYGON':
                        tmp.geometry = this.m_(shape.getPaths(), encoded);
                        tmp.style = {fillOpacity: shape.fillOpacity, fillColor: shape.fillColor, strokeColor: shape.strokeColor, strokeOpacity: shape.strokeOpacity, strokeWeight: shape.strokeWeight};
                        break;
                }
                shapes.push(tmp);
            }

            return shapes;
        },
        //returns array with google.maps.Overlays
        OUT: function (arr, //array containg the stored shape-definitions
                map//map where to draw the shapes
                ) {
            var shapes = [],
                    goo = google.maps,
                    map = map || null,
                    shape, tmp;

            for (var i = 0; i < arr.length; i++)
            {
                shape = arr[i];

                switch (shape.type) {
                    case 'CIRCLE':
                        tmp = new goo.Circle({
                            radius: Number(shape.radius),
                            center: this.pp_.apply(this, shape.geometry),
                            fillOpacity: shape.style.fillOpacity,
                            fillColor: shape.style.fillColor,
                            strokeColor: shape.style.strokeColor,
                            strokeOpacity: shape.style.strokeOpacity,
                            strokeWeight: shape.style.strokeWeight
                        });
                        break;
                    case 'MARKER':
                        tmp = new goo.Marker({
                            position: this.pp_.apply(this, shape.geometry)
                        });
                        break;
                    case 'RECTANGLE':
                        tmp = new goo.Rectangle({
                            bounds: this.bb_.apply(this, shape.geometry),
                            fillOpacity: shape.style.fillOpacity,
                            fillColor: shape.style.fillColor,
                            strokeColor: shape.style.strokeColor,
                            strokeOpacity: shape.style.strokeOpacity,
                            strokeWeight: shape.style.strokeWeight
                        });
                        break;
                    case 'POLYLINE':
                        tmp = new goo.Polyline({
                            path: this.ll_(shape.geometry),
                            strokeColor: shape.style.strokeColor,
                            strokeOpacity: shape.style.strokeOpacity,
                            strokeWeight: shape.style.strokeWeight
                        });
                        break;
                    case 'POLYGON':
                        tmp = new goo.Polygon({
                            paths: this.mm_(shape.geometry),
                            fillOpacity: shape.style.fillOpacity,
                            fillColor: shape.style.fillColor,
                            strokeColor: shape.style.strokeColor,
                            strokeOpacity: shape.style.strokeOpacity,
                            strokeWeight: shape.style.strokeWeight
                        });
                        break;
                }
                tmp.setValues({
                    map: map,
                    id: shape.id,
                    type: shape.type.toLowerCase(),
                    draggable: true,

                })
                tmp.addListener('click', function (e) {
                    setSelection(this);
                });
                shapes.push(tmp);
            }
            return shapes;
        },
        l_: function (path, e) {
            path = (path.getArray) ? path.getArray() : path;
            if (e) {
                return google.maps.geometry.encoding.encodePath(path);
            } else {
                var r = [];
                for (var i = 0; i < path.length; ++i) {
                    r.push(this.p_(path[i]));
                }
                return r;
            }
        },
        ll_: function (path) {
            if (typeof path === 'string') {
                return google.maps.geometry.encoding.decodePath(path);
            } else {
                var r = [];
                for (var i = 0; i < path.length; ++i) {
                    r.push(this.pp_.apply(this, path[i]));
                }
                return r;
            }
        },

        m_: function (paths, e) {
            var r = [];
            paths = (paths.getArray) ? paths.getArray() : paths;
            for (var i = 0; i < paths.length; ++i) {
                r.push(this.l_(paths[i], e));
            }
            return r;
        },
        mm_: function (paths) {
            var r = [];
            for (var i = 0; i < paths.length; ++i) {
                r.push(this.ll_.call(this, paths[i]));

            }
            return r;
        },
        p_: function (latLng) {
            return([latLng.lat(), latLng.lng()]);
        },
        pp_: function (lat, lng) {
            return new google.maps.LatLng(lat, lng);
        },
        b_: function (bounds) {
            return([this.p_(bounds.getSouthWest()),
                this.p_(bounds.getNorthEast())]);
        },
        bb_: function (sw, ne) {
            return new google.maps.LatLngBounds(this.pp_.apply(this, sw),
                    this.pp_.apply(this, ne));
        },
        t_: function (s) {
            var t = ['CIRCLE', 'MARKER', 'RECTANGLE', 'POLYLINE', 'POLYGON'];
            for (var i = 0; i < t.length; ++i) {
                if (s === google.maps.drawing.OverlayType[t[i]]) {
                    return t[i];
                }
            }
        }

    }, byId = function (s) {
        return document.getElementById(s)
    };
</script>
@endpush