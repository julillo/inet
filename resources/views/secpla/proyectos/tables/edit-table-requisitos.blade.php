<div class="table-responsive">
    {{ Form::model($proyecto, array('route' => array('secpla.proyectos.update-requisitos', $proyecto), 'method' => 'PUT')) }}
    <table id="secpla-proyectos-requisitos-table" class="table table-bordered" style="width: 100%">
        <thead>
            <tr>
                <th>Id</th>
                <th>Nombre</th>
                <th>Action</th>
            </tr>
        </thead>
    </table>
    {{ Form::submit('Guardar', array('class' => 'btn btn-primary')) }}

    {{ Form::close() }}
</div>