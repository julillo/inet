@permission('read-secpla-admin')
<h3>Admin. Secpla</h3>
<ul class="nav nav-pills flex-column">
    @permission('read-secpla-ejes-estrategicos')
    <li class="nav-item">
        <a class="nav-link" href="{{route("secpla.tipos.ejes.index")}}">@icon("eye", "octicon-eye") Ver ejes estrategicos</a>
    </li>
    @endpermission
    @permission('create-secpla-ejes-estrategicos')
    <li class="nav-item">
        <a class="nav-link" href="{{route("secpla.tipos.ejes.create")}}">@icon("diff-added", "octicon-diff-added") Crear ejes estrategicos</a>
    </li>
    @endpermission
    @permission('read-secpla-tipos-proyectos')
    <li class="nav-item">
        <a class="nav-link" href="{{route("secpla.tipos.proyecto.index")}}">@icon("eye", "octicon-eye") Ver tipos de proyectos</a>
    </li>
    @endpermission
    @permission('create-secpla-tipos-proyectos')
    <li class="nav-item">
        <a class="nav-link" href="{{route("secpla.tipos.proyecto.create")}}">@icon("diff-added", "octicon-diff-added") Crear tipo de proyecto</a>
    </li>
    @endpermission
    @permission('read-secpla-tipos-cuentas')
    <li class="nav-item">
        <a class="nav-link" href="{{route("secpla.tipos.cuentas.index")}}">@icon("eye", "octicon-eye") Ver tipos de cuentas</a>
    </li>
    @endpermission
    @permission('create-secpla-tipos-cuentas')
    <li class="nav-item">
        <a class="nav-link" href="{{route("secpla.tipos.cuentas.create")}}">@icon("diff-added", "octicon-diff-added") Crear tipo de cuenta</a>
    </li>
    @endpermission
    @permission('read-secpla-tipos-financiamiento')
    <li class="nav-item">
        <a class="nav-link" href="{{route("secpla.tipos.financiamiento.index")}}">@icon("eye", "octicon-eye") Ver tipos de financiamiento</a>
    </li>
    @endpermission
    @permission('create-secpla-tipos-financiamiento')
    <li class="nav-item">
        <a class="nav-link" href="{{route("secpla.tipos.financiamiento.create")}}">@icon("diff-added", "octicon-diff-added") Crear tipo de financiamiento</a>
    </li>
    @endpermission
</ul>
<hr>
@endpermission
@permission('read-secpla-admin')
<!--<h3>Secpla - Cuentas presupuestarias</h3>-->
<ul class="nav nav-pills flex-column">
    @permission('read-secpla-cuentas')
    <li class="nav-item">
        <a class="nav-link" href="{{route("secpla.cuentas.index")}}">@icon("eye", "octicon-eye") Ver Cuentas</a>
    </li>
    @endpermission
    @permission('create-secpla-cuentas')
    <li class="nav-item">
        <a class="nav-link" href="{{route("secpla.cuentas.create")}}">@icon("diff-added", "octicon-diff-added") Crear Cuenta</a>
    </li>
    @endpermission
</ul>
<hr>
@endpermission
@permission('read-secpla-project-manager')
<h3>Secpla - Proyectos</h3>
<ul class="nav nav-pills flex-column">
    @permission('read-secpla-proyectos')
    <li class="nav-item">
        <a class="nav-link" href="{{route("secpla.proyectos.index")}}">@icon("eye", "octicon-eye") Ver proyectos</a>
    </li>
    @endpermission
    @permission('create-secpla-proyectos')
    <li class="nav-item">
        <a class="nav-link" href="{{route("secpla.proyectos.create")}}">@icon("diff-added", "octicon-diff-added") Crear proyecto</a>
    </li>
    @endpermission
</ul>
<hr>
@endpermission