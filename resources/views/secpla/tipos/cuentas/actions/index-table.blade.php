@permission('update-secpla-tipos-cuentas')
<a class="btn btn-xs btn-primary" 
    href="{{ route("secpla.tipos.cuentas.edit", $cuenta) }}" 
    title="Editar">
    @icon('pencil', 'octicon-pencil')
</a>
@endpermission
@permission('update-secpla-tipos-cuentas')
@if($cuenta->status)
<button class="btn btn-xs btn-warning" 
    onclick="event.preventDefault();if(confirm('Desactivar tipo cuenta {{ $cuenta->nombre }}?')){document.getElementById('status-form-{{$cuenta->id}}').submit(); }" 
    title="Desactivar">
    @icon('alert', 'octicon-alert')
</button>
@else
<button class="btn btn-xs btn-success" 
    onclick="event.preventDefault();if(confirm('Activar tipo cuenta {{ $cuenta->nombre }}?')){document.getElementById('status-form-{{$cuenta->id}}').submit(); }" 
    title="Activar">
    @icon('alert', 'octicon-alert')
</button>
@endif
@endpermission
@permission('delete-secpla-tipos-cuentas')
<button class="btn btn-xs btn-danger" 
    onclick="event.preventDefault();if(confirm('Eliminar tipo cuenta {{ $cuenta->nombre }}?')){document.getElementById('delete-form-{{$cuenta->id}}').submit(); }" 
    title="Eliminar">
    @icon('x', 'octicon-x')
</button>
@endpermission
@permission('update-secpla-tipos-cuentas')
{{ Form::model($cuenta, array('route' => array('secpla.tipos.cuentas.switch-status', $cuenta), 'method' => 'PUT', "id" => "status-form-".$cuenta->id)) }}
{{ Form::close() }}
@endpermission
@permission('delete-secpla-tipos-cuentas')
{{ Form::model($cuenta, array('route' => array('secpla.tipos.cuentas.delete', $cuenta), 'method' => 'DELETE', "id" => "delete-form-".$cuenta->id)) }}
{{ Form::close() }}
@endpermission