@extends ("layouts.master")

@section ("title")
| Admin Secpla - Registrar nuevo tipo cuenta
@endsection

@section ("content")
<h1>Registrar nuevo tipo cuenta</h1>
<div class="card card-default">
    <div class="card-block">
        @include ("secpla.tipos.cuentas.forms.create")
    </div>
</div>
<hr>
@endsection