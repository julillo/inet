@extends ("layouts.master")

@section ("title")
| Admin Secpla - Editar tipo cuenta '{{ $cuenta->nombre }}'
@endsection

@section ("content")
<h1>Editar tipo cuenta '{{ $cuenta->nombre }}'</h1>
<div class="card card-default">
    <div class="card-block">
        @include ("secpla.tipos.cuentas.forms.edit")
    </div>
</div>
<hr>
@endsection