<!-- if there are creation errors, they will show here -->
{{ Html::ul($errors->all()) }}

{{ Form::model(array('route' => 'secpla.tipos.cuentas.store', 'method' => 'POST')) }}

@include ("secpla.tipos.cuentas.forms.params")

{{ Form::submit('Guardar', array('class' => 'btn btn-primary')) }}

{{ Form::close() }}