<div class="form-group">
    {{ Form::label('nombre', 'Nombre', array("class" => "col-md-4 control-label")) }}
    <div class="col-md-6">
        {{ Form::text('nombre', null, array('class' => 'form-control', "required" => true)) }}
    </div>
</div>
@stack ("extras")