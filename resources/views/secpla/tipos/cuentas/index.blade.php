@extends ("layouts.master")

@section ("title")
| Admin Secpla - Tipos Cuentas presupuestarias
@endsection

@section ("content")
<h1>Tipos Cuentas presupuestarias</h1>
        @include ("secpla.tipos.cuentas.tables.index")
<hr>
@endsection

@push ("css")
@endpush

@push ("js")
@include ("secpla.tipos.cuentas.js.index-table")
@endpush