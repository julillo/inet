<script type="text/javascript">
    jQuery('#secpla_tipos_cuentas_table').DataTable({
        serverSide: true,
        processing: true,
        ajax: '{{ route("secpla.tipos.cuentas.index-data") }}',
        pagingType: "full_numbers",
        dom: 'B<"clear">lfrtip',
        buttons: [
            {
                extend: 'copyHtml5',
                exportOptions: {
                    columns: ':visible'
                }
            },
            {
                extend: 'csvHtml5',
                exportOptions: {
                    columns: ':visible'
                }
            },
            {
                extend: 'pdfHtml5',
                download: 'open',
                orientation: 'landscape',
                exportOptions: {
                    columns: ':visible'
                }
            },
            {
                extend: 'print',
                exportOptions: {
                    columns: ':visible'
                }
            },
            'colvis'
        ],
        columns: [
            {data: 'id'},
            {data: 'nombre'},
            {data: 'created_at'},
            {data: 'updated_at'},
            {data: 'action', orderable: false, searchable: false}
        ]
    });
</script>