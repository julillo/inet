@permission('update-secpla-ejes-estrategicos')
<a class="btn btn-xs btn-primary" 
    href="{{ route("secpla.tipos.ejes.edit", $eje) }}" 
    title="Editar">
    @icon('pencil', 'octicon-pencil')
</a>
@endpermission
@permission('update-secpla-ejes-estrategicos')
@if($eje->status)
<button class="btn btn-xs btn-warning" 
    onclick="event.preventDefault();if(confirm('Desactivar eje {{ $eje->nombre }}?')){document.getElementById('status-form-{{$eje->id}}').submit(); }" 
    title="Desactivar">
    @icon('alert', 'octicon-alert')
</button>
@else
<button class="btn btn-xs btn-success" 
    onclick="event.preventDefault();if(confirm('Activar eje {{ $eje->nombre }}?')){document.getElementById('status-form-{{$eje->id}}').submit(); }" 
    title="Activar">
    @icon('alert', 'octicon-alert')
</button>
@endif
@endpermission
@permission('delete-secpla-ejes-estrategicos')
<button class="btn btn-xs btn-danger" 
    onclick="event.preventDefault();if(confirm('Eliminar eje {{ $eje->nombre }}?')){document.getElementById('delete-form-{{$eje->id}}').submit(); }" 
    title="Eliminar">
    @icon('x', 'octicon-x')
</button>
@endpermission
@permission('update-secpla-ejes-estrategicos')
{{ Form::model($eje, array('route' => array('secpla.tipos.ejes.switch-status', $eje), 'method' => 'PUT', "id" => "status-form-".$eje->id)) }}
{{ Form::close() }}
@endpermission
@permission('delete-secpla-ejes-estrategicos')
{{ Form::model($eje, array('route' => array('secpla.tipos.ejes.delete', $eje), 'method' => 'DELETE', "id" => "delete-form-".$eje->id)) }}
{{ Form::close() }}
@endpermission