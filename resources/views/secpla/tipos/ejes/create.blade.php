@extends ("layouts.master")

@section ("title")
| Admin Secpla - Registrar nuevo eje estratégico
@endsection

@section ("content")
<h1>Registrar nuevo eje estratégico</h1>
<div class="card card-default">
    <div class="card-block">
        @include ("secpla.tipos.ejes.forms.create")
    </div>
</div>
<hr>
@endsection