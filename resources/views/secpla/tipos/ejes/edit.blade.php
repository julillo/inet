@extends ("layouts.master")

@section ("title")
| Admin Secpla - Editar eje estratégico '{{ $ejeEstrategico->nombre }}'
@endsection

@section ("content")
<h1>Editar eje estratégico '{{ $ejeEstrategico->nombre }}'</h1>
<div class="card card-default">
    <div class="card-block">
        @include ("secpla.tipos.ejes.forms.edit")
    </div>
</div>
<hr>
@endsection