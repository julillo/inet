@extends ("layouts.master")

@section ("title")
| Admin Secpla - Ejes estrategicos
@endsection

@section ("content")
<h1>Ejes estrategicos</h1>
<div class="card card-default">
    <div class="card-header">
        <ul class="nav nav-tabs card-header-tabs">
            <li class="nav-item">
                <a id="link1"class="nav-link active" href="#">Vista tabla</a>
            </li>
            <li class="nav-item">
                <a id="link2"class="nav-link" href="#">Vista arbol</a>
            </li>
        </ul>
    </div>
    <div id="card1" class="card-block" >
        @include ("secpla.tipos.ejes.tables.index")
    </div>
    <div id="card2" class="card-block" style="display: none;">
        @include ("secpla.tipos.ejes.tree.master")
    </div>
</div>
<hr>
@endsection

@push ("css")
@include ("organizational-unit.units.css.tree")
@endpush

@push ("js")
@include ("secpla.tipos.ejes.js.index-tabs")
@include ("secpla.tipos.ejes.js.index-table")
@include ("organizational-unit.units.js.tree")
@endpush