<ul id="tree1">
    @foreach($roots as $root)
    <li>
        {{ $root->nombre }}
        @if(count($root->childs))
        @include('secpla.tipos.ejes.tree.manageChild',['childs' => $root->childs])
        @endif
    </li>
    @endforeach
</ul>