@permission('update-secpla-tipos-financiamiento')
<a class="btn btn-xs btn-primary" 
   href="{{ route("secpla.tipos.financiamiento.edit", $tipo) }}" 
   title="Editar">
    @icon('pencil', 'octicon-pencil')
</a>
@endpermission
@permission('update-secpla-tipos-financiamiento')
@if($tipo->status)
<button class="btn btn-xs btn-warning" 
    onclick="event.preventDefault();if(confirm('Desactivar tipo {{ $tipo->nombre }}?')){document.getElementById('status-form-{{$tipo->id}}').submit(); }" 
    title="Desactivar">
    @icon('alert', 'octicon-alert')
</button>
@else
<button class="btn btn-xs btn-success" 
    onclick="event.preventDefault();if(confirm('Activar tipo {{ $tipo->nombre }}?')){document.getElementById('status-form-{{$tipo->id}}').submit(); }" 
    title="Activar">
    @icon('alert', 'octicon-alert')
</button>
@endif
@endpermission
@permission('delete-secpla-tipos-financiamiento')
<button class="btn btn-xs btn-danger" 
    onclick="event.preventDefault();if(confirm('Eliminar tipo {{ $tipo->nombre }}?')){document.getElementById('delete-form-{{$tipo->id}}').submit(); }" 
    title="Eliminar">
    @icon('x', 'octicon-x')
</button>
@endpermission
@permission('update-secpla-tipos-financiamiento')
{{ Form::model($tipo, array('route' => array('secpla.tipos.financiamiento.switch-status', $tipo), 'method' => 'PUT', "id" => "status-form-".$tipo->id)) }}
{{ Form::close() }}
@endpermission
@permission('delete-secpla-tipos-financiamiento')
{{ Form::model($tipo, array('route' => array('secpla.tipos.financiamiento.delete', $tipo), 'method' => 'DELETE', "id" => "delete-form-".$tipo->id)) }}
{{ Form::close() }}
@endpermission