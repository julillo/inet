@extends ("layouts.master")

@section ("title")
| Admin Secpla - Registrar nuevo tipo de financiamiento
@endsection

@section ("content")
<h1>Registrar nuevo tipo de financiamiento</h1>
<div class="card card-default">
    <div class="card-block">
        @include ("secpla.tipos.financiamiento.forms.create")
    </div>
</div>
<hr>
@endsection