@extends ("layouts.master")

@section ("title")
| Admin Secpla - Editar tipo de financiamiento '{{ $financiamiento->nombre }}'
@endsection

@section ("content")
<h1>Editar tipo de financiamiento '{{ $financiamiento->nombre }}'</h1>
<div class="card card-default">
    <div class="card-block">
        @include ("secpla.tipos.financiamiento.forms.edit")
    </div>
</div>
<hr>
@endsection