@extends ("layouts.master")

@section ("title")
| Admin Secpla - Registrar nuevo tipo de proyecto
@endsection

@section ("content")
<h1>Registrar nuevo tipo de proyecto</h1>
<div class="card card-default">
    <div class="card-block">
        @include ("secpla.tipos.proyecto.forms.create")
    </div>
</div>
<hr>
@endsection