@extends ("layouts.master")

@section ("title")
| Admin Secpla - Editar tipo de proyecto '{{ $proyecto->nombre }}'
@endsection

@section ("content")
<h1>Editar tipo de proyecto '{{ $proyecto->nombre }}'</h1>
<div class="card card-default">
    <div class="card-block">
        @include ("secpla.tipos.proyecto.forms.edit")
    </div>
</div>
<hr>
@endsection