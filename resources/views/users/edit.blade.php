@extends ("layouts.master")

@section ("title")
| Editar usuario '{{ $user->name }}'
@endsection

@section ("content")
<h1>Editar usuario '{{ $user->name }}'</h1>
<div class="card card-default">
    <div class="card-header">
        <ul class="nav nav-tabs card-header-tabs">
            <li class="nav-item">
                <a id="link1"class="nav-link active" href="#">Información de login</a>
            </li>
            <li class="nav-item">
                <a id="link2"class="nav-link" href="#">Información de perfil</a>
            </li>
            <li class="nav-item">
                <a id="link3"class="nav-link" href="#">Roles</a>
            </li>
            <li class="nav-item">
                <a id="link4" class="nav-link" href="#">Permisos</a>
            </li>
        </ul>
    </div>
    <div id="card1" class="card-block" >
        @include ("users.forms.edit")
    </div>
    <div id="card2" class="card-block" style="display: none;">
        @include ("users.profile.forms.edit")
    </div>
    <div id="card3" class="card-block" style="display: none;">
        @include ("users.tables.roles")
    </div>
    <div id="card4" class="card-block" style="display: none;">
        @include ("users.tables.permissions")
    </div>
</div>
<hr>
@endsection

@push ("js")
@include ("users.tables.scripts.roles")
@include ("users.tables.scripts.permissions")
@include ("users.scripts.edit-tab")
@endpush