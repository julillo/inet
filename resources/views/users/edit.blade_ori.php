@extends ("layouts.master")

@section ("content")
<div class="container-fluid">
    <div class="row">
        @include ("layouts.side")
        <main class="col-sm-9 offset-sm-3 col-md-10 offset-md-2 pt-3">
            <h1>Edit {{ $user->name }}</h1>
            <div class="card card-default">
                <div class="card-block">
                    @include ("users.forms.edit")
                </div>
            </div>
            <hr>
            <h2>Roles</h2>
            <div class="card card-default">
                <div class="card-block">
                    @include ("users.tables.roles")
                </div>
            </div>
            <hr>
            <h2>Permisos</h2>
            <div class="card card-default">
                <div class="card-block">
                    @include ("users.tables.permissions")
                </div>
            </div>
        </main>
    </div>
</div>
@endsection

@section ("js")
@include ("users.tables.scripts.roles")
@include ("users.tables.scripts.permissions")
@endsection