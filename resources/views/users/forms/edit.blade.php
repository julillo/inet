<!-- if there are creation errors, they will show here -->
{{ Html::ul($errors->all()) }}

{{ Form::model($user, array('route' => array('users.update', $user), 'method' => 'PUT')) }}

<div class="form-group">
    {{ Form::label('name', 'Nombre', array("class" => "col-md-4 control-label")) }}
    <div class="col-md-6">
        {{ Form::text('name', null, array('class' => 'form-control')) }}
    </div>

</div>

<div class="form-group">
    {{ Form::label('email', 'E-mail', array("class" => "col-md-4 control-label")) }}
    <div class="col-md-6">
        {{ Form::email('email', null, array('class' => 'form-control')) }}
    </div>

</div>

<div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
    <label for="password" class="col-md-4 control-label">Nuevo password</label>

    <div class="col-md-6">
        <input id="password" type="password" class="form-control" name="password">

        @if ($errors->has('password'))
        <span class="help-block">
            <strong>{{ $errors->first('password') }}</strong>
        </span>
        @endif
    </div>
</div>

<div class="form-group">
    <label for="password-confirm" class="col-md-4 control-label">Confirmar Password</label>

    <div class="col-md-6">
        <input id="password-confirm" type="password" class="form-control" name="password_confirmation">
    </div>
</div>

<div class="form-group">
    {{ Form::label('status', 'Estado', array("class" => "col-md-4 control-label")) }}
    <div class="col-md-6">
        {{ Form::radio('status', '1') }} Activo | {{ Form::radio('status', '0') }} Inactivo
    </div>
</div>

{{ Form::submit('Guardar', array('class' => 'btn btn-primary')) }}

{{ Form::close() }}