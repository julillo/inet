@extends ("layouts.master")

@section ("title")
| Lista de usuarios
@endsection

@section ("content")
<h1>Lista de usuarios</h1>
@include ("users.tables.index")
<hr>
@endsection

@push ("js")
@include ("users.tables.scripts.index")
@endpush