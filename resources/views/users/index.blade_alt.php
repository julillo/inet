@extends ("layouts.master")

@section ("content")
<div class="container-fluid">
    <div class="row">
        @include ("layouts.side")
        <main class="col-sm-9 offset-sm-3 col-md-10 offset-md-2 pt-3">
            <h1>Lista de usuarios</h1>
            <div class="card card-default">
                <div class="card-header">
                    <ul class="nav nav-tabs card-header-tabs">
                        <li class="nav-item">
                            <a id="link1"class="nav-link active" href="#">Usuarios Activos</a>
                        </li>
                        <li class="nav-item">
                            <a id="link2"class="nav-link" href="#">Usuarios Inactivos</a>
                        </li>
<!--                        <li class="nav-item">
                            <a id="link3" class="nav-link" href="#">Permisos</a>
                        </li>-->
                    </ul>
                </div>
                <div id="asd1" class="card-block" >
                    @include ("users.tables.index")
                </div>
<!--                <div id="asd2" class="card-block" style="display: none;">
                </div>-->
<!--                <div id="asd3" class="card-block" style="display: none;">
                </div>-->
            </div>

        </main>
    </div>
</div>
@endsection

@section ("js")

@include ("users.tables.scripts.index")

@endsection