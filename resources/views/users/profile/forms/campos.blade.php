<div class="form-group">
    {{ Form::label('nombres', 'Nombres', array("class" => "col-md-4 control-label")) }}
    <div class="col-md-6">
        {{ Form::text('nombres', null, array('class' => 'form-control')) }}
    </div>
</div>
<div class="form-group">
    {{ Form::label('apellido_p', 'Apellido paterno', array("class" => "col-md-4 control-label")) }}
    <div class="col-md-6">
        {{ Form::text('apellido_p', null, array('class' => 'form-control')) }}
    </div>
</div>
<div class="form-group">
    {{ Form::label('apellido_m', 'Apellido materno', array("class" => "col-md-4 control-label")) }}
    <div class="col-md-6">
        {{ Form::text('apellido_m', null, array('class' => 'form-control')) }}
    </div>
</div>