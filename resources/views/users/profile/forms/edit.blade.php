<!-- if there are creation errors, they will show here -->
{{ Html::ul($errors->all()) }}

{{ Form::model($user->profile, array('route' => array('profiles.update', $user->profile), 'method' => 'PUT')) }}

@include ("users.profile.forms.campos")

{{ Form::submit('Guardar', array('class' => 'btn btn-primary')) }}

{{ Form::close() }}