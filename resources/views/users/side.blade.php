@permission('read-users')
<h3>@icon("person", "octicon-person", ["width" => 24, "height" => 32]) Usuarios</h3>
<ul class="nav nav-pills flex-column">
    @permission('read-users')
    <li class="nav-item">
        <a class="nav-link" href="{{route("users.index")}}">@icon("eye", "octicon-eye") Ver usuarios</a>
    </li>
    @endpermission
    @permission('create-users')
    <li class="nav-item">
        <a class="nav-link" href="{{route("users.register-form")}}">@icon("diff-added", "oction-diff-added") Crear usuario</a>
    </li>
    @endpermission
</ul>
<hr>
@endpermission