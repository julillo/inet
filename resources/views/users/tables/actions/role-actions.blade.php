@if ($user->roles()->find($role->id))
{{ Form::radio('roles['.$role->id."]", '1', true) }} Y | {{ Form::radio('roles['.$role->id."]", '0') }} N
@else
{{ Form::radio('roles['.$role->id."]", '1') }} Y | {{ Form::radio('roles['.$role->id."]", '0', true) }} N
@endif