@permission('update-users')
<a class="btn btn-xs btn-primary" href="{{ route("users.edit", $user) }}" title="Editar">@icon('pencil', 'octicon-pencil')</a>
@endpermission
@permission('delete-users')
<a class="btn btn-xs btn-danger" href="#" onclick="event.preventDefault();if(confirm('Eliminar usuario {{ $user->name }}?')){document.getElementById('delete-form-{{$user->id}}').submit();}" title="Eliminar">@icon('x', 'octicon-x')</a>
{{ Form::model($user, array('route' => array('users.delete', $user), 'method' => 'DELETE', "id" => "delete-form-".$user->id)) }}
{{ Form::close() }}
@endpermission