<div class="table-responsive">
    {{ Form::model($user, array('route' => array('users.update-permission', $user), 'method' => 'PUT')) }}
    <table id="user-permissions-table" class="table table-bordered" style="width: 100%">
        <thead>
            <tr>
                <th>#</th>
                <th>Name</th>
                <th>Description</th>
                <th>Action</th>
            </tr>
        </thead>
    </table>
    {{ Form::submit('Guardar', array('class' => 'btn btn-primary')) }}

    {{ Form::close() }}
</div>