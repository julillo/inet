<script type="text/javascript">
    var tabla_roles = function ($) {
        $('#user-roles-table').DataTable({
            serverSide: true,
            processing: true,
            ajax: '{{ route("users.roles-data", $user) }}',
            pagingType: "full_numbers",
            columns: [
                {data: 'id'},
                {data: 'display_name'},
                {data: 'description'},
                {data: 'action', orderable: false, searchable: false}
            ]
        });
    }(jQuery);
</script>