<?php

/*
  |--------------------------------------------------------------------------
  | Web Routes
  |--------------------------------------------------------------------------
  |
  | Here is where you can register web routes for your application. These
  | routes are loaded by the RouteServiceProvider within a group which
  | contains the "web" middleware group. Now create something great!
  |
 */

Auth::routes();

Route::group(['middleware' => ['preventbackbutton', 'auth']], function() {

    Route::get('/', function () {
        return redirect("login");
    });

    Route::get('/home', 'HomeController@index')->name('home');

    Route::group(["prefix" => "secpla", "namespace" => "Secpla"], function() {
        Route::group(['middleware' => ['preventbackbutton', 'auth'], "prefix" => "proyectos"], function() {
            Route::get("/index", ["middleware" => ["permission:read-secpla-proyectos"], "uses" => "ProyectoController@index"])->name("secpla.proyectos.index");
            Route::get("/index-data", ["middleware" => ["permission:read-secpla-proyectos"], "uses" => "ProyectoController@indexData"])->name("secpla.proyectos.index-data");
            Route::get("/create", ["middleware" => ["permission:create-secpla-proyectos"], "uses" => "ProyectoController@create"])->name("secpla.proyectos.create");
            Route::post("/create", ["middleware" => ["permission:create-secpla-proyectos"], "uses" => "ProyectoController@store"])->name("secpla.proyectos.store");
            Route::get("/{proyecto}/edit", ["middleware" => ["permission:update-secpla-proyectos"], "uses" => "ProyectoController@edit"])->name("secpla.proyectos.edit");
            Route::get("/{proyecto}/edit/requisitos", ["middleware" => ["permission:update-secpla-proyectos"], "uses" => "ProyectoController@requisitosData"])->name("secpla.proyectos.edit-requisitos");
            Route::put("/{proyecto}", ["middleware" => ["permission:update-secpla-proyectos"], "uses" => "ProyectoController@update"])->name("secpla.proyectos.update");
            Route::get("/{proyecto}/edit/map", ["middleware" => ["permission:update-secpla-proyectos"], "uses" => "ProyectoController@map"])->name("secpla.proyectos.map");
            Route::put("/{proyecto}/edit/map", ["middleware" => ["permission:update-secpla-proyectos"], "uses" => "ProyectoController@saveMap"])->name("secpla.proyectos.save-map");
            Route::get("/{proyecto}/map", ["middleware" => ["permission:update-secpla-proyectos"], "uses" => "ProyectoController@getMap"])->name("secpla.proyectos.get-map");
            Route::put("/{proyecto}/requisitos", ["middleware" => ["permission:update-secpla-proyectos"], "uses" => "ProyectoController@updateRequisitos"])->name("secpla.proyectos.update-requisitos");
            Route::delete("/{proyecto}", ["middleware" => ["permission:delete-secpla-proyectos"], "uses" => "ProyectoController@delete"])->name("secpla.proyectos.delete");
        });
    });

    Route::group(["prefix" => "profile"], function() {
        Route::get("/show/{profile}", ["middleware" => ["permission:read-own-profile"], "uses" => "ProfileController@showOwn"])->name("profiles.own.show");
    });

    Route::group(["prefix" => "dom"], function() {
        Route::group(["prefix" => "madir", "namespace" => "Dom\Madir\Terrenos"], function() {
            Route::get("/index", ["middleware" => ["permission:read-madir-user|read-madir-direcciones"], "uses" => "TerrenoController@indexUser"])->name("madir.user.index");
            Route::get("/index-data", ["middleware" => ["permission:read-madir-user|read-madir-direcciones"], "uses" => "TerrenoController@indexUserData"])->name("madir.user.index-data");
        });
    });
});

/*
  |--------------------------------------------------------------------------
  | Admin subdir
  |--------------------------------------------------------------------------
 */
Route::group(['middleware' => ['preventbackbutton', 'auth'], "prefix" => "admin"], function() {

    /*
      |----------------------------------------------------------------------
      | ACL Users subdir
      |----------------------------------------------------------------------
     */
    Route::group(["prefix" => "users"], function() {
        Route::group(["prefix" => "credentials"], function() {
            Route::get("/index", ['middleware' => ['permission:read-users'], 'uses' => 'User\UserController@index'])->name("users.index");
            Route::get('/index-data', ['middleware' => ['permission:read-users'], 'uses' => 'User\UserController@data'])->name("users.index-data");
            Route::get("/create", ['middleware' => ['permission:create-users'], 'uses' => "Auth\RegisterController@showRegistrationForm"])->name("users.register-form");
            Route::post("/create", ['middleware' => ['permission:create-users'], 'uses' => "Auth\RegisterController@register"])->name("users.register");
            Route::get("/{user}", ["middleware" => ['permission:update-users'], "uses" => "User\UserController@show"])->name("users.show");
            Route::get("/{user}/edit", ["middleware" => ['permission:update-users'], "uses" => "User\UserController@edit"])->name("users.edit");
            Route::get('/{user}/edit/role-data', ['middleware' => ['permission:update-users'], 'uses' => 'User\UserController@roleData'])->name("users.roles-data");
            Route::get('/{user}/edit/permission-data', ['middleware' => ['permission:update-users'], 'uses' => 'User\UserController@permissionData'])->name("users.permissions-data");
            Route::put("/{user}", ["middleware" => ['permission:update-users'], "uses" => "User\UserController@update"])->name("users.update");
            Route::put('/{user}/role', ['middleware' => ['permission:update-users'], 'uses' => 'User\UserController@updateRoles'])->name("users.update-roles");
            Route::put('/{user}/permission', ['middleware' => ['permission:update-users'], 'uses' => 'User\UserController@updatePermissions'])->name("users.update-permission");
            Route::delete("/{user}", ["middleware" => ['permission:delete-users'], "uses" => "User\UserController@delete"])->name("users.delete");
        });
        Route::group(["prefix" => "profiles"], function() {
//            Route::get("/show/{profile}", ["middleware" => ["permission:read-profile"], "uses" => "ProfileController@show"])->name("profiles.show");
            Route::put("/{profile}", ["middleware" => ["permission:update-profile"], "uses" => "ProfileController@update"])->name("profiles.update");
        });
    });
    /*
      |----------------------------------------------------------------------
      | ACL Roles subdir
      |----------------------------------------------------------------------
     */
    Route::group(["prefix" => "roles", "namespace" => "Acl"], function() {
        Route::get("/index", ['middleware' => ['permission:read-acl'], 'uses' => 'RoleController@index'])->name("roles.index");
        Route::get('/index-data', ['middleware' => ['permission:read-acl'], 'uses' => 'RoleController@data'])->name("roles.index-data");
        Route::get("/create", ["middleware" => ["permission:create-acl"], "uses" => "RoleController@create"])->name("roles.create");
        Route::post("/create", ["middleware" => ["permission:create-acl"], "uses" => "RoleController@store"])->name("roles.store");
        Route::get("/{role}/edit", ["middleware" => ["permission:update-acl"], "uses" => "RoleController@edit"])->name("roles.edit");
        Route::get("/{role}/edit/permissions-data", ["middleware" => ["permission:update-acl"], "uses" => "RoleController@permissionsData"])->name("roles.permissions-data");
        Route::put("/{role}", ["middleware" => ["permission:update-acl"], "uses" => "RoleController@update"])->name("roles.update");
        Route::put("/{role}/permissions", ["middleware" => ["permission:update-acl"], "uses" => "RoleController@updatePermissions"])->name("roles.update-permissions");
        Route::delete("/{role}", ["middleware" => ["permission:delete-acl"], "uses" => "RoleController@delete"])->name("roles.delete");
    });
    /*
      |----------------------------------------------------------------------
      | ACL permisos subdir
      |----------------------------------------------------------------------
     */
    Route::group(["prefix" => "permissions", "namespace" => "Acl"], function() {
        Route::get("/index", ['middleware' => ['permission:read-acl'], 'uses' => 'PermissionController@index'])->name("permissions.index");
        Route::get('/index-data', ['middleware' => ['permission:read-acl'], 'uses' => 'PermissionController@data'])->name("permissions.index-data");
        Route::get("/create", ["middleware" => ["permission:create-acl"], "uses" => "PermissionController@create"])->name("permissions.create");
        Route::post("/create", ["middleware" => ["permission:create-acl"], "uses" => "PermissionController@store"])->name("permissions.store");
        Route::get("/{permission}/edit", ["middleware" => ["permission:update-acl"], "uses" => "PermissionController@edit"])->name("permissions.edit");
        Route::put("/{permission}", ["middleware" => ["permission:update-acl"], "uses" => "PermissionController@update"])->name("permissions.update");
        Route::delete("/{permission}", ["middleware" => ["permission:delete-acl"], "uses" => "PermissionController@delete"])->name("permissions.delete");
    });
    /*
      |----------------------------------------------------------------------
      | Unidades organizacionales subdir
      |----------------------------------------------------------------------
     */
    Route::group(["prefix" => "ou"], function() {
        Route::group(["prefix" => "types"], function() {
            Route::get("/index", ["middleware" => ["permission:read-organizational-unit-types"], "uses" => "OrganizationalUnitTypeController@index"])->name("ou-types.index");
            Route::get("/index-data", ["middleware" => ["permission:read-organizational-unit-types"], "uses" => "OrganizationalUnitTypeController@indexData"])->name("ou-types.index-data");
            Route::get("/create", ["middleware" => ["permission:create-organizational-unit-types"], "uses" => "OrganizationalUnitTypeController@create"])->name("ou-types.create");
            Route::post("/create", ["middleware" => ["permission:create-organizational-unit-types"], "uses" => "OrganizationalUnitTypeController@store"])->name("ou-types.store");
            Route::get("/{organizationalUnitType}/edit", ["middleware" => ["permission:update-organizational-unit-types"], "uses" => "OrganizationalUnitTypeController@edit"])->name("ou-types.edit");
            Route::put("/{organizationalUnitType}", ["middleware" => ["permission:update-organizational-unit-types"], "uses" => "OrganizationalUnitTypeController@update"])->name("ou-types.update");
        });
        Route::get("/index", ["middleware" => ["permission:read-organizational-units"], "uses" => "OrganizationalUnitController@index"])->name("ous.index");
        Route::get("/index-data", ["middleware" => ["permission:read-organizational-units"], "uses" => "OrganizationalUnitController@indexData"])->name("ous.index-data");
        Route::get("/create", ["middleware" => ["permission:create-organizational-units"], "uses" => "OrganizationalUnitController@create"])->name("ous.create");
        Route::post("/create", ["middleware" => ["permission:create-organizational-units"], "uses" => "OrganizationalUnitController@store"])->name("ous.store");
        Route::get("/{organizationalUnit}/edit", ["middleware" => ["permission:update-organizational-units"], "uses" => "OrganizationalUnitController@edit"])->name("ous.edit");
        Route::put("/{organizationalUnit}", ["middleware" => ["permission:update-organizational-units"], "uses" => "OrganizationalUnitController@update"])->name("ous.update");
    });
    /*
      |----------------------------------------------------------------------
      | Secpla subdir
      |----------------------------------------------------------------------
     */
    Route::group(["prefix" => "secpla"], function() {
        Route::group(["prefix" => "tipos"], function() {
            Route::group(["prefix" => "proyectos", "namespace" => "Secpla\Tipos"], function() {
                Route::get("/index", ["middleware" => ["permission:read-secpla-tipos-proyectos"], "uses" => "ProyectoController@index"])->name("secpla.tipos.proyecto.index");
                Route::get("/index-data", ["middleware" => ["permission:read-secpla-tipos-proyectos"], "uses" => "ProyectoController@indexData"])->name("secpla.tipos.proyecto.index-data");
                Route::get("/create", ["middleware" => ["permission:create-secpla-tipos-proyectos"], "uses" => "ProyectoController@create"])->name("secpla.tipos.proyecto.create");
                Route::post("/create", ["middleware" => ["permission:create-secpla-tipos-proyectos"], "uses" => "ProyectoController@store"])->name("secpla.tipos.proyecto.store");
                Route::get("/{proyecto}/edit", ["middleware" => ["permission:update-secpla-tipos-proyectos"], "uses" => "ProyectoController@edit"])->name("secpla.tipos.proyecto.edit");
                Route::put("/{proyecto}", ["middleware" => ["permission:update-secpla-tipos-proyectos"], "uses" => "ProyectoController@update"])->name("secpla.tipos.proyecto.update");
                Route::put("/{proyecto}/switch-status", ["middleware" => ["permission:update-secpla-tipos-proyectos"], "uses" => "ProyectoController@switchStatus"])->name("secpla.tipos.proyecto.switch-status");
                Route::delete("/{proyecto}", ["middleware" => ["permission:delete-secpla-tipos-proyectos"], "uses" => "ProyectoController@delete"])->name("secpla.tipos.proyecto.delete");
            });
            Route::group(["prefix" => "financiamiento", "namespace" => "Secpla\Tipos"], function() {
                Route::get("/index", ["middleware" => ["permission:read-secpla-tipos-financiamiento"], "uses" => "FinanciamientoController@index"])->name("secpla.tipos.financiamiento.index");
                Route::get("/index-data", ["middleware" => ["permission:read-secpla-tipos-financiamiento"], "uses" => "FinanciamientoController@indexData"])->name("secpla.tipos.financiamiento.index-data");
                Route::get("/create", ["middleware" => ["permission:create-secpla-tipos-financiamiento"], "uses" => "FinanciamientoController@create"])->name("secpla.tipos.financiamiento.create");
                Route::post("/create", ["middleware" => ["permission:create-secpla-tipos-financiamiento"], "uses" => "FinanciamientoController@store"])->name("secpla.tipos.financiamiento.store");
                Route::get("/{financiamiento}/edit", ["middleware" => ["permission:update-secpla-tipos-financiamiento"], "uses" => "FinanciamientoController@edit"])->name("secpla.tipos.financiamiento.edit");
                Route::put("/{financiamiento}", ["middleware" => ["permission:update-secpla-tipos-financiamiento"], "uses" => "FinanciamientoController@update"])->name("secpla.tipos.financiamiento.update");
                Route::put("/{financiamiento}/switch-status", ["middleware" => ["permission:update-secpla-tipos-financiamiento"], "uses" => "FinanciamientoController@switchStatus"])->name("secpla.tipos.financiamiento.switch-status");
                Route::delete("/{financiamiento}", ["middleware" => ["permission:delete-secpla-tipos-financiamiento"], "uses" => "FinanciamientoController@delete"])->name("secpla.tipos.financiamiento.delete");
            });
            Route::group(["prefix" => "cuentas", "namespace" => "Secpla\Tipos"], function() {
                Route::get("/index", ["middleware" => ["permission:read-secpla-tipos-cuentas"], "uses" => "CuentaController@index"])->name("secpla.tipos.cuentas.index");
                Route::get("/index-data", ["middleware" => ["permission:read-secpla-tipos-cuentas"], "uses" => "CuentaController@indexData"])->name("secpla.tipos.cuentas.index-data");
                Route::get("/create", ["middleware" => ["permission:create-secpla-tipos-cuentas"], "uses" => "CuentaController@create"])->name("secpla.tipos.cuentas.create");
                Route::post("/create", ["middleware" => ["permission:create-secpla-tipos-cuentas"], "uses" => "CuentaController@store"])->name("secpla.tipos.cuentas.store");
                Route::get("/{cuenta}/edit", ["middleware" => ["permission:update-secpla-tipos-cuentas"], "uses" => "CuentaController@edit"])->name("secpla.tipos.cuentas.edit");
                Route::put("/{cuenta}", ["middleware" => ["permission:update-secpla-tipos-cuentas"], "uses" => "CuentaController@update"])->name("secpla.tipos.cuentas.update");
                Route::put("/{cuenta}/switch-status", ["middleware" => ["permission:update-secpla-tipos-cuentas"], "uses" => "CuentaController@switchStatus"])->name("secpla.tipos.cuentas.switch-status");
                Route::delete("/{cuenta}", ["middleware" => ["permission:delete-secpla-tipos-cuentas"], "uses" => "CuentaController@delete"])->name("secpla.tipos.cuentas.delete");
            });
        });
        Route::group(["prefix" => "ejes-estrategicos", "namespace" => "Secpla\Tipos"], function() {
            Route::get("/index", ["middleware" => ["permission:read-secpla-ejes-estrategicos"], "uses" => "EjeEstrategicoController@index"])->name("secpla.tipos.ejes.index");
            Route::get("/index-data", ["middleware" => ["permission:read-secpla-ejes-estrategicos"], "uses" => "EjeEstrategicoController@indexData"])->name("secpla.tipos.ejes.index-data");
            Route::get("/create", ["middleware" => ["permission:create-secpla-ejes-estrategicos"], "uses" => "EjeEstrategicoController@create"])->name("secpla.tipos.ejes.create");
            Route::post("/create", ["middleware" => ["permission:create-secpla-ejes-estrategicos"], "uses" => "EjeEstrategicoController@store"])->name("secpla.tipos.ejes.store");
            Route::get("/{ejeEstrategico}/edit", ["middleware" => ["permission:update-secpla-ejes-estrategicos"], "uses" => "EjeEstrategicoController@edit"])->name("secpla.tipos.ejes.edit");
            Route::put("/{ejeEstrategico}", ["middleware" => ["permission:update-secpla-ejes-estrategicos"], "uses" => "EjeEstrategicoController@update"])->name("secpla.tipos.ejes.update");
            Route::put("/{ejeEstrategico}/switch-status", ["middleware" => ["permission:update-secpla-ejes-estrategicos"], "uses" => "EjeEstrategicoController@switchStatus"])->name("secpla.tipos.ejes.switch-status");
            Route::delete("/{ejeEstrategico}", ["middleware" => ["permission:delete-secpla-ejes-estrategicos"], "uses" => "EjeEstrategicoController@delete"])->name("secpla.tipos.ejes.delete");
        });
        Route::group(["prefix" => "cuentas", "namespace" => "Secpla"], function() {
            Route::get("/index", ["middleware" => ["permission:read-secpla-cuentas"], "uses" => "CuentaController@index"])->name("secpla.cuentas.index");
            Route::get("/index-data", ["middleware" => ["permission:read-secpla-cuentas"], "uses" => "CuentaController@indexData"])->name("secpla.cuentas.index-data");
            Route::get("/create", ["middleware" => ["permission:create-secpla-cuentas"], "uses" => "CuentaController@create"])->name("secpla.cuentas.create");
            Route::post("/create", ["middleware" => ["permission:create-secpla-cuentas"], "uses" => "CuentaController@store"])->name("secpla.cuentas.store");
            Route::get("/{cuenta}/edit", ["middleware" => ["permission:update-secpla-cuentas"], "uses" => "CuentaController@edit"])->name("secpla.cuentas.edit");
            Route::put("/{cuenta}", ["middleware" => ["permission:update-secpla-cuentas"], "uses" => "CuentaController@update"])->name("secpla.cuentas.update");
            Route::put("/{cuenta}/switch-status", ["middleware" => ["permission:update-secpla-cuentas"], "uses" => "CuentaController@switchStatus"])->name("secpla.cuentas.switch-status");
            Route::delete("/{cuenta}", ["middleware" => ["permission:delete-secpla-cuentas"], "uses" => "CuentaController@delete"])->name("secpla.cuentas.delete");
        });
    });
    /*
      |----------------------------------------------------------------------
      | DOM subdir
      |----------------------------------------------------------------------
     */
    Route::group(["prefix" => "dom"], function() {
        Route::group(["prefix" => "madir"], function() {
            Route::group(["prefix" => "terrenos", "namespace" => "Dom\Madir\Terrenos"], function() {
                Route::group(["prefix" => "tipos"], function() {
                    Route::get("/index", ["middleware" => ["permission:read-madir-tipos-terrenos"], "uses" => "TipoTerrenoController@index"])->name("madir.tipos.territoriales.index");
                    Route::get("/index-data", ["middleware" => ["permission:read-madir-tipos-terrenos"], "uses" => "TipoTerrenoController@indexData"])->name("madir.tipos.territoriales.index-data");
                    Route::get("/create", ["middleware" => ["permission:create-madir-tipos-terrenos"], "uses" => "TipoTerrenoController@create"])->name("madir.tipos.territoriales.create");
                    Route::post("/create", ["middleware" => ["permission:create-madir-tipos-terrenos"], "uses" => "TipoTerrenoController@store"])->name("madir.tipos.territoriales.store");
                    Route::get("/{tipoTerreno}/edit", ["middleware" => ["permission:update-madir-tipos-terrenos"], "uses" => "TipoTerrenoController@edit"])->name("madir.tipos.territoriales.edit");
                    Route::put("/{tipoTerreno}", ["middleware" => ["permission:update-madir-tipos-terrenos"], "uses" => "TipoTerrenoController@update"])->name("madir.tipos.territoriales.update");
                    Route::put("/{tipoTerreno}/switch-status", ["middleware" => ["permission:update-madir-tipos-terrenos"], "uses" => "TipoTerrenoController@switchStatus"])->name("madir.tipos.territoriales.switch-status");
                    Route::delete("/{tipoTerreno}", ["middleware" => ["permission:delete-madir-tipos-terrenos"], "uses" => "TipoTerrenoController@delete"])->name("madir.tipos.territoriales.delete");
                });
                Route::group(["prefix" => "maestro"], function() {
                    Route::get("/index", ["middleware" => ["permission:read-madir-terrenos"], "uses" => "TerrenoController@index"])->name("madir.maestro.territoriales.index");
                    Route::get("/index-data", ["middleware" => ["permission:read-madir-terrenos"], "uses" => "TerrenoController@indexData"])->name("madir.maestro.territoriales.index-data");
                    Route::get('/find', ["middleware" => ["permission:read-madir-bases-viales"], "uses" => "TerrenoController@find"])->name("madir.maestro.territoriales.find");
                    Route::get("/create/{terreno?}", ["middleware" => ["permission:create-madir-terrenos"], "uses" => "TerrenoController@create"])->name("madir.maestro.territoriales.create");
                    Route::post("/create", ["middleware" => ["permission:create-madir-terrenos"], "uses" => "TerrenoController@store"])->name("madir.maestro.territoriales.store");
                    Route::get("/{terreno}/edit", ["middleware" => ["permission:update-madir-terrenos"], "uses" => "TerrenoController@edit"])->name("madir.maestro.territoriales.edit");
                    Route::put("/{terreno}", ["middleware" => ["permission:update-madir-terrenos"], "uses" => "TerrenoController@update"])->name("madir.maestro.territoriales.update");
                    Route::put("/{terreno}/switch-status", ["middleware" => ["permission:update-madir-terrenos"], "uses" => "TerrenoController@switchStatus"])->name("madir.maestro.territoriales.switch-status");
                    Route::delete("/{terreno}", ["middleware" => ["permission:delete-madir-terrenos"], "uses" => "TerrenoController@delete"])->name("madir.maestro.territoriales.delete");
                });
            });
            Route::group(["prefix" => "direcciones", "namespace" => "Dom\Madir\Direcciones"], function() {
                Route::group(["prefix" => "tipos-viales"], function() {
                    Route::get("/index", ["middleware" => ["permission:read-madir-tipos-viales"], "uses" => "TipoVialController@index"])->name("madir.tipos.viales.index");
                    Route::get("/index-data", ["middleware" => ["permission:read-madir-tipos-viales"], "uses" => "TipoVialController@indexData"])->name("madir.tipos.viales.index-data");
                    Route::get("/create", ["middleware" => ["permission:create-madir-tipos-viales"], "uses" => "TipoVialController@create"])->name("madir.tipos.viales.create");
                    Route::post("/create", ["middleware" => ["permission:create-madir-tipos-viales"], "uses" => "TipoVialController@store"])->name("madir.tipos.viales.store");
                    Route::get("/{tipoVial}/edit", ["middleware" => ["permission:update-madir-tipos-viales"], "uses" => "TipoVialController@edit"])->name("madir.tipos.viales.edit");
                    Route::put("/{tipoVial}", ["middleware" => ["permission:update-madir-tipos-viales"], "uses" => "TipoVialController@update"])->name("madir.tipos.viales.update");
                    Route::put("/{tipoVial}/switch-status", ["middleware" => ["permission:update-madir-tipos-viales"], "uses" => "TipoVialController@switchStatus"])->name("madir.tipos.viales.switch-status");
                    Route::delete("/{tipoVial}", ["middleware" => ["permission:delete-madir-tipos-viales"], "uses" => "TipoVialController@delete"])->name("madir.tipos.viales.delete");
                });
                Route::group(["prefix" => "bases-viales"], function() {
                    Route::get("/index", ["middleware" => ["permission:read-madir-bases-viales"], "uses" => "BaseVialController@index"])->name("madir.bases.viales.index");
                    Route::get("/index-data", ["middleware" => ["permission:read-madir-bases-viales"], "uses" => "BaseVialController@indexData"])->name("madir.bases.viales.index-data");
                    Route::get('/find', ["middleware" => ["permission:read-madir-bases-viales"], "uses" => "BaseVialController@find"])->name("madir.bases.viales.find");
                    Route::get("/create", ["middleware" => ["permission:create-madir-bases-viales"], "uses" => "BaseVialController@create"])->name("madir.bases.viales.create");
                    Route::post("/create", ["middleware" => ["permission:create-madir-bases-viales"], "uses" => "BaseVialController@store"])->name("madir.bases.viales.store");
                    Route::get("/{baseVial}/edit", ["middleware" => ["permission:update-madir-bases-viales"], "uses" => "BaseVialController@edit"])->name("madir.bases.viales.edit");
                    Route::put("/{baseVial}", ["middleware" => ["permission:update-madir-bases-viales"], "uses" => "BaseVialController@update"])->name("madir.bases.viales.update");
                    Route::put("/{baseVial}/switch-status", ["middleware" => ["permission:update-madir-bases-viales"], "uses" => "BaseVialController@switchStatus"])->name("madir.bases.viales.switch-status");
                    Route::delete("/{baseVial}", ["middleware" => ["permission:delete-madir-bases-viales"], "uses" => "BaseVialController@delete"])->name("madir.bases.viales.delete");
                });
                Route::group(["prefix" => "segmentos-viales"], function() {
                    Route::get("/index", ["middleware" => ["permission:read-madir-segmentos-viales"], "uses" => "SegmentoVialController@index"])->name("madir.segmentos.viales.index");
                    Route::get("/index-data", ["middleware" => ["permission:read-madir-segmentos-viales"], "uses" => "SegmentoVialController@indexData"])->name("madir.segmentos.viales.index-data");
                    Route::get('/find', ["middleware" => ["permission:read-madir-segmentos-viales"], "uses" => "SegmentoVialController@find"])->name("madir.segmentos.viales.find");
                    Route::get("/create", ["middleware" => ["permission:create-madir-segmentos-viales"], "uses" => "SegmentoVialController@create"])->name("madir.segmentos.viales.create");
                    Route::post("/create", ["middleware" => ["permission:create-madir-segmentos-viales"], "uses" => "SegmentoVialController@store"])->name("madir.segmentos.viales.store");
                    Route::get("/{segmentoVial}/edit", ["middleware" => ["permission:update-madir-segmentos-viales"], "uses" => "SegmentoVialController@edit"])->name("madir.segmentos.viales.edit");
                    Route::put("/{segmentoVial}", ["middleware" => ["permission:update-madir-segmentos-viales"], "uses" => "SegmentoVialController@update"])->name("madir.segmentos.viales.update");
                    Route::put("/{segmentoVial}/switch-status", ["middleware" => ["permission:update-madir-segmentos-viales"], "uses" => "SegmentoVialController@switchStatus"])->name("madir.segmentos.viales.switch-status");
                    Route::delete("/{segmentoVial}", ["middleware" => ["permission:delete-madir-segmentos-viales"], "uses" => "SegmentoVialController@delete"])->name("madir.segmentos.viales.delete");
                });
                Route::group(["prefix" => "maestro"], function() {
                    Route::get("/index", ["middleware" => ["permission:read-madir-segmentos-viales"], "uses" => "DireccionController@index"])->name("madir.maestro.direcciones.index");
                    Route::get("/index-data", ["middleware" => ["permission:read-madir-segmentos-viales"], "uses" => "DireccionController@indexData"])->name("madir.maestro.direcciones.index-data");
                    Route::get("/create", ["middleware" => ["permission:create-madir-segmentos-viales"], "uses" => "DireccionController@create"])->name("madir.maestro.direcciones.create");
                    Route::post("/create", ["middleware" => ["permission:create-madir-segmentos-viales"], "uses" => "DireccionController@store"])->name("madir.maestro.direcciones.store");
                    Route::get("/{direccion}/edit", ["middleware" => ["permission:update-madir-segmentos-viales"], "uses" => "DireccionController@edit"])->name("madir.maestro.direcciones.edit");
                    Route::put("/{direccion}", ["middleware" => ["permission:update-madir-segmentos-viales"], "uses" => "DireccionController@update"])->name("madir.maestro.direcciones.update");
                    Route::put("/{direccion}/switch-status", ["middleware" => ["permission:update-madir-segmentos-viales"], "uses" => "DireccionController@switchStatus"])->name("madir.maestro.direcciones.switch-status");
                    Route::delete("/{direccion}", ["middleware" => ["permission:delete-madir-segmentos-viales"], "uses" => "DireccionController@delete"])->name("madir.maestro.direcciones.delete");
                });
            });
        });
    });
});
