<?php

/*
  |--------------------------------------------------------------------------
  | Web Routes
  |--------------------------------------------------------------------------
  |
  | Here is where you can register web routes for your application. These
  | routes are loaded by the RouteServiceProvider within a group which
  | contains the "web" middleware group. Now create something great!
  |
 */

Route::get('/', function () {
//    return view('welcome');
//    return view('master');
//    return view('home');
    return redirect("login");
});

Auth::routes();

//Route::get("/login", "Auth\LoginController@showLoginForm")->name("login");
//
//Route::post("/login", "Auth\LoginController@login")->name("login");
//
//Route::post("/logout", "Auth\LoginController@logout")->name("logout");
//
////Route::get("/user/password/request", "Auth\ForgotPasswordController@showLinkRequestForm")->name("password.request");
////
////Route::post("/user/password/email", "Auth\ForgotPasswordController@sendResetLinkEmail")->name("password.email");
////
////Route::get("/user/password/reset", "Auth\ResetPasswordController@showResetForm")->name("password.reset");
////
////Route::post("/user/password/reset", "Auth\ResetPasswordController@reset")->name("password.request");

//Route::get("/user/register", ['middleware' => ['role:administrator'], "uses" => "Auth\RegisterController@showRegistrationForm"])->name("register");
//
//Route::post("/user/register", ['middleware' => ['role:administrator'], "uses" => "Auth\RegisterController@register"])->name("register");

Route::group(['prefix' => 'admin', 'middleware' => ['role:administrator']], function() {
    
    Route::get("/register", "Auth\RegisterController@showRegistrationForm")->name("register");
    Route::post("/register", "Auth\RegisterController@register")->name("register");
    
//    Route::get('/', 'AdminController@welcome');
//    Route::get('/manage', ['middleware' => ['permission:manage-admins'], 'uses' => 'AdminController@manageAdmins']);
});



Route::get('/home', 'HomeController@index')->name('home');
