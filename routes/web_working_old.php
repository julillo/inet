<?php

/*
  |--------------------------------------------------------------------------
  | Web Routes
  |--------------------------------------------------------------------------
  |
  | Here is where you can register web routes for your application. These
  | routes are loaded by the RouteServiceProvider within a group which
  | contains the "web" middleware group. Now create something great!
  |
 */

Auth::routes();

Route::group(['middleware' => ['preventbackbutton', 'auth']], function() {
    Route::get('/', function () {
        return redirect("login");
    });
    Route::get('/home', 'HomeController@index')->name('home');
});

Route::group(['middleware' => ['preventbackbutton', 'auth'], 'prefix' => 'admin/users'], function() {
    Route::get("/index", ['middleware' => ['permission:read-users'], 'uses' => 'User\UserController@index'])->name("users.index");
    Route::get('/index-data', ['middleware' => ['permission:read-users'], 'uses' => 'User\UserController@data'])->name("users.index-data");
    Route::get("/create", ['middleware' => ['permission:create-users'], 'uses' => "Auth\RegisterController@showRegistrationForm"])->name("users.register-form");
    Route::post("/create", ['middleware' => ['permission:create-users'], 'uses' => "Auth\RegisterController@register"])->name("users.register");
    Route::get("/{user}", ["middleware" => ['permission:update-users'], "uses" => "User\UserController@show"])->name("users.show");
    Route::get("/{user}/edit", ["middleware" => ['permission:update-users'], "uses" => "User\UserController@edit"])->name("users.edit");
    Route::get('/{user}/edit/role-data', ['middleware' => ['permission:update-users'], 'uses' => 'User\UserController@roleData'])->name("users.roles-data");
    Route::get('/{user}/edit/permission-data', ['middleware' => ['permission:update-users'], 'uses' => 'User\UserController@permissionData'])->name("users.permissions-data");
    Route::put("/{user}", ["middleware" => ['permission:update-users'], "uses" => "User\UserController@update"])->name("users.update");
    Route::put('/{user}/role', ['middleware' => ['permission:update-users'], 'uses' => 'User\UserController@updateRoles'])->name("users.update-roles");
    Route::put('/{user}/permission', ['middleware' => ['permission:update-users'], 'uses' => 'User\UserController@updatePermissions'])->name("users.update-permission");
    Route::delete("/{user}", ["middleware" => ['permission:delete-users'], "uses" => "User\UserController@delete"])->name("users.delete");
});

Route::group(['middleware' => ['preventbackbutton', 'auth'], 'prefix' => 'admin/roles'], function() {
    Route::get("/index", ['middleware' => ['permission:read-acl'], 'uses' => 'Acl\RoleController@index'])->name("roles.index");
    Route::get('/index-data', ['middleware' => ['permission:read-acl'], 'uses' => 'Acl\RoleController@data'])->name("roles.index-data");
    Route::get("/create", ["middleware" => ["permission:create-acl"], "uses" => "Acl\RoleController@create"])->name("roles.create");
    Route::post("/create", ["middleware" => ["permission:create-acl"], "uses" => "Acl\RoleController@store"])->name("roles.store");
    Route::get("/{role}/edit", ["middleware" => ["permission:update-acl"], "uses" => "Acl\RoleController@edit"])->name("roles.edit");
    Route::get("/{role}/edit/permissions-data", ["middleware" => ["permission:update-acl"], "uses" => "Acl\RoleController@permissionsData"])->name("roles.permissions-data");
    Route::put("/{role}", ["middleware" => ["permission:update-acl"], "uses" => "Acl\RoleController@update"])->name("roles.update");
    Route::put("/{role}/permissions", ["middleware" => ["permission:update-acl"], "uses" => "Acl\RoleController@updatePermissions"])->name("roles.update-permissions");
    Route::delete("/{role}", ["middleware" => ["permission:delete-acl"], "uses" => "Acl\RoleController@delete"])->name("roles.delete");
});

Route::group(['middleware' => ['preventbackbutton', 'auth'], 'prefix' => 'admin/permissions'], function() {
    Route::get("/index", ['middleware' => ['permission:read-acl'], 'uses' => 'Acl\PermissionController@index'])->name("permissions.index");
    Route::get('/index-data', ['middleware' => ['permission:read-acl'], 'uses' => 'Acl\PermissionController@data'])->name("permissions.index-data");
    Route::get("/create", ["middleware" => ["permission:create-acl"], "uses" => "Acl\PermissionController@create"])->name("permissions.create");
    Route::post("/create", ["middleware" => ["permission:create-acl"], "uses" => "Acl\PermissionController@store"])->name("permissions.store");
    Route::get("/{permission}/edit", ["middleware" => ["permission:update-acl"], "uses" => "Acl\PermissionController@edit"])->name("permissions.edit");
    Route::put("/{permission}", ["middleware" => ["permission:update-acl"], "uses" => "Acl\PermissionController@update"])->name("permissions.update");
    Route::delete("/{permission}", ["middleware" => ["permission:delete-acl"], "uses" => "Acl\PermissionController@delete"])->name("permissions.delete");
});

Route::group(['middleware' => ['preventbackbutton', 'auth'], "prefix" => "admin/profile"], function() {
//    Route::get("/show/{profile}", ["middleware" => ["permission:read-profile"], "uses" => "ProfileController@show"])->name("profiles.show");
    Route::put("/{profile}", ["middleware" => ["permission:update-profile"], "uses" => "ProfileController@update"])->name("profiles.update");
});

Route::group(['middleware' => ['preventbackbutton', 'auth'], "prefix" => "admin/ou"], function() {
    Route::get("/index", ["middleware" => ["permission:read-organizational-units"], "uses" => "OrganizationalUnitController@index"])->name("ous.index");
    Route::get("/index-data", ["middleware" => ["permission:read-organizational-units"], "uses" => "OrganizationalUnitController@indexData"])->name("ous.index-data");
    Route::get("/create", ["middleware" => ["permission:create-organizational-units"], "uses" => "OrganizationalUnitController@create"])->name("ous.create");
    Route::post("/create", ["middleware" => ["permission:create-organizational-units"], "uses" => "OrganizationalUnitController@store"])->name("ous.store");
    Route::get("/{organizationalUnit}/edit", ["middleware" => ["permission:update-organizational-units"], "uses" => "OrganizationalUnitController@edit"])->name("ous.edit");
    Route::put("/{organizationalUnit}", ["middleware" => ["permission:update-organizational-units"], "uses" => "OrganizationalUnitController@update"])->name("ous.update");
});

Route::group(['middleware' => ['preventbackbutton', 'auth'], "prefix" => "admin/ou-types"], function() {
    Route::get("/index", ["middleware" => ["permission:read-organizational-unit-types"], "uses" => "OrganizationalUnitTypeController@index"])->name("ou-types.index");
    Route::get("/index-data", ["middleware" => ["permission:read-organizational-unit-types"], "uses" => "OrganizationalUnitTypeController@indexData"])->name("ou-types.index-data");
    Route::get("/create", ["middleware" => ["permission:create-organizational-unit-types"], "uses" => "OrganizationalUnitTypeController@create"])->name("ou-types.create");
    Route::post("/create", ["middleware" => ["permission:create-organizational-unit-types"], "uses" => "OrganizationalUnitTypeController@store"])->name("ou-types.store");
    Route::get("/{organizationalUnitType}/edit", ["middleware" => ["permission:update-organizational-unit-types"], "uses" => "OrganizationalUnitTypeController@edit"])->name("ou-types.edit");
    Route::put("/{organizationalUnitType}", ["middleware" => ["permission:update-organizational-unit-types"], "uses" => "OrganizationalUnitTypeController@update"])->name("ou-types.update");
});

Route::group(['middleware' => ['preventbackbutton', 'auth'], "prefix" => "admin/secpla/tipos/ejes-estrategicos"], function() {
    Route::get("/index", ["middleware" => ["permission:read-secpla-ejes-estrategicos"], "uses" => "Secpla\Tipos\EjeEstrategicoController@index"])->name("secpla.tipos.ejes.index");
    Route::get("/index-data", ["middleware" => ["permission:read-secpla-ejes-estrategicos"], "uses" => "Secpla\Tipos\EjeEstrategicoController@indexData"])->name("secpla.tipos.ejes.index-data");
    Route::get("/create", ["middleware" => ["permission:create-secpla-ejes-estrategicos"], "uses" => "Secpla\Tipos\EjeEstrategicoController@create"])->name("secpla.tipos.ejes.create");
    Route::post("/create", ["middleware" => ["permission:create-secpla-ejes-estrategicos"], "uses" => "Secpla\Tipos\EjeEstrategicoController@store"])->name("secpla.tipos.ejes.store");
    Route::get("/{ejeEstrategico}/edit", ["middleware" => ["permission:update-secpla-ejes-estrategicos"], "uses" => "Secpla\Tipos\EjeEstrategicoController@edit"])->name("secpla.tipos.ejes.edit");
    Route::put("/{ejeEstrategico}", ["middleware" => ["permission:update-secpla-ejes-estrategicos"], "uses" => "Secpla\Tipos\EjeEstrategicoController@update"])->name("secpla.tipos.ejes.update");
    Route::put("/{ejeEstrategico}/switch-status", ["middleware" => ["permission:update-secpla-ejes-estrategicos"], "uses" => "Secpla\Tipos\EjeEstrategicoController@switchStatus"])->name("secpla.tipos.ejes.switch-status");
    Route::delete("/{ejeEstrategico}", ["middleware" => ["permission:delete-secpla-ejes-estrategicos"], "uses" => "Secpla\Tipos\EjeEstrategicoController@delete"])->name("secpla.tipos.ejes.delete");
});

Route::group(['middleware' => ['preventbackbutton', 'auth'], "prefix" => "admin/secpla/tipos/proyectos"], function() {
    Route::get("/index", ["middleware" => ["permission:read-secpla-tipos-proyectos"], "uses" => "Secpla\Tipos\ProyectoController@index"])->name("secpla.tipos.proyecto.index");
    Route::get("/index-data", ["middleware" => ["permission:read-secpla-tipos-proyectos"], "uses" => "Secpla\Tipos\ProyectoController@indexData"])->name("secpla.tipos.proyecto.index-data");
    Route::get("/create", ["middleware" => ["permission:create-secpla-tipos-proyectos"], "uses" => "Secpla\Tipos\ProyectoController@create"])->name("secpla.tipos.proyecto.create");
    Route::post("/create", ["middleware" => ["permission:create-secpla-tipos-proyectos"], "uses" => "Secpla\Tipos\ProyectoController@store"])->name("secpla.tipos.proyecto.store");
    Route::get("/{proyecto}/edit", ["middleware" => ["permission:update-secpla-tipos-proyectos"], "uses" => "Secpla\Tipos\ProyectoController@edit"])->name("secpla.tipos.proyecto.edit");
    Route::put("/{proyecto}", ["middleware" => ["permission:update-secpla-tipos-proyectos"], "uses" => "Secpla\Tipos\ProyectoController@update"])->name("secpla.tipos.proyecto.update");
    Route::put("/{proyecto}/switch-status", ["middleware" => ["permission:update-secpla-tipos-proyectos"], "uses" => "Secpla\Tipos\ProyectoController@switchStatus"])->name("secpla.tipos.proyecto.switch-status");
    Route::delete("/{proyecto}", ["middleware" => ["permission:delete-secpla-tipos-proyectos"], "uses" => "Secpla\Tipos\ProyectoController@delete"])->name("secpla.tipos.proyecto.delete");
});

Route::group(['middleware' => ['preventbackbutton', 'auth'], "prefix" => "admin/secpla/tipos/financiamiento"], function() {
    Route::get("/index", ["middleware" => ["permission:read-secpla-tipos-financiamiento"], "uses" => "Secpla\Tipos\FinanciamientoController@index"])->name("secpla.tipos.financiamiento.index");
    Route::get("/index-data", ["middleware" => ["permission:read-secpla-tipos-financiamiento"], "uses" => "Secpla\Tipos\FinanciamientoController@indexData"])->name("secpla.tipos.financiamiento.index-data");
    Route::get("/create", ["middleware" => ["permission:create-secpla-tipos-financiamiento"], "uses" => "Secpla\Tipos\FinanciamientoController@create"])->name("secpla.tipos.financiamiento.create");
    Route::post("/create", ["middleware" => ["permission:create-secpla-tipos-financiamiento"], "uses" => "Secpla\Tipos\FinanciamientoController@store"])->name("secpla.tipos.financiamiento.store");
    Route::get("/{financiamiento}/edit", ["middleware" => ["permission:update-secpla-tipos-financiamiento"], "uses" => "Secpla\Tipos\FinanciamientoController@edit"])->name("secpla.tipos.financiamiento.edit");
    Route::put("/{financiamiento}", ["middleware" => ["permission:update-secpla-tipos-financiamiento"], "uses" => "Secpla\Tipos\FinanciamientoController@update"])->name("secpla.tipos.financiamiento.update");
    Route::put("/{financiamiento}/switch-status", ["middleware" => ["permission:update-secpla-tipos-financiamiento"], "uses" => "Secpla\Tipos\FinanciamientoController@switchStatus"])->name("secpla.tipos.financiamiento.switch-status");
    Route::delete("/{financiamiento}", ["middleware" => ["permission:delete-secpla-tipos-financiamiento"], "uses" => "Secpla\Tipos\FinanciamientoController@delete"])->name("secpla.tipos.financiamiento.delete");
});

Route::group(['middleware' => ['preventbackbutton', 'auth'], 'prefix' => 'admin/secpla/tipos/cuentas'], function() {
    Route::get("/index", ["middleware" => ["permission:read-secpla-tipos-cuentas"], "uses" => "Secpla\Tipos\CuentaController@index"])->name("secpla.tipos.cuentas.index");
    Route::get("/index-data", ["middleware" => ["permission:read-secpla-tipos-cuentas"], "uses" => "Secpla\Tipos\CuentaController@indexData"])->name("secpla.tipos.cuentas.index-data");
    Route::get("/create", ["middleware" => ["permission:create-secpla-tipos-cuentas"], "uses" => "Secpla\Tipos\CuentaController@create"])->name("secpla.tipos.cuentas.create");
    Route::post("/create", ["middleware" => ["permission:create-secpla-tipos-cuentas"], "uses" => "Secpla\Tipos\CuentaController@store"])->name("secpla.tipos.cuentas.store");
    Route::get("/{cuenta}/edit", ["middleware" => ["permission:update-secpla-tipos-cuentas"], "uses" => "Secpla\Tipos\CuentaController@edit"])->name("secpla.tipos.cuentas.edit");
    Route::put("/{cuenta}", ["middleware" => ["permission:update-secpla-tipos-cuentas"], "uses" => "Secpla\Tipos\CuentaController@update"])->name("secpla.tipos.cuentas.update");
    Route::put("/{cuenta}/switch-status", ["middleware" => ["permission:update-secpla-tipos-cuentas"], "uses" => "Secpla\Tipos\CuentaController@switchStatus"])->name("secpla.tipos.cuentas.switch-status");
    Route::delete("/{cuenta}", ["middleware" => ["permission:delete-secpla-tipos-cuentas"], "uses" => "Secpla\Tipos\CuentaController@delete"])->name("secpla.tipos.cuentas.delete");
});

Route::group(['middleware' => ['preventbackbutton', 'auth'], 'prefix' => 'admin/secpla/cuentas'], function() {
    Route::get("/index", ["middleware" => ["permission:read-secpla-cuentas"], "uses" => "Secpla\CuentaController@index"])->name("secpla.cuentas.index");
    Route::get("/index-data", ["middleware" => ["permission:read-secpla-cuentas"], "uses" => "Secpla\CuentaController@indexData"])->name("secpla.cuentas.index-data");
    Route::get("/create", ["middleware" => ["permission:create-secpla-cuentas"], "uses" => "Secpla\CuentaController@create"])->name("secpla.cuentas.create");
    Route::post("/create", ["middleware" => ["permission:create-secpla-cuentas"], "uses" => "Secpla\CuentaController@store"])->name("secpla.cuentas.store");
    Route::get("/{cuenta}/edit", ["middleware" => ["permission:update-secpla-cuentas"], "uses" => "Secpla\CuentaController@edit"])->name("secpla.cuentas.edit");
    Route::put("/{cuenta}", ["middleware" => ["permission:update-secpla-cuentas"], "uses" => "Secpla\CuentaController@update"])->name("secpla.cuentas.update");
    Route::put("/{cuenta}/switch-status", ["middleware" => ["permission:update-secpla-cuentas"], "uses" => "Secpla\CuentaController@switchStatus"])->name("secpla.cuentas.switch-status");
    Route::delete("/{cuenta}", ["middleware" => ["permission:delete-secpla-cuentas"], "uses" => "Secpla\CuentaController@delete"])->name("secpla.cuentas.delete");
});

Route::group(['middleware' => ['preventbackbutton', 'auth'], "prefix" => "/secpla/proyectos"], function() {
    Route::get("/index", ["middleware" => ["permission:read-secpla-proyectos"], "uses" => "Secpla\ProyectoController@index"])->name("secpla.proyectos.index");
    Route::get("/index-data", ["middleware" => ["permission:read-secpla-proyectos"], "uses" => "Secpla\ProyectoController@indexData"])->name("secpla.proyectos.index-data");
    Route::get("/create", ["middleware" => ["permission:create-secpla-proyectos"], "uses" => "Secpla\ProyectoController@create"])->name("secpla.proyectos.create");
    Route::post("/create", ["middleware" => ["permission:create-secpla-proyectos"], "uses" => "Secpla\ProyectoController@store"])->name("secpla.proyectos.store");
    Route::get("/{proyecto}/edit", ["middleware" => ["permission:update-secpla-proyectos"], "uses" => "Secpla\ProyectoController@edit"])->name("secpla.proyectos.edit");
    Route::get("/{proyecto}/edit/requisitos", ["middleware" => ["permission:update-secpla-proyectos"], "uses" => "Secpla\ProyectoController@requisitosData"])->name("secpla.proyectos.edit-requisitos");
    Route::put("/{proyecto}", ["middleware" => ["permission:update-secpla-proyectos"], "uses" => "Secpla\ProyectoController@update"])->name("secpla.proyectos.update");
    Route::get("/{proyecto}/edit/map", ["middleware" => ["permission:update-secpla-proyectos"], "uses" => "Secpla\ProyectoController@map"])->name("secpla.proyectos.map");
    Route::put("/{proyecto}/edit/map", ["middleware" => ["permission:update-secpla-proyectos"], "uses" => "Secpla\ProyectoController@saveMap"])->name("secpla.proyectos.save-map");
    Route::get("/{proyecto}/map", ["middleware" => ["permission:update-secpla-proyectos"], "uses" => "Secpla\ProyectoController@getMap"])->name("secpla.proyectos.get-map");
    Route::put("/{proyecto}/requisitos", ["middleware" => ["permission:update-secpla-proyectos"], "uses" => "Secpla\ProyectoController@updateRequisitos"])->name("secpla.proyectos.update-requisitos");
    Route::delete("/{proyecto}", ["middleware" => ["permission:delete-secpla-proyectos"], "uses" => "Secpla\ProyectoController@delete"])->name("secpla.proyectos.delete");
});

Route::group(['middleware' => ['preventbackbutton', 'auth'], "prefix" => "profile"], function() {
    Route::get("/show/{profile}", ["middleware" => ["permission:read-own-profile"], "uses" => "ProfileController@showOwn"])->name("profiles.own.show");
});



