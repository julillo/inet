const {mix} = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

//mix.js('resources/assets/js/app.js', 'public/js');
//mix.js('node_modules/jquery/dist/jquery.min.js', 'public/js');
//mix.js('resources/assets/js/tether.min.js', 'public/js');
//mix.js('node_modules/bootstrap/dist/js/bootstrap.min.js', "public/js");
//mix.sass('resources/assets/sass/app.scss', 'public/css');
//mix.sass('resources/assets/sass/dashboard.scss', 'public/css');
//mix.sass('resources/assets/sass/app.scss', 'public/css')
//        .sass('resources/assets/sass/dashboard.scss', 'public/css')
//        .js(['resources/assets/js/app.js',
//            'node_modules/jquery/dist/jquery.min.js',
//            'resources/assets/js/tether.min.js',
//            'node_modules/bootstrap/dist/js/bootstrap.min.js'], "public/js");

//mix.js([
//    'node_modules/jquery/dist/jquery.min.js',
//    'node_modules/bootstrap/dist/js/bootstrap.min.js',
//    'resources/assets/js/tether.min.js'], 'public/js/app.js');
//mix
////        .sass('resources/assets/sass/app.scss', 'public/css')
//        .sass("node_modules/bootstrap/dist/css/bootstrap.min.css", 'public/css')
//        .sass('resources/assets/sass/dashboard.scss', 'public/css');

//mix.js('resources/assets/js/app.js', 'public/js/app.js');
//mix.js('node_modules/jquery/dist/jquery.min.js', 'public/js/jquery.min.js');
//mix.js('resources/assets/js/tether.min.js', 'public/js/tether.min.js');
//mix.js('node_modules/bootstrap/dist/js/bootstrap.min.js', "public/js/bootstrap.min.js");
//mix.sass('resources/assets/sass/app.scss', 'public/css');
//mix.sass('resources/assets/sass/dashboard.scss', 'public/css');

mix.js('resources/assets/js/app.js', 'public/js')
    .sass('resources/assets/sass/app.scss', 'public/css');